<?php

namespace app\modules\admin;

use Yii;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    public $layout = 'main';

    // public $defaultRoute = 'main/index';

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
        Yii::configure($this, require(__DIR__ . '/config/main.php'));
        // Yii::$app->errorHandler->errorAction = 'error';
        // Yii::$app->user->loginUrl = 'admin/default/login';
    }
}
