<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Property;

/* @var $this yii\web\View */
/* @var $model app\models\PropertyNews */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="m-portlet">
    <?php $form = ActiveForm::begin([
        'id' => 'property-news-form',
        'enableClientValidation' => true,
        'options' => [
            'class' => 'm-form m-form--fit m-form--label-align-right',
        ],
    ]); ?>
    <div class="m-portlet__body">
        <!-- <div class="form-group m-form__group m--margin-top-10">
            <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand alert-dismissible fade show" role="alert">
                <div class="m-alert__icon">
                    <i class="flaticon-exclamation-1"></i>
                    <span></span>
                </div>
                <div class="m-alert__text" style="color: black;">
                    <strong>Attention!</strong> Entries with marks (<span class="required">*</span>) must be filled in.
                </div>
            </div>
        </div> -->
        <div class="form-group m-form__group m--margin-top-10">
            <?= $form->errorSummary($model); ?>
        </div>

        <?php
            $field = $form->field($model, 'id_property', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->dropDownList(Property::getAllProperty(), ['prompt'=>'-- Choose Property --','class'=>'form-control select'])->label('Property', ['class'=>'col-lg-3 col-form-label']);
        ?>
        <?php
            $field = $form->field($model, 'title', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->textInput(['maxlength' => 255])->label('Title', ['class'=>'col-lg-3 col-form-label']);
        ?>
        <?php
            $field = $form->field($model, 'description', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->textarea(['rows' => 6])->label('Description', ['class'=>'col-lg-3 col-form-label']);
        ?>
        <?php
            $field = $form->field($model, 'youtube_1', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->textInput(['maxlength' => 255])->label('Youtube URL', ['class'=>'col-lg-3 col-form-label']);
        ?>
        <?php
            // $field = $form->field($model, 'youtube_2', ['options' => ['class' => 'form-group m-form__group row']]);
            // $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            // echo $field->textInput(['maxlength' => 255])->label('Youtube', ['class'=>'col-lg-3 col-form-label']);
        ?>
        <?php
            $field = $form->field($gallery, 'imagesGallery[]', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            $imagesPreview = [];
            $imagesPreviewConfig = [];
            if (isset($uploadedImages)) {
                // sort for images sort_order
                if ($uploadedImages) foreach ($uploadedImages as $key => $image) {
                    $imagesPreview[] = $image->image_path;
                    $imagesPreviewConfig[$key]['caption'] = $image->image_path;
                    $imagesPreviewConfig[$key]['url'] = Url::to(['news/remove-image']);
                    $imagesPreviewConfig[$key]['key'] = $image->id_property_news_gallery;
                }
            }
            echo $field->widget(\kartik\file\FileInput::classname(), [
                'options' => [
                    'multiple' => true,
                    'class' =>'file-loading',
                    'accept' => 'image/*',
                ],
                'pluginOptions' => [
                    'initialPreview'=> $imagesPreview,
                    'initialPreviewConfig' => $imagesPreviewConfig,
                    'initialPreviewAsData' => true, // identify if you are sending preview data only and not the raw markup
                    'initialPreviewFileType' => 'image', // image is the default and can be overridden in config below
                    'deleteUrl' => false,
                    'uploadUrl' => Url::to(['upload']),
                    'uploadAsync' => false,
                    'allowedFileTypes' => ["image"],
                    'showCaption' => false,
                    'showRemove' => true,
                    'showUpload' => false,
                    'showCancel' => false,
                    'browseOnZoneClick' => true,
                    'autoOrientImage' => true,
                    'browseClass' => 'btn btn-info btn-block',
                    'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                    'browseLabel' =>  'Select Photo',
                    'removeClass' => 'btn btn-default btn-secondary btn-block',
                    'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> ',
                    'removeLabel' =>  'Remove',
                    'fileActionSettings' => [
                        'showUpload' => false,
                        'showDrag' => true,
                        'autoOrientImage' => true,
                        'showRemove' => false,
                    ],
                    'overwriteInitial'=>false,
                    'maxFileCount' => 4,
                ],
                'pluginEvents' => [
                    'filebatchselected' => new \yii\web\JsExpression(
                    'function(event, files) {
                        jQuery(event.currentTarget).fileinput("upload");
                    }')
                ]
            ])->label('Image', ['class'=>'col-lg-3 col-form-label']);
        ?>

        <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/7TF00hJI78Y" frameborder="0" allowfullscreen></iframe> -->
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions m-form__actions">
            <div class="form-group m-form__group row">
                <div class="col-lg-4"></div>
                <div class="col-lg-8">
                    <?= Html::submitButton('<span><i class="fa fa-save"></i> <span>Submit</span></span>', ['class' => 'btn btn-info m-btn m-btn--icon m-btn--wide']) ?>
                    <button type="reset" class="btn btn-secondary m-btn m-btn--icon m-btn--wide"><span><i class="fa fa-history"></i> <span>Cancel</span></span></button>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
