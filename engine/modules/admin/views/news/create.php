<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PropertyNews */

$this->title = 'Create Property News';
$this->params['breadcrumbs'][] = ['label' => 'Property News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="property-news-create">
    <?= $this->render('_form', [
        'model' => $model,
        'gallery' => $gallery,
    ]) ?>
</div>