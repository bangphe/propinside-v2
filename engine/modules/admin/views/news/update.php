<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PropertyNews */

$this->title = 'Update Property News: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Property News', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id_property_news]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="property-news-update">

    <?= $this->render('_form', [
        'model' => $model,
        'gallery' => $gallery,
        'uploadedImages' => $uploadedImages,
    ]) ?>

</div>
