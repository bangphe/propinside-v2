<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PropertyFacilities */

$this->title = 'Create Property Facilities';
$this->params['breadcrumbs'][] = ['label' => 'Property Facilities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-facilities-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
