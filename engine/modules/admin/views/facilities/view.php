<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PropertyFacilities */

$this->title = $model->facilities;
$this->params['breadcrumbs'][] = ['label' => 'Property Facilities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="row">
    <div class="col-md-12">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-progress">

                    <!-- here can place a progress bar-->
                </div>
                <div class="m-portlet__head-wrapper">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="flaticon-envelope"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Detail Property Facilities
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <?= Html::a('<span><i class="fa fa-plus"></i><span>Add Data</span></span>', ['create'], ['class' => 'btn btn-info m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10']) ?>
                        <?= Html::a('<span><i class="fa fa-edit"></i><span>Update Data</span></span>', ['update', 'id' => $model->id_facilities], ['class' => 'btn btn-success m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10']) ?>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <?= Yii::$app->session->getFlash('info');?>
                <div class="table-scrollable">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id_facilities',
                            'facilities',
                            [
                                'label' => 'Status',
                                'format' => 'raw',
                                'value' => $model->getStatusLabel(),
                            ],
                            'created_at',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>


