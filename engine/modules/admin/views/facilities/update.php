<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PropertyFacilities */

$this->title = 'Update Property Facilities: ' . $model->facilities;
$this->params['breadcrumbs'][] = ['label' => 'Property Facilities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_facilities, 'url' => ['view', 'id' => $model->id_facilities]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="property-facilities-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
