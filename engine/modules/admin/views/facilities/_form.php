<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PropertyFacilities */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="m-portlet">
    <?php $form = ActiveForm::begin([
        'id' => 'property-facilities-form',
        'enableClientValidation' => true,
        'options' => [
            'class' => 'm-form m-form--fit m-form--label-align-right',
        ],
    ]); ?>
    <div class="m-portlet__body">
        <?php
            $field = $form->field($model, 'facilities', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->textInput(['maxlength' => 255])->label('Facilities', ['class'=>'col-lg-3 col-form-label']);
        ?>
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions m-form__actions">
            <div class="form-group m-form__group row">
                <div class="col-lg-4"></div>
                <div class="col-lg-8">
                    <?= Html::submitButton('<span><i class="fa fa-save"></i> <span>Submit</span></span>', ['class' => 'btn btn-info m-btn m-btn--icon m-btn--wide']) ?>
                    <button type="reset" class="btn btn-secondary m-btn m-btn--icon m-btn--wide"><span><i class="fa fa-history"></i> <span>Cancel</span></span></button>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
