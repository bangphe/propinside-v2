<?php

use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Agent;
use app\models\Property;

$this->title = 'Dashboard';
// $this->params['breadcrumbs'][] = ['label' => 'Agents', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<style type="text/css">
table.dataTable tr td.select-checkbox {
    text-align: center;
}
</style>

<div class="row">
    <div class="col-md-12">
        <?= Yii::$app->session->getFlash('info');?>
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-progress">

                    <!-- here can place a progress bar-->
                </div>
                <div class="m-portlet__head-wrapper">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="flaticon-user"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Data Agents
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <button class="btn btn-danger m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10" onclick="deleteAllAgent(this)">
                            <span><i class="fa fa-trash"></i><span>Delete All</span></span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <table class="table table-striped- table-bordered table-hover" id="table-agent">
                    <thead>
                        <tr>
                            <th width="10px" class="text-center"></th>
                            <th width="90px">Full Name</th>
                            <th width="90px">Username</th>
                            <th width="75px">Email</th>
                            <th width="40px">Title</th>
                            <th width="70px">Phone Number</th>
                            <th width="50px">Created At</th>
                            <th width="70px">Status</th>
                            <th width="60px">#</th>
                        </tr>
                    </thead>
                </table>
                <!-- <form id="form-checked"></form> -->
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-progress">
                    <!-- here can place a progress bar-->
                </div>
                <div class="m-portlet__head-wrapper">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="flaticon-home"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Data Properties
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <button class="btn btn-success m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10" onclick="approveAllProp(this)">
                            <span><i class="fa fa-check"></i><span>Approve All</span></span>
                        </button>
                        <button class="btn btn-danger m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10" onclick="deleteAllProp(this)">
                            <span><i class="fa fa-trash"></i><span>Delete All</span></span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <table class="table table-striped- table-bordered table-hover" id="table-prop">
                    <thead>
                        <tr>
                            <th width="10px" class="text-center">#</th>
                            <th width="90px">Title</th>
                            <th width="90px">Price</th>
                            <th width="90px">Location</th>
                            <th width="90px">Type</th>
                            <th width="90px">Developer</th>
                            <th width="50px">Created At</th>
                            <th width="70px">Status</th>
                            <th width="60px">#</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    getDataAgent();
    getDataProperty();
});
function getDataAgent() {
    var url = '<?= Url::to(["/admin/default/getalldataagent"])?>';
    $('#table-agent').DataTable({
        "ajax": {
            "url": url,
            "data": function(d){
                return JSON.stringify(d.data);
            },
        },
        "lengthMenu": [
            [5,10, 25, -1],
            [5,10, 25, "All"]
        ],
        "pageLength": 10,
        "columnDefs": [{
            'className': 'select-checkbox',
            'targets': 0,
            'checkboxes': {
                'selectRow': true,
            },
        //     'createdCell':  function (td, cellData, rowData, row, col){
        //        if(rowData[6].indexOf("Not Approved") === -1){
        //           this.api().cell(td).checkboxes.disable();
        //        }
        //     },
        }],
        "select": {
            'style': 'multi',
            'selector': 'td:first-child'
        },
        "order": [
            [6, "desc"]
        ],
        "processing": true,
    });
}

// Handle form submission event 
function approveAllAgent(e){
    var table = $('#table-agent').DataTable();
    var rows_selected = table.column(0).checkboxes.selected();

    var url = '<?= Url::to(["/admin/default/setallstatusagent"]); ?>';
    var value = '<?= Agent::STATUS_APPROVED; ?>';

    swal({
        title: "Are you sure to change this status of all agents?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        reverseButtons: !0
    }).then(function(e) {
        if (e.value) {
            $.ajax({
                url: url,
                type: "POST",
                data: { "status": value, "ids": rows_selected.join(",") },
                success: function () {
                    swal("Success!", "Data is successfully saved!", "success");
                    setTimeout(function(){// wait for 5 secs(2)
                       $('#table-agent').DataTable().ajax.reload();
                    }, 1000); 
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error!", "Please try again.", "error");
                }
            });
        } else {
            swal("Cancel!", "The changes is cancelled :)", "error");
        }
    });
};

function getDataProperty() {
    var url = '<?= Url::to(["/admin/default/getalldataproperty"])?>';
    $('#table-prop').DataTable({
        "ajax": {
            "url": url,
            "data": function(d){
                return JSON.stringify(d.data);
            },
        },
        "lengthMenu": [
            [5,10, 25, -1],
            [5,10, 25, "All"]
        ],
        "pageLength": 10,
        "columnDefs": [{
            'className': 'select-checkbox',
            'targets': 0,
            'checkboxes': {
                'selectRow': true,
                // 'selectCallback': function(){
                //     printSelectedRows();
                // }
            },
            // 'createdCell':  function (td, cellData, rowData, row, col){
            //    if(rowData[7].indexOf("Not Published") === -1){
            //       this.api().cell(td).checkboxes.disable();
            //    }
            // },
        }],
        "select": {
            'style': 'multi',
            'selector': 'td:first-child'
        },
        "order": [
            [6, "desc"]
        ],
        "processing": true,
    });
}

function approveAllProp(e){
    var table = $('#table-prop').DataTable();
    var rows_selected = table.column(0).checkboxes.selected();
    // console.log("rows :: ", rows_selected.length);

    if (rows_selected.length == 0) {
        swal("Warning!", "Please choose the data first at least one data.", "warning");
        return;
    }

    var url = '<?= Url::to(["/admin/default/setallstatusprop"]); ?>';
    var value = '<?= Property::STATUS_APPROVED; ?>';

    swal({
        title: "Are you sure to change this status of all properties?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        reverseButtons: !0
    }).then(function(e) {
        if (e.value) {
            $.ajax({
                url: url,
                type: "POST",
                data: { "status": value, "ids": rows_selected.join(",") },
                success: function (e) {
                    if (e.result == "success") {
                        swal("Success!", "Data is successfully saved!", "success");
                        setTimeout(function(){// wait for 5 secs(2)
                           $('#table-prop').DataTable().ajax.reload();
                        }, 500); 
                    } else {
                        swal({title:"", text:"Error! Please try again.", type: "error" });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error!", "Please try again.", "error");
                }
            });
        } else {
            swal("Cancel!", "The changes is cancelled :)", "error");
        }
    });
};

function deleteAllProp(e){
    var table = $('#table-prop').DataTable();
    var rows_selected = table.column(0).checkboxes.selected();
    // console.log("rows :: ", rows_selected.length);

    if (rows_selected.length == 0) {
        swal("Warning!", "Please choose the data first at least one data.", "warning");
        return;
    }

    var url = '<?= Url::to(["/admin/default/deleteallprop"]); ?>';
    var value = '<?= Property::STATUS_NOT_ACTIVE; ?>';

    swal({
        title: "Are you sure to delete the data of all properties?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        reverseButtons: !0
    }).then(function(e) {
        if (e.value) {
            $.ajax({
                url: url,
                type: "POST",
                data: { "status": value, "ids": rows_selected.join(",") },
                success: function (e) {
                    if (e.result == "success") {
                        swal("Success!", "Data is successfully saved!", "success");
                        setTimeout(function(){// wait for 5 secs(2)
                           $('#table-prop').DataTable().ajax.reload();
                        }, 500); 
                    } else {
                        swal({title:"", text:"Error! Please try again.", type: "error" });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error!", "Please try again.", "error");
                }
            });
        } else {
            swal("Cancel!", "The changes is cancelled :)", "error");
        }
    });
};

function deleteAllAgent(e){
    var table = $('#table-agent').DataTable();
    var rows_selected = table.column(0).checkboxes.selected();

    if (rows_selected.length == 0) {
        swal("Warning!", "Please choose the data first at least one data.", "warning");
        return;
    }

    var url = '<?= Url::to(["/admin/default/deleteallagent"]); ?>';
    var value = '<?= Property::STATUS_NOT_ACTIVE; ?>';

    swal({
        title: "Are you sure to delete the data of all agents?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        reverseButtons: !0
    }).then(function(e) {
        if (e.value) {
            $.ajax({
                url: url,
                type: "POST",
                data: { "status": value, "ids": rows_selected.join(",") },
                success: function (e) {
                    if (e.result == "success") {
                        swal("Success!", "Data is successfully saved!", "success");
                        setTimeout(function(){// wait for 5 secs(2)
                           $('#table-agent').DataTable().ajax.reload();
                        }, 500); 
                    } else {
                        swal({title:"", text:"Error! Please try again.", type: "error" });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error!", "Please try again.", "error");
                }
            });
        } else {
            swal("Cancel!", "The changes is cancelled :)", "error");
        }
    });
};

function setStatus(id, type)
{
    var url = '<?= Url::to(["/admin/default/setstatus"]); ?>';
    var value = '';
    if (type == 'approved') {
        value = '<?= Agent::STATUS_APPROVED; ?>';
    } else {
        value = '<?= Agent::STATUS_NOT_APPROVED; ?>';
    }
    // console.log(value);

    swal({
        title: "Are you sure to approve this agent?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        reverseButtons: !0
    }).then(function(e) {
        if (e.value) {
            $.ajax({
                url: url,
                type: "POST",
                data: { "status": value, "id": id },
                success: function (e) {
                    if (e.result == "success") {
                        swal("Success!", "Data is successfully saved!", "success");
                        setTimeout(function(){// wait for 5 secs(2)
                           $('#table-agent').DataTable().ajax.reload();
                        }, 500); 
                    } else {
                        swal({title:"", text:"Error! Please try again.", type: "error" });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error!", "Please try again.", "error");
                }
            });
        } else {
            swal("Cancel!", "The changes is cancelled :)", "error");
        }
    });
}

function setStatusProp(id, type)
{
    var url = '<?= Url::to(["/admin/default/setstatusprop"]); ?>';
    var value = '';
    if (type == 'approved') {
        value = '<?= Property::STATUS_APPROVED; ?>';
    } else {
        value = '<?= Property::STATUS_NOT_APPROVED; ?>';
    }
    // console.log(value);

    swal({
        title: "Are you sure to change this status of property?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        reverseButtons: !0
    }).then(function(e) {
        if (e.value) {
            $.ajax({
                url: url,
                type: "POST",
                data: { "status": value, "id": id },
                success: function () {
                    swal("Success!", "Data is successfully saved!", "success");
                    setTimeout(function(){// wait for 5 secs(2)
                       $('#table-prop').DataTable().ajax.reload();
                    }, 1000); 
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error!", "Please try again.", "error");
                }
            });
        } else {
            swal("Cancel!", "The changes is cancelled :)", "error");
        }
    });
}
</script>