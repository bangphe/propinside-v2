<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Properties';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-index">
    <div class="row">
        <div class="col-md-12">
            <?= Yii::$app->session->getFlash('info');?>
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-progress">

                        <!-- here can place a progress bar-->
                    </div>
                    <div class="m-portlet__head-wrapper">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="flaticon-price-tag"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Data Properties
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <?= Html::a('<span><i class="fa fa-plus"></i><span>Add Data</span></span>', ['create'], ['class' => 'btn btn-info m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10']) ?>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <table class="table table-striped- table-bordered table-hover" id="tablegrid">
                        <thead>
                            <tr>
                                <th width="10px">#</th>
                                <th width="90px">Title</th>
                                <th width="90px">Price</th>
                                <th width="90px">Location</th>
                                <th width="90px">Type</th>
                                <th width="90px">Developer</th>
                                <th width="50px">Created At</th>
                                <th width="40px">Status</th>
                                <th width="80px">#</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>