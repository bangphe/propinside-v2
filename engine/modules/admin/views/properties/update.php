<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Property */

$this->title = 'Update Property: ' . $property->title;
$this->params['breadcrumbs'][] = ['label' => 'Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $property->title, 'url' => ['view', 'id' => $property->id_property]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="property-update">

    <?= $this->render('_form', [
        'property' => $property,
        'property_images' => $property_images,
        'uploadedImages' => $uploadedImages,
    ]) ?>

</div>
