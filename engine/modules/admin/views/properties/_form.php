<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Property;
use app\models\PropertyFacilities;
use app\models\PropertyAmenities;
use kartik\select2\Select2;

$dataDistrict = Property::getAllDistrict();
$dataType = Property::getAllType();
/* @var $this yii\web\View */
/* @var $model app\models\Property */
/* @var $form yii\widgets\ActiveForm */
?>

<style type="text/css">
.select2-container .select2-selection--single {
    height: 38px;
}
.select2-container--krajee .select2-selection--single .select2-selection__arrow {
    height: 38px;
}
</style>

<div class="m-portlet">
    <?php $form = ActiveForm::begin([
        'id' => 'property-form',
        'enableClientValidation' => true,
        'options' => [
            'class' => 'm-form m-form--fit m-form--label-align-right',
            'enctype'=>'multipart/form-data',
        ],
    ]); ?>
    <div class="m-portlet__body">
        <div class="form-group m-form__group row">
            <div class="col-sm-6"> 
                <label>Title</label>
                <?= $form->field($property, 'title')->textInput(['class'=>'form-control m-input', 'placeholder'=>'Enter your property title'])->label(false) ?>
            </div>
            <div class="col-sm-6">
                <label>Location</label>
                <?//= $form->field($property, 'location')->textInput(['class'=>'form-control m-input', 'placeholder'=>'Enter your property location'])->label(false) ?>
                <?= $form->field($property, 'location')->widget(Select2::classname(), [
                    'data' => $dataDistrict,
                    'options' => ['placeholder' => 'Select Property Location'],
                    'size' => Select2::MEDIUM,
                    'pluginOptions' => [
                        'allowClear' => false
                    ],
                ])->label(false); ?>
            </div>
        </div>

        <div class="form-group m-form__group row">
            <div class="col-sm-6">
                <label>Type</label>
                <?//= $form->field($property, 'type')->textInput(['class'=>'form-control m-input', 'placeholder'=>'Enter your property type'])->label(false) ?>
                <?= $form->field($property, 'type')->widget(Select2::classname(), [
                    'data' => $dataType,
                    'options' => ['placeholder' => 'Select Property Type'],
                    'size' => Select2::MEDIUM,
                    'pluginOptions' => [
                        'allowClear' => false
                    ],
                ])->label(false); ?>
            </div>
            <div class="col-sm-6">
                <label>Price</label>
                <?= $form->field($property, 'price')->textInput(['class'=>'form-control m-input', 'placeholder'=>'23,000', 'type'=>'number'])->label(false) ?>
            </div>
        </div>

        <div class="form-group m-form__group row">
            <div class="col-sm-12">
                <h3 class="m-form__heading-title">Property Photos <i class="fa fa-info-circle help" data-toggle="tooltip" title="Add images to upload for property!"></i></h3>
                <?php
                    $imagesPreview = [];
                    $imagesPreviewConfig = [];
                    if (isset($uploadedImages)) {
                        // sort for images sort_order
                        if ($uploadedImages) foreach ($uploadedImages as $key => $image) {
                            $imagesPreview[] = $image->image_path;
                            $imagesPreviewConfig[$key]['caption'] = $image->image_path;
                            $imagesPreviewConfig[$key]['url'] = Url::to(['news/remove-image']);
                            $imagesPreviewConfig[$key]['key'] = $image->id_image;
                        }
                    }
                    echo $form->field($property_images, 'imagesGallery[]')->widget(\kartik\file\FileInput::classname(), [
                    'options' => [
                        'multiple' => true,
                        'class' =>'file-loading',
                        'accept' => 'image/*',
                    ],
                    'pluginOptions' => [
                        'initialPreview'=> $imagesPreview,
                        'initialPreviewConfig' => $imagesPreviewConfig,
                        'initialPreviewAsData'=>true,
                        'deleteUrl' => false,
                        'allowedFileTypes' => ["image"],
                        'showCaption' => false,
                        'showRemove' => true,
                        'showUpload' => false,
                        'showCancel' => false,
                        'browseOnZoneClick' => true,
                        'autoOrientImage' => true,
                        'browseClass' => 'btn btn-primary btn-block',
                        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                        'browseLabel' =>  'Select Photo',
                        'removeClass' => 'btn btn-default btn-danger btn-block',
                        'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> ',
                        'removeLabel' =>  'Remove',
                        'fileActionSettings' => [
                            'showUpload' => false,
                            'showDrag' => true,
                            'autoOrientImage' => true,
                        ],
                        'overwriteInitial'=>false,
                        'maxFileCount' => 5,
                    ],
                    'pluginEvents' => [
                        'filebatchselected' => new \yii\web\JsExpression(
                        'function(event, files) {
                            jQuery(event.currentTarget).fileinput("upload");
                        }')
                    ]
                ])->label(false);
                ?>
            </div>
        </div>

        <div class="form-group m-form__group row">
            <div class="col-sm-12">
                <h3 class="m-form__heading-title">Property Detail <i class="fa fa-info-circle help" data-toggle="tooltip" title="Add details for property!"></i></h3>
            </div>
            <div class="col-sm-4">
                <label>Size Prefix</label>
                <?= $form->field($property, 'floor_size')->textInput(['class'=>'form-control m-input', 'placeholder'=>'Enter your property floor size'])->label(false) ?>
            </div>
      
            <div class="col-sm-4">
                <label>Developer</label>
                <?= $form->field($property, 'developer')->textInput(['class'=>'form-control m-input', 'placeholder'=>'Enter your property developer'])->label(false) ?>
            </div>

            <div class="col-sm-4">
                <label>Land Size</label>
                <?= $form->field($property, 'land_size')->textInput(['class'=>'form-control m-input', 'placeholder'=>'Enter your property land size'])->label(false) ?>
            </div>
                
            <div class="col-sm-4">
                <label>Psf</label>
                <?= $form->field($property, 'psf')->textInput(['class'=>'form-control m-input', 'placeholder'=>'Enter your property psf'])->label(false) ?>
            </div>

            <div class="col-sm-4">
                <label>Furnishing</label>
                <?= $form->field($property, 'furnishing')->textInput(['class'=>'form-control m-input', 'placeholder'=>'Enter your property furnishing'])->label(false) ?>
            </div>

            <div class="col-sm-4">
                <label>Top</label>
                <?= $form->field($property, 'top')->textInput(['class'=>'form-control m-input', 'placeholder'=>'Enter your property top'])->label(false) ?>
            </div>

            <div class="col-sm-4">
                <label>Floor Level</label>
                <?= $form->field($property, 'floor_level')->textInput(['class'=>'form-control m-input', 'placeholder'=>'Enter your property floor level'])->label(false) ?>
            </div>

            <div class="col-sm-4">
                <label>Tenure</label>
                <?= $form->field($property, 'tenure')->textInput(['class'=>'form-control m-input', 'placeholder'=>'Enter your property tenure'])->label(false) ?>
            </div>
        </div>

        <div class="form-group m-form__group row">
            <div class="col-sm-12">
                <h3 class="m-form__heading-title">Property Description <i class="fa fa-info-circle help" data-toggle="tooltip" title="Add description for property!"></i></h3>
                <?= $form->field($property, 'description')->textarea(['class'=>'form-control', 'placeholder'=>'Write here something about property', 'rows'=>5])->label(false) ?>
            </div>
        </div>

        <div class="form-group m-form__group row">
            <div class="col-sm-12">
                <h3 class="m-form__heading-title">Property Facilities <i class="fa fa-info-circle help" data-toggle="tooltip" title="Add property facilities!"></i></h3>
                    <?php 
                    if(!$property->isNewRecord) {
                        $checkedList = [];
                        foreach ($property->propertyHasFacilities as $v) {
                            $checkedList[] = $v->id_facilities;
                        }
                        $property->prop_facilities = $checkedList;
                    }

                    echo $form->field($property, 'prop_facilities')->checkboxList(
                        PropertyFacilities::listFacilities(),
                        [
                            'class' => 'm-checkbox-inline',
                            'item' => function($index, $label, $name, $checked, $value) {
                                $checkbox = '<label class="m-checkbox">';
                                if ($checked) {
                                    $checkbox .= '<input type="checkbox" name="' . $name . '" value="' . $value . '" checked>';
                                } else {
                                    $checkbox .= '<input type="checkbox" name="' . $name . '" value="' . $value . '">';
                                }
                                $checkbox .= $label;
                                $checkbox .= '<span></span>';
                                $checkbox .= '</label>';

                                return $checkbox;
                            }
                        ]
                    )->label(false);
                ?>
            </div>
        </div>

        <div class="form-group m-form__group row">
            <div class="col-sm-12">
                <h3 class="m-form__heading-title">Property Amenities <i class="fa fa-info-circle help" data-toggle="tooltip" title="Add property amenities!"></i></h3>
                <div class="search-propertie-filters">
                    <div class="container-2">
                    <?php
                    if(!$property->isNewRecord) {
                        $checkedList = [];
                        foreach ($property->propertyHasAmenities as $v) {
                            $checkedList[] = $v->id_amenities;
                        }
                        $property->prop_amenities = $checkedList;
                    }

                    echo $form->field($property, 'prop_amenities')->checkboxList(
                        PropertyAmenities::listAmenities(),
                        [
                            'class' => 'm-checkbox-inline',
                            'item' => function($index, $label, $name, $checked, $value) {
                                $checkbox = '<label class="m-checkbox">';
                                if ($checked) {
                                    $checkbox .= '<input type="checkbox" name="' . $name . '" value="' . $value . '" checked>';
                                } else {
                                    $checkbox .= '<input type="checkbox" name="' . $name . '" value="' . $value . '">';
                                }
                                $checkbox .= $label;
                                $checkbox .= '<span></span>';
                                $checkbox .= '</label>';

                                return $checkbox;
                            }
                        ]
                    )->label(false);
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions m-form__actions">
            <div class="form-group m-form__group row">
                <div class="col-lg-4"></div>
                <div class="col-lg-8">
                    <?= Html::submitButton('<span><i class="fa fa-save"></i> <span>Submit</span></span>', ['class' => 'btn btn-info m-btn m-btn--icon m-btn--wide']) ?>
                    <button type="reset" class="btn btn-secondary m-btn m-btn--icon m-btn--wide"><span><i class="fa fa-history"></i> <span>Cancel</span></span></button>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>