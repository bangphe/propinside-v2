<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Agent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="m-portlet">
    <?php $form = ActiveForm::begin([
        'id' => 'property-news-form',
        'enableClientValidation' => true,
        'options' => [
            'class' => 'm-form m-form--fit m-form--label-align-right',
        ],
    ]); ?>
    <div class="m-portlet__body">
        <?php
            $field = $form->field($model, 'name', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->textInput(['maxlength' => true])->label('Name', ['class'=>'col-lg-3 col-form-label']);
        ?>

        <?php
            $field = $form->field($model, 'gender', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->dropDownList($model->getAllGender(), ['prompt'=>'-- Choose Gender --','class'=>'form-control select'])->label('Gender', ['class'=>'col-lg-3 col-form-label']);
        ?>

        <?php
            $field = $form->field($model, 'username', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->textInput(['maxlength' => true])->label('Username', ['class'=>'col-lg-3 col-form-label']);
        ?>

        <?php
            $field = $form->field($model, 'password', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->passwordInput(['maxlength' => true])->label('Password', ['class'=>'col-lg-3 col-form-label']);
        ?>

        <?php
            $field = $form->field($model, 'photo', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            $imagesPreview = '';
            if (!$model->isNewRecord) {
                $imagesPreview = Yii::getAlias('@web').$model->photo;
            }
            echo $field->widget(\kartik\file\FileInput::classname(), [
                'options' => [
                    'class' =>'file-loading',
                    'accept' => 'image/*',
                ],
                'pluginOptions' => [
                    'initialPreview'=> $imagesPreview,
                    'initialPreviewAsData'=>true,
                    'deleteUrl' => false,
                    'allowedFileTypes' => ["image"],
                    'showCaption' => false,
                    'showRemove' => true,
                    'showUpload' => false,
                    'showCancel' => false,
                    'browseOnZoneClick' => true,
                    'autoOrientImage' => true,
                    'browseClass' => 'btn btn-info btn-block',
                    'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                    'browseLabel' =>  'Select Photo',
                    'removeClass' => 'btn btn-default btn-secondary btn-block',
                    'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> ',
                    'removeLabel' =>  'Remove',
                    'fileActionSettings' => [
                        'showUpload' => false,
                        'showDrag' => true,
                        'autoOrientImage' => true,
                    ],
                    'overwriteInitial'=>false,
                ],
                'pluginEvents' => [
                    'filebatchselected' => new \yii\web\JsExpression(
                    'function(event, files) {
                        jQuery(event.currentTarget).fileinput("upload");
                    }')
                ]
            ])->label('Agent Photo', ['class'=>'col-lg-3 col-form-label']);
        ?>

        <?php
            $field = $form->field($model, 'registration_number', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->textInput(['maxlength' => true])->label('Registration Number', ['class'=>'col-lg-3 col-form-label']);
        ?>

        <?php
            $field = $form->field($model, 'title', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->textInput(['maxlength' => true])->label('Title', ['class'=>'col-lg-3 col-form-label']);
        ?>

        <?php
            $field = $form->field($model, 'description', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->textarea(['rows' => 5])->label('Description', ['class'=>'col-lg-3 col-form-label']);
        ?>

        <?php
            $field = $form->field($model, 'phone_number', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->textInput(['maxlength' => true, 'type' => 'number'])->label('Phone Number', ['class'=>'col-lg-3 col-form-label']);
        ?>

        <?php
            $field = $form->field($model, 'email', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->textInput(['maxlength' => true])->label('Email', ['class'=>'col-lg-3 col-form-label']);
        ?>

        <?php
            $field = $form->field($model, 'award', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->textInput(['maxlength' => true])->label('Award', ['class'=>'col-lg-3 col-form-label']);
        ?>

        <?php
            $field = $form->field($model, 'speciality', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->textInput(['maxlength' => true])->label('Speciality', ['class'=>'col-lg-3 col-form-label']);
        ?>

        <?php
            $field = $form->field($model, 'country_operability', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->textInput(['maxlength' => true])->label('Country Operability', ['class'=>'col-lg-3 col-form-label']);
        ?>

        <?php
            $field = $form->field($model, 'investment_deposit_range', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->textInput(['maxlength' => true])->label('Investment Deposit Range', ['class'=>'col-lg-3 col-form-label']);
        ?>

        <?php
            $field = $form->field($model, 'facebook', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->textInput(['maxlength' => true])->label('Link Facebook', ['class'=>'col-lg-3 col-form-label']);
        ?>

        <?php
            $field = $form->field($model, 'twitter', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->textInput(['maxlength' => true])->label('Link Twitter', ['class'=>'col-lg-3 col-form-label']);
        ?>

        <?php
            $field = $form->field($model, 'linkedin', ['options' => ['class' => 'form-group m-form__group row']]);
            $field->template = '{label}<div class="col-lg-6">{input}{error}</div>';
            echo $field->textInput(['maxlength' => true])->label('Link Linkedin', ['class'=>'col-lg-3 col-form-label']);
        ?>
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions m-form__actions">
            <div class="form-group m-form__group row">
                <div class="col-lg-4"></div>
                <div class="col-lg-8">
                    <?= Html::submitButton('<span><i class="fa fa-save"></i> <span>Submit</span></span>', ['class' => 'btn btn-info m-btn m-btn--icon m-btn--wide']) ?>
                    <button type="reset" class="btn btn-secondary m-btn m-btn--icon m-btn--wide"><span><i class="fa fa-history"></i> <span>Cancel</span></span></button>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
