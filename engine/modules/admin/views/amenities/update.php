<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PropertyAmenities */

$this->title = 'Update Property Amenities: ' . $model->amenities;
$this->params['breadcrumbs'][] = ['label' => 'Property Amenities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_amenities, 'url' => ['view', 'id' => $model->id_amenities]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="property-amenities-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
