<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PropertyAmenities */

$this->title = 'Create Property Amenities';
$this->params['breadcrumbs'][] = ['label' => 'Property Amenities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-amenities-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
