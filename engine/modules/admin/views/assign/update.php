<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Agent */

$this->title = 'Update Property Listing: ' . $model->id_listing;
$this->params['breadcrumbs'][] = ['label' => 'Agents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_listing, 'url' => ['view', 'id' => $model->id_listing]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="agent-listing-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
