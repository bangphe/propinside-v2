<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PropertyNews */

$this->title = 'Add Property Listing';
$this->params['breadcrumbs'][] = ['label' => 'Property Listing', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="property-listing-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>