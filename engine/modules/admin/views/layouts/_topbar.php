<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<div class="m-header__bottom">
    <div class="m-container m-container--fluid m-container--full-height m-page__container">
        <div class="m-stack m-stack--ver m-stack--desktop">

            <!-- begin::Horizontal Menu -->
            <div class="m-stack__item m-stack__item--fluid m-header-menu-wrapper">
                <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-light " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
                <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light ">
                    <ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
                        <li class="m-menu__item m-menu__item--submenu m-menu__item--tabs <?= Yii::$app->controller->id == 'default' ? 'm-menu__item--active m-menu__item--active-tab' : ''; ?>" m-menu-submenu-toggle="tab" aria-haspopup="true">
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <span class="m-menu__link-text">Dashboard</span>
                                <i class="m-menu__hor-arrow la la-angle-down"></i>
                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left m-menu__submenu--tabs"><span class="m-menu__arrow m-menu__arrow--adjust"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'default' ? 'm-menu__item--active' : ''; ?>" m-menu-link-redirect="1" aria-haspopup="true">
                                        <?= Html::a('<i class="m-menu__link-icon flaticon-home-2"></i><span class="m-menu__link-text">Dashboard</span>', ['default/index'], ['class'=>'m-menu__link']) ?>
                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item  m-menu__item--submenu m-menu__item--tabs <?= Yii::$app->controller->id == 'agents' || Yii::$app->controller->id == 'assign' ? 'm-menu__item--active m-menu__item--active-tab' : ''; ?>" m-menu-submenu-toggle="tab" aria-haspopup="true"><a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__link-text"> Agents</span><i class="m-menu__hor-arrow la la-angle-down"></i><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                            <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left m-menu__submenu--tabs"><span class="m-menu__arrow m-menu__arrow--adjust"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'agents' ? 'm-menu__item--active' : ''; ?>" m-menu-link-redirect="1" aria-haspopup="true">
                                        <?= Html::a('<i class="m-menu__link-icon flaticon-users"></i><span class="m-menu__link-text">List Data</span>', ['agents/index'], ['class'=>'m-menu__link']) ?>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'assign' ? 'm-menu__item--active' : ''; ?>" m-menu-link-redirect="1" aria-haspopup="true">
                                        <?= Html::a('<i class="m-menu__link-icon flaticon-list"></i><span class="m-menu__link-text">Assign Property</span>', ['assign/index'], ['class'=>'m-menu__link']) ?>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item   m-menu__item--submenu m-menu__item--tabs <?= Yii::$app->controller->id == 'properties' || Yii::$app->controller->id == 'news' ? 'm-menu__item--active m-menu__item--active-tab' : ''; ?>" m-menu-submenu-toggle="tab" aria-haspopup="true"><a href="javascript:;" class="m-menu__link m-menu__toggle"><span
                                 class="m-menu__link-text"> Properties</span><i class="m-menu__hor-arrow la la-angle-down"></i><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                            <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left m-menu__submenu--tabs"><span class="m-menu__arrow m-menu__arrow--adjust"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'properties' ? 'm-menu__item--active' : ''; ?>" m-menu-link-redirect="1" aria-haspopup="true">
                                        <?= Html::a('<i class="m-menu__link-icon flaticon-placeholder-3"></i><span class="m-menu__link-text">List Property</span>', ['properties/index'], ['class'=>'m-menu__link']) ?>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'news' ? 'm-menu__item--active' : ''; ?>" m-menu-link-redirect="1" aria-haspopup="true">
                                        <?= Html::a('<i class="m-menu__link-icon flaticon-feed"></i><span class="m-menu__link-text">List Property News</span>', ['news/index'], ['class'=>'m-menu__link']) ?>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item   m-menu__item--submenu m-menu__item--tabs <?= Yii::$app->controller->id == 'facilities' || Yii::$app->controller->id == 'amenities' ? 'm-menu__item--active m-menu__item--active-tab' : ''; ?>" m-menu-submenu-toggle="tab" aria-haspopup="true"><a href="javascript:;" class="m-menu__link m-menu__toggle"><span
                                 class="m-menu__link-text"> Master Data</span><i class="m-menu__hor-arrow la la-angle-down"></i><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                            <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left m-menu__submenu--tabs"><span class="m-menu__arrow m-menu__arrow--adjust"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'facilities' ? 'm-menu__item--active' : ''; ?> " m-menu-link-redirect="1" aria-haspopup="true">
                                        <?= Html::a('<i class="m-menu__link-icon flaticon-graphic-2"></i><span class="m-menu__link-text">Property Facilities</span>', ['facilities/index'], ['class'=>'m-menu__link']) ?>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'amenities' ? 'm-menu__item--active' : ''; ?>">
                                        <?= Html::a('<i class="m-menu__link-icon flaticon-graphic"></i><span class="m-menu__link-text">Property Amenities</span>', ['amenities/index'], ['class'=>'m-menu__link']) ?>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- end::Horizontal Menu -->
        </div>
    </div>
</div>