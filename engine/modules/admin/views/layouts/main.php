<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AdminAsset;

AdminAsset::register($this);
$theme = Url::base().'/assets/metronic';
$request = Yii::$app->request;
if ($request->enableCsrfValidation) {
    $this->registerMetaTag(['name' => 'csrf-param', 'content' => $request->csrfParam]);
    $this->registerMetaTag(['name' => 'csrf-token', 'content' => $request->getCsrfToken()]);
}
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

    <!-- begin::Head -->
    <head>
        <meta charset="utf-8" />
        <title>Prop Inside Admin - <?= $this->title; ?></title>
        <meta name="description" content="londreturah.com">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
        <?php $this->head() ?>
        <!--begin::Web font -->

        <script src="<?= $theme; ?>/admin/js/jquery.min.js"></script>
        <script src="<?= $theme; ?>/admin/js/webfont.js"></script>
        <script>
            WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700","Asap+Condensed:500"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>

        <!--end::Web font -->

        <!--begin::Global Theme Styles -->
        <link href="<?= $theme; ?>/admin/css/vendors.bundle.css" rel="stylesheet" type="text/css" />

        <!--RTL version:<link href="../../../assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
        <link href="<?= $theme; ?>/admin/css/style.bundle.css" rel="stylesheet" type="text/css" />
        <link href="<?= $theme; ?>/admin/css/customadmin.css" rel="stylesheet" type="text/css" />

        <!--RTL version:<link href="../../../assets/demo/default/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

        <!--end::Global Theme Styles -->

        <!--begin::Page Vendors Styles -->
        <link href="<?= $theme; ?>/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />

        <link rel="shortcut icon" href="<?= $theme; ?>/admin/images/favicon.ico" />
        <?php //Yii::app()->clientScript->registerCoreScript('jquery') ?>
    </head>

    <!-- end::Head -->

    <!-- begin::Body -->
    <body class="m-page--fluid m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
        <?php $this->beginBody() ?>
        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">

            <!-- begin::Header -->
            <header id="m_header" class="m-grid__item m-header " m-minimize="minimize" m-minimize-mobile="minimize" m-minimize-offset="10" m-minimize-mobile-offset="10">
                <div class="m-header__top">
                    <div class="m-container m-container--fluid m-container--full-height m-page__container">
                        <div class="m-stack m-stack--ver m-stack--desktop">

                            <!-- begin::Brand -->
                            <div class="m-stack__item m-brand m-stack__item--left">
                                <div class="m-stack m-stack--ver m-stack--general m-stack--inline">
                                    <div class="m-stack__item m-stack__item--middle m-brand__logo">
                                        <a href="<?= Url::to(['default/index']); ?>" class="m-brand__logo-wrapper">
                                            <!-- <img alt="" src="<?//= Url::base(); ?>/assets/metronic/demo/demo10/media/img/logo/logo.png" class="m-brand__logo-desktop"> -->
                                            <!-- <img alt="" src="<?//= Url::base(); ?>/assets/metronic/demo/demo10/media/img/logo/logo_mini.png" class="m-brand__logo-mobile"> -->
                                            <img alt="" src="<?= Url::base(); ?>/assets/img/logo.png" alt="logo" class="m-brand__logo-desktop" style="height:52px;">
                                            <img alt="" src="<?= Url::base(); ?>/assets/img/logo.png" alt="logo" class="m-brand__logo-mobile">
                                        </a>
                                    </div>
                                    <div class="m-stack__item m-stack__item--middle m-brand__tools">
                                        <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-left m-dropdown--align-push" m-dropdown-toggle="click" aria-expanded="true">
                                           <!--  Total Deposit : <span class="m-badge m-badge--danger m-badge--wide">Rp. 2.350.000 </span> -->
                                        </div>
                                        <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                                        <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                            <span></span>
                                        </a>

                                        <!-- END -->

                                        <!-- begin::Responsive Header Menu Toggler-->
                                        <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                                            <span></span>
                                        </a>

                                        <!-- end::Responsive Header Menu Toggler-->

                                        <!-- begin::Topbar Toggler-->
                                        <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                            <i class="flaticon-more"></i>
                                        </a>

                                        <!--end::Topbar Toggler-->
                                    </div>
                                </div>
                            </div>

                            <!-- end::Brand -->

                            <!-- begin::Topbar -->
                            <div class="m-stack__item m-stack__item--right m-header-head" id="m_header_nav">
                                <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
                                    <div class="m-stack__item m-topbar__nav-wrapper">
                                        <ul class="m-topbar__nav m-nav m-nav--inline">
                                            <li class="m-nav__item m-nav__item--danger m-dropdown m-dropdown--skin-light m-dropdown--large m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
                                                <a href="#" class="m-nav__link m-dropdown__toggle">
                                                    <span class="m-nav__link-badge m-badge m-badge--dot m-badge--info m--hide"></span>
                                                    <span class="m-nav__link-icon"><span class="m-nav__link-icon-wrapper"><i class="flaticon-share"></i></span></span>
                                                </a>
                                                <div class="m-dropdown__wrapper" style="z-index: 101;">
                                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 22.5px;"></span>
                                                    <div class="m-dropdown__inner">
                                                        <div class="m-dropdown__header m--align-center">
                                                            <div class="m-card-user m-card-user--skin-light">
                                                                <div class="m-card-user__pic">
                                                                    
                                                                </div>
                                                                <div class="m-card-user__details">
                                                                    <span class="m-card-user__name m--font-weight-500">Welcome, <?= Yii::$app->user->identity == null ? 'user' : Yii::$app->user->identity->username; ?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="m-dropdown__body">
                                                            <div class="m-dropdown__content">
                                                                <ul class="m-nav m-nav--skin-light">
                                                                   <!--  <li class="m-nav__item">
                                                                        <a href="profile.html" class="m-nav__link">
                                                                            <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                                            <span class="m-nav__link-text">Profil</span>
                                                                        </a>
                                                                    </li> -->
                                                                    <li class="m-nav__separator m-nav__separator--fit">
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <?= Html::a('Log Out',['default/logout'],[
                                                                                'class'=>'btn m-btn--pill btn-secondary m-btn m btn--custom m-btn--label-brand m-btn--bolder',
                                                                                'data'=>[
                                                                                    'method'=>'post'
                                                                                ]
                                                                            ]);
                                                                        ?>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- end::Topbar -->
                        </div>
                    </div>
                </div>
                <?php require_once('_topbar.php'); ?>
            </header>

            <!-- end::Header -->

            <!-- begin::Body -->
            <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop m-page__container m-body">

                <!-- BEGIN: Left Aside -->
                <button class="m-aside-left-close m-aside-left-close--skin-light" id="m_aside_left_close_btn"><i class="la la-close"></i></button>

                <!-- END: Left Aside -->
                <div class="m-grid__item m-grid__item--fluid m-wrapper">

                    <!-- BEGIN: Subheader -->
                    <div class="m-subheader ">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="m-subheader__title m-subheader__title--separator"><?= $this->title; ?></h3>
                                <?= Breadcrumbs::widget([
                                    'homeLink' => [
                                        'label' => '<i class="m-nav__link-icon la la-home"></i>',
                                        'url' => Yii::$app->getHomeUrl() . 'admin/default/index',
                                        'template' => '<li class="m-nav__item m-nav__item--home">{link}</li><li class="m-nav__separator">-</li>',
                                        'class'=>'m-nav__link m-nav__link--icon',
                                        'encode' => false,
                                    ],
                                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                    'activeItemTemplate' => '<li class="m-nav__item">{link}</li>',
                                    'itemTemplate' => '<li class="m-nav__item">{link}</li><li class="m-nav__separator">-</li>', // template for all links
                                    'options' => ['class'=>'m-subheader__breadcrumbs m-nav m-nav--inline'],
                                ]); ?>
                            </div>
                        </div>
                    </div>

                    <!-- END: Subheader -->
                    <div class="m-content">
                        <?php echo $content ?>
                    </div>
                </div>
            </div>

            <!-- end::Body -->

            <!-- begin::Footer -->
            <footer class="m-grid__item m-footer ">
                <div class="m-container m-container--fluid m-container--full-height m-page__container">
                    <div class="m-footer__wrapper">
                        <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                            <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                                <span class="m-footer__copyright">
                                    <?= date('Y'); ?> &copy; PropInside <!-- by <a href="http://propdemo.xyz" class="m-link">Safira Solution</a> -->
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

            <!-- end::Footer -->
        </div>
        
        <!-- end:: Page -->

        <!-- begin::Scroll Top -->
        <div id="m_scroll_top" class="m-scroll-top">
            <i class="la la-arrow-up"></i>
        </div>

        <!-- end::Scroll Top -->

        <!-- begin::Quick Nav -->

        <!--begin::Global Theme Bundle -->
        <script src="<?= $theme; ?>/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= $theme; ?>/admin/js/vendors.bundle.js" type="text/javascript"></script>
        <script src="<?= $theme; ?>/admin/js/scripts.bundle.js" type="text/javascript"></script>

        <!--end::Global Theme Bundle -->

        <!--begin::Page Vendors -->
        <script src="<?= $theme; ?>/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
        <script src="<?= $theme; ?>/vendors/custom/datatables/dataTables.checkboxes.min.js" type="text/javascript"></script>
        <!--end::Page Vendors -->
        
        <!--begin::Page Scripts -->
        <!-- <script src="<?= $theme; ?>/admin/js/dashboard.js" type="text/javascript"></script> -->
        <!-- <script src="<?= $theme; ?>/admin/js/data-local.js" type="text/javascript"></script> -->

        <!-- begin::Page Loader -->
        <script>
            $(window).on('load', function() {
                $('body').removeClass('m-page--loading');
            });
        </script>

        <script type="text/javascript">
        $(document).ready(function() {
            getData();
            $('.tanggal').datepicker({
                minDate:new Date(),
                autoclose: true
            });
            $(".select").select2({
                // minimumInputLength: 2,
            });
        });

        function getData() {
            var ctrl = '<?= Yii::$app->controller->id; ?>';
            if (ctrl == 'default') { return; } // karena di default ada 2 table yg sudah definisikan getData sendiri

            var url = '<?= Url::to([Yii::$app->controller->id.'/'.'getalldata']); ?>';
            $('#tablegrid').DataTable({
                "ajax": {
                    "url": url,
                    "data": function(d){
                        return JSON.stringify(d.data);
                    },
                },
                "lengthMenu": [
                    [5,10, 25, -1],
                    [5,10, 25, "All"]
                ],
                "pageLength": 10,
                "columnDefs": [{
                    'orderable': false,
                    'targets': [0]
                }, {
                    "searchable": true,
                    "targets": [0]
                }],
                "order": [
                    [0, "asc"]
                ],
                "processing": true,
            });
        }

        function deleteRow(id)
        {
            // var ctrl = '<?= Yii::$app->controller->id; ?>'+ '/delete/id/' + id;
            // var ctrl = '' ;
            var url = '<?= Url::to([Yii::$app->controller->id.'/setstatus/']); ?>';
            console.log("url :: ", url);
            swal({
                title: "Are you sure you want to delete this item?",
                text: "You will not be able to restore this item!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, please!",
                cancelButtonText: "No, cancel it!",
                reverseButtons: !0
            }).then(function(e) {
                if (e.value) {
                    $.ajax({
                        url: url,
                        type: "POST",
                        // dataType: "json",
                        data: {id: id},
                        success: function () {
                            swal("Success!", "Data is successfully deleted!", "success");
                            setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                            }, 1000); 
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            swal("Error!", "Please try again.", "error");
                        }
                    });
                } else {
                    swal("Cancel!", "Your data is still on the database.", "error");
                }
            });
        }

        function convertToRupiah(angka)
        {
            var rupiah = '';        
            var angkarev = angka.toString().split('').reverse().join('');
            for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
            var tot = rupiah.split('',rupiah.length-1).reverse().join('');
            return tot.toString();
        }

        function convertToAngka(rupiah)
        {
            return parseInt(rupiah.replace(/,.*|[^0-9]/g, ''), 10);
        }

        //Rekap Transaksi Harian admin
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true
        });
        </script>

        <!--end::Global Theme Bundle -->
        <?php $this->endBody() ?>
    </body>

    <!-- end::Body -->
</html>
<?php $this->endPage() ?>