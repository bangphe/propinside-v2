<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\PropertyAmenities;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\AccessRule;
use app\components\MyFormatter;

/**
 * AmenitiesController implements the CRUD actions for PropertyAmenities model.
 */
class AmenitiesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // 'ruleConfig' => [
                //     'class' => AccessRule::className(),
                // ],
                'only' => ['index', 'create', 'update', 'view', 'setstatus'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'view', 'setstatus'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id == 'setstatus') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * Lists all PropertyAmenities models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGetalldata()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => PropertyAmenities::find()
                // ->where(['not in', 'status', ['1']])
                ->where(['status'=>PropertyAmenities::STATUS_ACTIVE])
                ->orderBy(['id_amenities'=>SORT_ASC]),
            'pagination'=>false
        ]);

        $result = [];
        $data = [];
        $i = 1;
        foreach ($dataProvider->getModels() as $value) {
            $nestedData = [];
            $nestedData[] = $i;
            $nestedData[] = $value['amenities'];
            $nestedData[] = $value->getStatusLabel();
            $nestedData[] = PropertyAmenities::getActionButton($value['id_amenities']);

            $data[] = $nestedData;
            $i++;
        }

        $result = array(
            "data" => $data,
        );

        return \yii\helpers\Json::encode($result);
    }

    /**
     * Displays a single PropertyAmenities model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PropertyAmenities model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PropertyAmenities();

        if ($model->load(Yii::$app->request->post())) {
            $model->created_at = date('Y-m-d H:i:s');
            if ($model->save()) {
                Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Congrats!</strong> Data is successfully saved.'));
                return $this->redirect(['view', 'id' => $model->id_amenities]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PropertyAmenities model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Congrats!</strong> Data is successfully saved.'));
                return $this->redirect(['view', 'id' => $model->id_amenities]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PropertyAmenities model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionSetstatus()
    {
        $id = $_POST['id'];
        $model = $this->findModel($id);
        $model->status = PropertyAmenities::STATUS_NOT_ACTIVE;
        $model->save();
    }

    /**
     * Finds the PropertyAmenities model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PropertyAmenities the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PropertyAmenities::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
