<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Property;
use app\models\PropertyImage;
use app\models\PropertyListing;
use app\models\PropertyHasAmenities;
use app\models\PropertyHasFacilities;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\AccessRule;
use app\components\MyFormatter;

/**
 * PropertiesController implements the CRUD actions for Property model.
 */
class PropertiesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Property models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGetalldata()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Property::find()
                // ->where(['not in', 'status', [Property::STATUS_DELETED]])
                // ->where(['is_approved'=>0])
                ->where(['status'=>Property::STATUS_ACTIVE])
                ->orderBy(['id_property'=>SORT_DESC]),
            'pagination'=>false
        ]);

        $result = [];
        $data = [];
        $i = 1;
        foreach ($dataProvider->getModels() as $value) {
            $nestedData = [];
            $nestedData[] = $i;
            $nestedData[] = $value['title'];
            $nestedData[] = $value['price'];
            $nestedData[] = $value['location'];
            $nestedData[] = $value['type'];
            $nestedData[] = $value['developer'];
            $nestedData[] = $value['created_at'];
            $nestedData[] = $value->getStatusLabel();
            $nestedData[] = Property::getActionButton($value['id_property']);

            $data[] = $nestedData;
            $i++;
        }

        $result = array(
            "data" => $data,
        );

        return \yii\helpers\Json::encode($result);
    }

    /**
     * Displays a single Property model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Property model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $id = Yii::$app->user->id;
        $property = new Property();
        $property_images = new PropertyImage();

        if ($property->load(Yii::$app->request->post())) {
            // die(var_dump($_POST['Property']['prop_facilities']));
            $property->id_user = $id;
            $property->created_at = date('Y-m-d H:i:s');
            if ($property->save()) {
                $modelImages = $property_images->handleUploadedImages($property->id_property);
                $property->saveFacilities($property->id_property, $_POST['Property']['prop_facilities']);
                $property->saveAmenities($property->id_property, $_POST['Property']['prop_amenities']);
                Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Congrats!</strong> Data is successfully saved.'));
                return $this->redirect(['view', 'id' => $property->id_property]);
            }
        }

        return $this->render('create', [
            'property' => $property,
            'property_images' => $property_images,
        ]);
    }

    /**
     * Updates an existing Property model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $property = $this->findModel($id);
        $property_images = new PropertyImage();
        $uploadedImages = PropertyImage::find()
            ->where(['id_property' => $id])
            ->orderBy('sort_order ASC')
            ->all();

        if ($property->load(Yii::$app->request->post()) && $property->save()) {
            $modelImages = $property_images->handleUploadedImages($property->id_property);
            $property->saveFacilities($property->id_property, $_POST['Property']['prop_facilities']);
            $property->saveAmenities($property->id_property, $_POST['Property']['prop_amenities']);
            Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Congrats!</strong> Data is successfully saved.'));
            return $this->redirect(['view', 'id' => $property->id_property]);
        }

        return $this->render('update', [
            'property' => $property,
            'property_images' => $property_images,
            'uploadedImages' => $uploadedImages,
        ]);
    }

    /**
     * Deletes an existing Property model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->enableCsrfValidation = false;
        $property = $this->findModel($id);
        // $property->delete();
        // $property_listing = PropertyListing::deleteAll('id_property = :id_property', [':id_property' => $id]);
        // $property_amenities = PropertyHasAmenities::deleteAll('id_property = :id_property', [':id_property' => $id]);
        // $property_facilities = PropertyHasFacilities::deleteAll('id_property = :id_property', [':id_property' => $id]);
        // $property_news = PropertyNews::deleteAll('id_property = :id_property', [':id_property' => $id]);
        $property->status = Property::STATUS_DELETED;

        if ($property->save()) {
            Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Congrats!</strong> Data is successfully deleted.'));
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Property model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Property the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Property::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
