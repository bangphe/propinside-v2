<?php

namespace app\modules\admin\controllers;

use Yii;
use app\components\AccessRule;
use app\components\MyFormatter;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\helpers\Url;
use app\models\LoginForm;
use app\models\User;
use app\models\Agent;
use app\models\Property;
use yii\data\ActiveDataProvider;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
	/**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => [
                            'login',
                            'logout',
                            'getalldataagent',
                            'getalldataproperty',
                            'setstatus',
                            'setstatusprop',
                            'setallstatusagent',
                            'setallstatusprop',
                            'deleteallprop',
                        ],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                    'setstatus' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGetalldataagent()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Agent::find()
                // ->where(['not in', 'status', ['1']])
                // ->where(['is_approved'=>0])
                ->orderBy(['id_agent'=>SORT_DESC]),
            'pagination'=>false
        ]);

        $result = [];
        $data = [];
        $i = 1;
        foreach ($dataProvider->getModels() as $value) {
            $nestedData = [];
            $nestedData[] = $value['id_agent'];
            $nestedData[] = $value['name'];
            $nestedData[] = $value['username'];
            $nestedData[] = $value['email'];
            $nestedData[] = $value['title'];
            $nestedData[] = $value['phone_number'];
            $nestedData[] = $value['created_at'];
            $nestedData[] = $value->getStatusLabel();
            $nestedData[] = Agent::getStatusButton($value['id_agent'], $value['is_approved']);

            $data[] = $nestedData;
            $i++;
        }

        $result = array(
            "data" => $data,
        );

        return \yii\helpers\Json::encode($result);
    }

    public function actionGetalldataproperty()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Property::find()
                // ->where(['not in', 'status', ['1']])
                ->where(['status'=>Property::STATUS_ACTIVE])
                ->orderBy(['id_property'=>SORT_DESC]),
            'pagination'=>false
        ]);

        $result = [];
        $data = [];
        $i = 1;
        foreach ($dataProvider->getModels() as $value) {
            $nestedData = [];
            $nestedData[] = $value['id_property'];
            $nestedData[] = $value['title'];
            $nestedData[] = $value['price'];
            $nestedData[] = $value['location'];
            $nestedData[] = $value['type'];
            $nestedData[] = $value['developer'];
            $nestedData[] = $value['created_at'];
            $nestedData[] = $value->getStatusLabel();
            $nestedData[] = Property::getStatusButton($value['id_property'], $value['is_published'], $value['slug']);

            $data[] = $nestedData;
            $i++;
        }

        $result = array(
            "data" => $data,
        );

        return \yii\helpers\Json::encode($result);
    }

    public function actionSetstatus()
    {
        $id = $_POST['id'];
        $status = $_POST['status'];
        $model = $this->findModel($id);
        $model->is_approved = $status;
        $result = [];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($model->save()) {
            try {
                // send email
                $email = Yii::$app->mailer->compose(['html' =>'email'],['model'=>$model])
                    ->setFrom([Yii::$app->params['admin.email'] => 'Administrator Property Inside'])
                    ->setTo($model->email)
                    ->setSubject(Yii::$app->params['subject.registrasi'])
                    ->send();
            } catch (Exception $e) {
                throw new Exception("Error Processing Request", $e);
            }
            $result = ["result" => "success", "message" => "OK"];
            return $result;
        } else {
            $result = ["result" => "error", "message" => $model->getErrors()];
            return $result;
        }
    }

    public function actionSetallstatusagent()
    {
        $ids = $_POST['ids'];
        $exp = explode(',', $ids);
        $status = $_POST['status'];

        if(isset($ids)) {
            $result = Agent::find()->where(['in','id_agent',$exp])->all();
            foreach ($result as $key => $model) {
                // die(var_dump($model));
                $model['is_approved'] = $status;
                if ($model->save()) {
                    try {
                        // send email
                        $email = Yii::$app->mailer->compose(['html' =>'email'],['model'=>$model])
                            ->setFrom([Yii::$app->params['admin.email'] => 'Administrator Property Inside'])
                            ->setTo($model['email'])
                            ->setSubject(Yii::$app->params['subject.registrasi'])
                            ->send();
                    } catch (Exception $e) {
                        throw new Exception("Error Processing Request", $e);
                    }
                }
            }
        }
    }

    public function actionSetallstatusprop()
    {
        $ids = $_POST['ids'];
        $exp = explode(',', $ids);
        $status = $_POST['status'];

        $result = [];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if(isset($ids)) {
            // $result = Property::find()->where(['in','id_property',$exp])->all();
            // foreach ($result as $key => $model) {
            //     $model['is_published'] = $status;
            //     $model->save();
            // }
            Property::updateAll(['is_published' => $status], ['in','id_property',$exp]);
            $result = ["result" => "success", "message" => "OK"];
            return $result;
        }
    }

    public function actionDeleteallprop()
    {
        $ids = $_POST['ids'];
        $exp = explode(',', $ids);
        $status = $_POST['status'];

        $result = [];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if(isset($ids)) {
            Property::updateAll(['status' => $status], ['in','id_property',$exp]);
            $result = ["result" => "success", "message" => "OK"];
            return $result;
        }
    }

    public function actionDeleteallagent()
    {
        $ids = $_POST['ids'];
        $exp = explode(',', $ids);
        $status = $_POST['status'];
        die(var_dump($_POST));

        $result = [];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if(isset($ids)) {
            Property::updateAll(['status' => $status], ['in','id_property',$exp]);
            $result = ["result" => "success", "message" => "OK"];
            return $result;
        }
    }

    public function actionSetstatusprop()
    {
        $id = $_POST['id'];
        $status = $_POST['status'];
        $model = $this->findModelProp($id);
        $model->is_published = $status;
        $model->save();
    }

    public function actionError()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $model = new LoginForm();

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['default/index']);
        }
        
        return $this->renderPartial('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        // die(var_dump(Yii::$app->user->loginUrl[0]));
        Yii::$app->user->logout();

        return $this->redirect([Yii::$app->user->loginUrl[0]]);
    }

    /**
     * Finds the Agent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Agent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Agent::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelProp($id)
    {
        if (($model = Property::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
