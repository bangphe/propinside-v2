<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Agent;
use app\models\PropertyListing;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\AccessRule;
use app\components\MyFormatter;

/**
 * AgentsController implements the CRUD actions for Agent model.
 */
class AgentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // 'ruleConfig' => [
                //     'class' => AccessRule::className(),
                // ],
                'only' => ['index', 'create', 'update', 'view', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'view', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Agent models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGetalldata()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Agent::find()
                // ->where(['not in', 'status', ['1']])
                // ->where(['is_approved'=>0])
                ->orderBy(['id_agent'=>SORT_DESC]),
            'pagination'=>false
        ]);

        $result = [];
        $data = [];
        $i = 1;
        foreach ($dataProvider->getModels() as $value) {
            $nestedData = [];
            $nestedData[] = $i;
            $nestedData[] = $value['name'];
            $nestedData[] = $value['username'];
            $nestedData[] = $value['email'];
            $nestedData[] = $value['title'];
            $nestedData[] = $value['phone_number'];
            $nestedData[] = $value['created_at'];
            $nestedData[] = $value->getStatusLabel();
            $nestedData[] = Agent::getActionButton($value['id_agent'], $value['is_approved']);

            $data[] = $nestedData;
            $i++;
        }

        $result = array(
            "data" => $data,
        );

        return \yii\helpers\Json::encode($result);
    }

    /**
     * Displays a single Agent model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Agent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Agent();

        if ($model->load(Yii::$app->request->post())) {
            $model->password = md5($model->password);
            $model->confirm_password_login = md5($model->password);
            $model->created_at = date('Y-m-d H:i:s');
            if ($model->save()) {
                Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Congrats!</strong> Data is successfully saved.'));
                return $this->redirect(['view', 'id' => $model->id_agent]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Agent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Congrats!</strong> Data is successfully saved.'));
            return $this->redirect(['view', 'id' => $model->id_agent]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Agent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->enableCsrfValidation = false;
        $agent = $this->findModel($id);
        $agent->delete();
        $property_listing = PropertyListing::deleteAll('id_agent = :id_agent', [':id_agent' => $id]);

        Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Congrats!</strong> Data is successfully deleted.'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the Agent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Agent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Agent::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
