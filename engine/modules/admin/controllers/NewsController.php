<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\PropertyNews;
use app\models\PropertyNewsGallery;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\AccessRule;
use app\components\MyFormatter;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * NewsController implements the CRUD actions for PropertyNews model.
 */
class NewsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // 'ruleConfig' => [
                //     'class' => AccessRule::className(),
                // ],
                'only' => ['index', 'create', 'update', 'view', 'remove-image', 'delete', 'upload'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'view', 'remove-image', 'delete', 'upload'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        if ($action->id !== "upload") {
            $this->enableCsrfValidation = false;
        } elseif ($action->id !== "remove-image") {
            $this->enableCsrfValidation = false;
        } else {
            $this->enableCsrfValidation = true;
        }
        // $this->enableCsrfValidation = ($action->id !== "upload");
        return parent::beforeAction($action);
    }

    public function actionUpload()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $error = null;
        $models = [];
        $images = new PropertyNewsGallery();

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $models = $images->handleUploadedImages((int)Yii::$app->request->post('id', 0));
            $transaction->commit();

        } catch (\Exception $e) {
            $error = $e->getMessage() . ' at ' . $e->getFile() . ":" . $e->getLine() . PHP_EOL . $e->getTraceAsString();
            $transaction->rollBack();
        }

        if (!$error && $models) {
            $model = reset($models);
            $file_explode = explode('/', $model->image_path);
            $file_name = $file_explode[4];
            $resp = [
                'initialPreview' => [
                    Yii::getAlias('@web'.$model->image_path),
                ],
                'initialPreviewConfig' => [[
                    'caption' => $file_name,
                    'url' => Url::to(['/agents/remove-image']),
                    'key' => $model->id_property_news_gallery
                ]]
            ];
        } else {
            $resp = ['error' => $error ? $error : ($images->hasErrors() ? $images->getFirstErrors() : 'Unknown')];
        }

        return $resp;
    }

    /**
     * @return array|Response
     */
    public function actionRemoveImage()
    {
        /* allow only ajax calls */
        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['admin/news/create']);
        }

        /* set the output to json */
        Yii::$app->response->format = Response::FORMAT_JSON;

        $imageId = Yii::$app->request->post('key');
        if (empty($imageId)) {
            return ['result' => 'error', 'html' => Yii::t('app', 'Something went wrong...')];
        }

        $imageId = PropertyImage::findOne(['id_image' => $imageId]);
        if (empty($imageId)) {
            return ['result' => 'error', 'html' => Yii::t('app', 'Something went wrong...')];
        }

        // if all images are deleted then the prop is not active anymore
        // $countImages = PropertyImage::find()->where(['id_property' => $imageId->id_property])->count();
        // if ($countImages == 1) {
        //     $prop = Property::find()->where(['id_property' => $imageId->id_property])->one();
        //     $prop->status = Property::STATUS_NOT_ACTIVE;
        //     $prop->save(false);
        // }

        $imageId->delete();

        return ['result' => 'success', 'response' => $imageId];
    }

    /**
     * Lists all PropertyNews models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGetalldata()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => PropertyNews::find()
                // ->where(['not in', 'status', ['1']])
                // ->where(['is_approved'=>0])
                ->orderBy(['id_property_news'=>SORT_DESC]),
            'pagination'=>false
        ]);

        $result = [];
        $data = [];
        $i = 1;
        foreach ($dataProvider->getModels() as $value) {
            $nestedData = [];
            $nestedData[] = $i;
            $nestedData[] = $value['title'];
            $nestedData[] = $value->property['title'];
            $nestedData[] = $value->property['location'];
            // $nestedData[] = $value['image_path'];
            $nestedData[] = $value['created_at'];
            $nestedData[] = $value->getStatusLabel();
            $nestedData[] = PropertyNews::getActionButton($value['id_property_news']);

            $data[] = $nestedData;
            $i++;
        }

        $result = array(
            "data" => $data,
        );

        return \yii\helpers\Json::encode($result);
    }

    /**
     * Displays a single PropertyNews model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PropertyNews model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PropertyNews();
        $gallery = new PropertyNewsGallery();

        if ($model->load(Yii::$app->request->post())) {
            $model->id_user = Yii::$app->user->identity->id;
            $model->created_at = date('Y-m-d H:i:s');
            if ($model->save()) {
                $modelGallery = $gallery->handleUploadedImages($model->id_property_news);
                Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Congrats!</strong> Data is successfully saved.'));
                return $this->redirect(['view', 'id' => $model->id_property_news]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'gallery' => $gallery,
        ]);
    }

    /**
     * Updates an existing PropertyNews model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $gallery = new PropertyNewsGallery();
        $uploadedImages = PropertyNewsGallery::find()
            ->where(['id_property_news' => $id])
            ->orderBy('sort_order ASC')
            ->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_property_news]);
        }

        return $this->render('update', [
            'model' => $model,
            'gallery' => $gallery,
            'uploadedImages' => $uploadedImages,
        ]);
    }

    /**
     * Deletes an existing PropertyNews model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->enableCsrfValidation = false;
        $news_gallery = PropertyNewsGallery::deleteAll('id_property_news = :id', [':id' => $id]);
        $news = $this->findModel($id);
        if ($news->delete()) { 
            Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Congrats!</strong> Data is successfully deleted.'));
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the PropertyNews model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PropertyNews the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PropertyNews::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
