<?php

return [
    'admin.email' => 'info@propdemo.xyz',
    'subject.registrasi' => 'Account User Approved - Property Inside',
    'site.author' => '@bangphe',
    'site.url' => 'http://propdemo.xyz',
    'site.name' => 'Property Inside '.date("Y"),
];
