<?php

Yii::setAlias('@webdir', realpath(dirname(__FILE__) . '/../../'));

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'PropInside',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    // 'layout' => '@webroot/engine/themes/prop/index',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        // '@theme' => '/engine/themes/prop',
        // '@assets' => '../../assets',
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
            'defaultRoute' => 'default',
        ],
    ],
    'components' => [
        'reCaptcha' => [
            'class' => 'himiklab\yii2\recaptcha\ReCaptchaConfig',
            'siteKeyV2' => '6LfacKEUAAAAAII5HjFLkk4bBz9fFLPnRJFF46_s',
            'secretV2' => '6LfacKEUAAAAAD29x-bPkM3vGm833Nd698iLD0c6',
            // 'siteKeyV3' => '6LfctqAUAAAAAJivbLYlQZbm3mBbzYovxpJVNNwg',
            // 'secretV3' => '6LfctqAUAAAAAFwLfaCMlYVoZYztpuCl3377qG9E',
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                ],
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                ],
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '3okiPsigyrP7VHEuGKhssGsuKjaZ1Jab',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'session' => [
            'class' => 'yii\web\Session',
            'cookieParams' => ['httponly' => true, 'lifetime' => 3600 * 4],
            'timeout' => 3600*4, //session expire
            'useCookies' => true,
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'viewPath' => '@app/mail',
            'transport' => [
                 // 'class' => 'Swift_SmtpTransport',
                 // 'host' => 'mail.propdemo.xyz',
                 // 'username' => 'info@propdemo.xyz',
                 // 'password' => 'Propdemo123456',
                 // 'port' => 587,
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.propdemo.xyz',
                'username' => 'info@propdemo.xyz',
                'password' => 'Bismillah1',
                'port' => 587,
                'encryption' => 'tls',
             ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'assetManager' => [
            'baseUrl' => '@web/assets/cache',
            'basePath' => '@webroot/assets/cache',
            'linkAssets' => false,
            'appendTimestamp' => true,
            // 'assetMap' => [
                // 'jquery.js' => 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js',
            // ],
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js'=>['jquery.min.js'],
                    'jsOptions' => ['position' => \yii\web\View::POS_HEAD]
                ],
                // 'yii\bootstrap\BootstrapPluginAsset' => [
                //     'js'=>[]
                // ],
                // 'yii\bootstrap\BootstrapAsset' => [
                //     'css' => []
                // ],
            ],
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => array(
                'index' => 'site/index',
                'login' => 'site/login',
                'register' => 'site/register',
                'about-us' => 'site/about-us',
                'contact-us' => 'site/contact-us',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                'agents/property/<slug>' => 'agents/property',
            ),
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'enableSession' => true,
            'identityCookie' => ['name' => '_identity_user'],
            'loginUrl' => ['/admin/default/login'],
        ],
        'agent' => [
            'class' => 'app\yii\web\WebAgent',
            'identityClass' => 'app\models\Agent',
            'enableAutoLogin' => true,
            'enableSession' => true,
            'identityCookie' => ['name' => '_identity_agent'],
            'idParam' => '__agent_id',
            'loginUrl' => ['/site/login'],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    // $config['bootstrap'][] = 'debug';
    // $config['modules']['debug'] = [
        // 'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    // ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
