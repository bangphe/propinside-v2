/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100121
Source Host           : localhost:3306
Source Database       : propinside_db

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2019-03-04 17:00:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for property_news
-- ----------------------------
DROP TABLE IF EXISTS `property_news`;
CREATE TABLE `property_news` (
  `id_property_news` int(11) NOT NULL AUTO_INCREMENT,
  `id_property` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `youtube` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `status` int(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_property_news`),
  KEY `id_property` (`id_property`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `property_news_ibfk_1` FOREIGN KEY (`id_property`) REFERENCES `property` (`id_property`) ON UPDATE CASCADE,
  CONSTRAINT `property_news_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
