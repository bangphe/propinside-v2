/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100121
Source Host           : localhost:3306
Source Database       : propinside_db

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2019-03-01 15:06:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for agent
-- ----------------------------
DROP TABLE IF EXISTS `agent`;
CREATE TABLE `agent` (
  `id_agent` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `photo` varchar(255) DEFAULT NULL,
  `registration_number` varchar(30) DEFAULT NULL,
  `title` varchar(30) DEFAULT NULL,
  `description` text,
  `phone_number` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `award` varchar(255) DEFAULT NULL,
  `speciality` varchar(30) NOT NULL,
  `country_operability` varchar(50) DEFAULT NULL,
  `investment_deposit_range` varchar(30) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `linkedin` varchar(255) DEFAULT NULL,
  `total_clicks` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `is_approved` int(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_agent`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agent
-- ----------------------------
INSERT INTO `agent` VALUES ('1', 'Test Agents', 'agent', 'a8f5f167f44f4964e6c998dee827110c', '/propinside-v2/uploads/images/agent/2e7f424706539_460s.jpg', null, 'President', '', '12345678', 'agent@mail.coms', 'asd', 'asd', 'Indonesia', 'Jawa Timur', '', '', '', null, '1', '1', '2019-02-28 14:47:50');
INSERT INTO `agent` VALUES ('2', 'asd', 'asd', 'a8f5f167f44f4964e6c998dee827110c', null, null, '', null, '2134234234', 'ma@mail.com', '', 'asdasdasdasd', '', '', null, null, null, null, '1', '0', '2019-02-27 10:51:31');

-- ----------------------------
-- Table structure for country
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `id_country` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `code` char(3) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_country`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of country
-- ----------------------------

-- ----------------------------
-- Table structure for property
-- ----------------------------
DROP TABLE IF EXISTS `property`;
CREATE TABLE `property` (
  `id_property` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `slug` varchar(50) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `location` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `tenure` varchar(100) NOT NULL,
  `floor_size` varchar(20) DEFAULT NULL,
  `developer` varchar(100) DEFAULT NULL,
  `land_size` varchar(20) DEFAULT NULL,
  `psf` varchar(20) DEFAULT NULL,
  `furnishing` varchar(50) DEFAULT NULL,
  `top` varchar(10) DEFAULT NULL,
  `floor_level` varchar(30) DEFAULT NULL,
  `listing_id` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `total_clicks` int(11) DEFAULT NULL,
  `is_published` int(1) DEFAULT '0',
  `status` int(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_property`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `property_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of property
-- ----------------------------

-- ----------------------------
-- Table structure for property_amenities
-- ----------------------------
DROP TABLE IF EXISTS `property_amenities`;
CREATE TABLE `property_amenities` (
  `id_amenities` int(11) NOT NULL AUTO_INCREMENT,
  `amenities` varchar(150) NOT NULL,
  `icons` varchar(20) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_amenities`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of property_amenities
-- ----------------------------
INSERT INTO `property_amenities` VALUES ('1', 'Washer and Dryer', null, '1', null);
INSERT INTO `property_amenities` VALUES ('2', 'Swimming Pool', null, '1', null);
INSERT INTO `property_amenities` VALUES ('3', 'Laundry Room', null, '1', null);
INSERT INTO `property_amenities` VALUES ('4', 'Home Theater', null, '1', null);

-- ----------------------------
-- Table structure for property_facilities
-- ----------------------------
DROP TABLE IF EXISTS `property_facilities`;
CREATE TABLE `property_facilities` (
  `id_facilities` int(11) NOT NULL AUTO_INCREMENT,
  `facilities` varchar(100) NOT NULL,
  `icons` varchar(20) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_facilities`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of property_facilities
-- ----------------------------
INSERT INTO `property_facilities` VALUES ('1', 'Air Conditioning', null, '1', null);
INSERT INTO `property_facilities` VALUES ('2', 'Balcony', null, '1', null);
INSERT INTO `property_facilities` VALUES ('3', 'Gas Heat', null, '1', null);
INSERT INTO `property_facilities` VALUES ('4', 'Fire Place', null, '1', null);

-- ----------------------------
-- Table structure for property_has_amenities
-- ----------------------------
DROP TABLE IF EXISTS `property_has_amenities`;
CREATE TABLE `property_has_amenities` (
  `id_property` int(11) NOT NULL,
  `id_amenities` int(11) NOT NULL,
  PRIMARY KEY (`id_property`,`id_amenities`),
  KEY `id_amenities` (`id_amenities`),
  CONSTRAINT `property_has_amenities_ibfk_1` FOREIGN KEY (`id_property`) REFERENCES `property` (`id_property`) ON UPDATE CASCADE,
  CONSTRAINT `property_has_amenities_ibfk_2` FOREIGN KEY (`id_amenities`) REFERENCES `property_amenities` (`id_amenities`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of property_has_amenities
-- ----------------------------

-- ----------------------------
-- Table structure for property_has_facilities
-- ----------------------------
DROP TABLE IF EXISTS `property_has_facilities`;
CREATE TABLE `property_has_facilities` (
  `id_property` int(11) NOT NULL,
  `id_facilities` int(11) NOT NULL,
  PRIMARY KEY (`id_property`,`id_facilities`),
  KEY `id_facilities` (`id_facilities`),
  CONSTRAINT `property_has_facilities_ibfk_1` FOREIGN KEY (`id_property`) REFERENCES `property` (`id_property`) ON UPDATE CASCADE,
  CONSTRAINT `property_has_facilities_ibfk_2` FOREIGN KEY (`id_facilities`) REFERENCES `property_facilities` (`id_facilities`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of property_has_facilities
-- ----------------------------

-- ----------------------------
-- Table structure for property_image
-- ----------------------------
DROP TABLE IF EXISTS `property_image`;
CREATE TABLE `property_image` (
  `id_image` int(11) NOT NULL AUTO_INCREMENT,
  `id_property` int(11) NOT NULL,
  `image_path` varchar(255) NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_image`),
  KEY `id_property` (`id_property`),
  CONSTRAINT `property_image_ibfk_1` FOREIGN KEY (`id_property`) REFERENCES `property` (`id_property`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of property_image
-- ----------------------------

-- ----------------------------
-- Table structure for property_listing
-- ----------------------------
DROP TABLE IF EXISTS `property_listing`;
CREATE TABLE `property_listing` (
  `id_listing` int(11) NOT NULL AUTO_INCREMENT,
  `id_property` int(11) DEFAULT NULL,
  `id_agent` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_listing`),
  KEY `id_property` (`id_property`),
  KEY `id_agent` (`id_agent`),
  CONSTRAINT `property_listing_ibfk_1` FOREIGN KEY (`id_property`) REFERENCES `property` (`id_property`) ON UPDATE CASCADE,
  CONSTRAINT `property_listing_ibfk_2` FOREIGN KEY (`id_agent`) REFERENCES `agent` (`id_agent`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of property_listing
-- ----------------------------

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(30) NOT NULL,
  `status` int(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'Admin', '1', '2019-02-21 09:10:05');
INSERT INTO `role` VALUES ('2', 'User', '1', '2019-02-21 09:10:29');
INSERT INTO `role` VALUES ('3', 'Agent', '1', '2019-02-21 09:10:32');
INSERT INTO `role` VALUES ('4', 'Investor', '1', '2019-02-21 09:10:34');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `id_role` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` int(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_user`),
  KEY `id_role` (`id_role`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1', 'admin', 'admin@mail.com', 'a8f5f167f44f4964e6c998dee827110c', '1', '2019-02-26 21:25:29');
