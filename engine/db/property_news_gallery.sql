/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100121
Source Host           : localhost:3306
Source Database       : propinside_db

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2019-03-18 13:08:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for property_news_gallery
-- ----------------------------
DROP TABLE IF EXISTS `property_news_gallery`;
CREATE TABLE `property_news_gallery` (
  `id_property_news_gallery` int(11) NOT NULL AUTO_INCREMENT,
  `id_property_news` int(11) NOT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_property_news_gallery`),
  KEY `id_property_news` (`id_property_news`),
  CONSTRAINT `property_news_gallery_ibfk_1` FOREIGN KEY (`id_property_news`) REFERENCES `property_news` (`id_property_news`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
