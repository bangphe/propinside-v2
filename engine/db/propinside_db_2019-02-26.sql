# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.37-MariaDB)
# Database: propinside_db
# Generation Time: 2019-02-26 14:29:27 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table agent
# ------------------------------------------------------------

DROP TABLE IF EXISTS `agent`;

CREATE TABLE `agent` (
  `id_agent` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `photo` varchar(255) DEFAULT NULL,
  `registration_number` varchar(30) DEFAULT NULL,
  `title` varchar(30) DEFAULT NULL,
  `description` text,
  `phone_number` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `award` varchar(255) DEFAULT NULL,
  `speciality` varchar(30) NOT NULL,
  `country_operability` varchar(50) DEFAULT NULL,
  `investment_deposit_range` varchar(30) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `linkedin` varchar(255) DEFAULT NULL,
  `total_clicks` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `is_approved` int(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_agent`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `agent` WRITE;
/*!40000 ALTER TABLE `agent` DISABLE KEYS */;

INSERT INTO `agent` (`id_agent`, `name`, `username`, `password`, `photo`, `registration_number`, `title`, `description`, `phone_number`, `email`, `award`, `speciality`, `country_operability`, `investment_deposit_range`, `facebook`, `twitter`, `linkedin`, `total_clicks`, `status`, `is_approved`, `created_at`)
VALUES
	(1,'Rizky Purnawan','bangphe','a8f5f167f44f4964e6c998dee827110c',NULL,NULL,'President',NULL,'0318947828','bangphe90@gmail.com','asd','asd','Indonesia','Jawa Timur',NULL,NULL,NULL,NULL,1,0,'2019-02-26 15:24:27');

/*!40000 ALTER TABLE `agent` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table country
# ------------------------------------------------------------

DROP TABLE IF EXISTS `country`;

CREATE TABLE `country` (
  `id_country` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `code` char(3) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_country`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table property
# ------------------------------------------------------------

DROP TABLE IF EXISTS `property`;

CREATE TABLE `property` (
  `id_property` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `slug` varchar(50) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `location` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `tenure` varchar(100) NOT NULL,
  `floor_size` varchar(20) DEFAULT NULL,
  `developer` varchar(100) DEFAULT NULL,
  `land_size` varchar(20) DEFAULT NULL,
  `psf` varchar(20) DEFAULT NULL,
  `furnishing` varchar(50) DEFAULT NULL,
  `top` varchar(10) DEFAULT NULL,
  `floor_level` varchar(30) DEFAULT NULL,
  `listing_id` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `total_clicks` int(11) DEFAULT NULL,
  `is_published` int(1) DEFAULT '0',
  `status` int(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_property`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `property_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table property_amenities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `property_amenities`;

CREATE TABLE `property_amenities` (
  `id_amenities` int(11) NOT NULL AUTO_INCREMENT,
  `amenities` varchar(150) NOT NULL,
  `icons` varchar(20) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_amenities`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table property_facilities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `property_facilities`;

CREATE TABLE `property_facilities` (
  `id_facilities` int(11) NOT NULL AUTO_INCREMENT,
  `facilities` varchar(100) NOT NULL,
  `icons` varchar(20) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_facilities`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table property_has_amenities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `property_has_amenities`;

CREATE TABLE `property_has_amenities` (
  `id_property` int(11) NOT NULL,
  `id_amenities` int(11) NOT NULL,
  PRIMARY KEY (`id_property`,`id_amenities`),
  KEY `id_amenities` (`id_amenities`),
  CONSTRAINT `property_has_amenities_ibfk_1` FOREIGN KEY (`id_property`) REFERENCES `property` (`id_property`) ON UPDATE CASCADE,
  CONSTRAINT `property_has_amenities_ibfk_2` FOREIGN KEY (`id_amenities`) REFERENCES `property_amenities` (`id_amenities`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table property_has_facilities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `property_has_facilities`;

CREATE TABLE `property_has_facilities` (
  `id_property` int(11) NOT NULL,
  `id_facilities` int(11) NOT NULL,
  PRIMARY KEY (`id_property`,`id_facilities`),
  KEY `id_facilities` (`id_facilities`),
  CONSTRAINT `property_has_facilities_ibfk_1` FOREIGN KEY (`id_property`) REFERENCES `property` (`id_property`) ON UPDATE CASCADE,
  CONSTRAINT `property_has_facilities_ibfk_2` FOREIGN KEY (`id_facilities`) REFERENCES `property_facilities` (`id_facilities`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table property_image
# ------------------------------------------------------------

DROP TABLE IF EXISTS `property_image`;

CREATE TABLE `property_image` (
  `id_image` int(11) NOT NULL AUTO_INCREMENT,
  `id_property` int(11) NOT NULL,
  `image_path` varchar(255) NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_image`),
  KEY `id_property` (`id_property`),
  CONSTRAINT `property_image_ibfk_1` FOREIGN KEY (`id_property`) REFERENCES `property` (`id_property`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table property_listing
# ------------------------------------------------------------

DROP TABLE IF EXISTS `property_listing`;

CREATE TABLE `property_listing` (
  `id_listing` int(11) NOT NULL AUTO_INCREMENT,
  `id_property` int(11) DEFAULT NULL,
  `id_agent` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_listing`),
  KEY `id_property` (`id_property`),
  KEY `id_agent` (`id_agent`),
  CONSTRAINT `property_listing_ibfk_1` FOREIGN KEY (`id_property`) REFERENCES `property` (`id_property`) ON UPDATE CASCADE,
  CONSTRAINT `property_listing_ibfk_2` FOREIGN KEY (`id_agent`) REFERENCES `agent` (`id_agent`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(30) NOT NULL,
  `status` int(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;

INSERT INTO `role` (`id_role`, `role`, `status`, `created_at`)
VALUES
	(1,'Admin',1,'2019-02-21 09:10:05'),
	(2,'User',1,'2019-02-21 09:10:29'),
	(3,'Agent',1,'2019-02-21 09:10:32'),
	(4,'Investor',1,'2019-02-21 09:10:34');

/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `id_role` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` int(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_user`),
  KEY `id_role` (`id_role`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id_user`, `id_role`, `username`, `email`, `password`, `status`, `created_at`)
VALUES
	(1,1,'admin','admin@mail.com','a8f5f167f44f4964e6c998dee827110c',1,'2019-02-26 21:25:29');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
