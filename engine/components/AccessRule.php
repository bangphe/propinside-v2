<?php

namespace app\components;


class AccessRule extends \yii\filters\AccessRule {

    /**
     * @inheritdoc
     */
    protected function matchRole($agent)
    {
        if (empty($this->roles)) {
            return true;
        }
        foreach ($this->roles as $role) {
            if ($role === '?') {
                if ($agent->getIsGuest()) {
                    return true;
                }
            } elseif ($role === '@') {
                if (!$agent->getIsGuest()) {
                    return true;
                }
            // Check if the agent is logged in, and the roles match
            } elseif (!$agent->getIsGuest() && $role === $agent->identity->ID_ROLE) {
                return true;
            }
        }

        return false;
    }
}
