<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "property_news".
 *
 * @property int $id_property_news
 * @property int $id_property
 * @property string $title
 * @property string $description
 * @property string $slug
 * @property string $youtube_1
 * @property string $youtube_2
 * @property int $id_user
 * @property int $status
 * @property string $created_at
 *
 * @property Property $property
 * @property User $user
 */
class PropertyNews extends \yii\db\ActiveRecord
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property_news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_property', 'title', 'description', 'id_user'], 'required'],
            [['id_property', 'id_user', 'status'], 'integer'],
            [['description'], 'string'],
            [['created_at'], 'safe'],
            [['title'], 'string', 'max' => 100],
            [['slug', 'youtube_1', 'youtube_2'], 'string', 'max' => 255],
            [['id_property'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['id_property' => 'id_property']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id_user']],
            // [['image_path'],
            //     'file',
            //     'mimeTypes' => 'image/jpeg, image/png, image/jpg',
            //     'checkExtensionByMimeType' => true,
            //     'wrongMimeType' => \Yii::t('app','Please try to upload an image.')
            // ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_property_news' => 'Id Property News',
            'id_property' => 'Id Property',
            'title' => 'Title',
            'description' => 'Description',
            'slug' => 'Slug',
            'youtube_1' => 'Youtube 1',
            'youtube_2' => 'Youtube 2',
            'image_path' => 'Image Path',
            'id_user' => 'Id User',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                // 'slugAttribute' => 'slug',
                'immutable' => true,
                'ensureUnique'=>true,
            ],
        ];
    }

    /**
     * @return bool
     */
    // public function afterValidate()
    // {
    //     parent::afterValidate();

    //     $this->handleUploadedImage();

    //     return true;
    // }

    /**
     * handleUploadedImage
     */
    // protected function handleUploadedImage()
    // {
    //     if ($this->hasErrors()) {
    //         return;
    //     }

    //     if (!($image = UploadedFile::getInstance($this, 'image_path'))) {
    //         return;
    //     }

    //     $storagePath = Yii::getAlias('@webroot/uploads/images/news');
    //     if (!file_exists($storagePath) || !is_dir($storagePath)) {
    //         if (!@mkdir($storagePath, 0777, true)) {
    //             \Yii::t('app', 'The images storage directory({path} does not exists and cannot be created!', ['path' =>$storagePath]);
    //             return;
    //         }
    //     }

    //     $newImageName = $image->name;
    //     $image->saveAs($storagePath . '/' . $newImageName);
    //     if (!is_file($storagePath . '/' . $newImageName)) {
    //         \Yii::t('app', 'Cannot move the avatar into the correct storage folder!');
    //         return;
    //     }
    //     $existing_file = $storagePath . '/' . $newImageName;
    //     $newImageName = substr(sha1(time()),0,6) . $newImageName;
    //     rename($existing_file , $storagePath . '/' . $newImageName );
    //     $avatar = str_replace('Upload', '', $image);
    //     $this->image_path = Yii::getAlias('@web/uploads/images/news/' . $newImageName);
    // }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id_property' => 'id_property']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyNewsGalleries()
    {
        return $this->hasMany(PropertyNewsGallery::className(), ['id_property_news' => 'id_property_news']);
    }

    public static function getActionButton($id)
    {
        $view = Html::a('<i class="fa fa-search"></i>', ['view','id'=>$id], ['class'=>'btn btn-secondary m-btn m-btn--icon m-btn--icon-only']);
        $update = Html::a('<i class="fa fa-pencil-alt"></i>', ['update','id'=>$id], ['class'=>'btn btn-success m-btn m-btn--icon m-btn--icon-only']);
        $delete = Html::a('<i class="fa fa-trash"></i>', ['delete','id'=>$id],
            [
                'class'=>'btn btn-danger m-btn m-btn--icon m-btn--icon-only',
                'data'=>[
                    'method'=>'post',
                    'confirm'=>'Are you sure to delete this data?',
                ]
            ]);

        return $view . ' ' . $update . ' ' .$delete;
    }

    public function getStatusLabel()
    {
        if($this->status==self::STATUS_ACTIVE)
            return '<span class="m-badge m-badge--success m-badge--wide">Active</span>';
        else
            return '<span class="m-badge m-badge--warning m-badge--wide">Not Active</span>';
    }

    /**
     * @param $slug
     * @return array|null|\yii\db\ActiveRecord
     */
    public function findBySlug($slug)
    {
        return self::find()->where([
            'slug' => $slug,
        ])->one();
    }

    public function getYoutubeID1()
    {
        if ($this->youtube_1 == NULL) {
            return '';
        }
        preg_match(
            '/[\\?\\&]v=([^\\?\\&]+)/',
            $this->youtube_1,
            $matches
        );
        return $matches[1];
    }

    public function getYoutubeIframe1($width, $height)
    {
        return '<iframe id="video1" class="lg-video-object lg-youtube " width="'.$width.'" height="'.$height.'" src="https://www.youtube.com/embed/'.$this->getYoutubeID1().'?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
    }

    public function getYoutubeID2()
    {
        if ($this->youtube_2 == NULL) {
            return '';
        }
        preg_match(
            '/[\\?\\&]v=([^\\?\\&]+)/',
            $this->youtube_2,
            $matches
        );
        return $matches[1];
    }

    public function getYoutubeIframe2($width, $height)
    {
        return '<iframe class="lg-video-object lg-youtube " width="'.$width.'" height="'.$height.'" src="https://www.youtube.com/embed/'.$this->getYoutubeID2().'?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
    }

    public function getYoutubeThumbnails1()
    {
        return 'http://img.youtube.com/vi/'.$this->getYoutubeID1().'/maxresdefault.jpg';
    }

    public function getYoutubeThumbnails2()
    {
        return 'http://img.youtube.com/vi/'.$this->getYoutubeID2().'/0.jpg';
    }
}
