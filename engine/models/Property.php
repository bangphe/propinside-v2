<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\behaviors\SluggableBehavior;
use app\models\PropertyHasFacilities;

/**
 * This is the model class for table "property".
 *
 * @property int $id_property
 * @property string $title
 * @property string $description
 * @property string $slug
 * @property int $price
 * @property string $location
 * @property string $type
 * @property string $tenure
 * @property string $floor_size
 * @property string $developer
 * @property string $land_size
 * @property string $psf
 * @property string $furnishing
 * @property string $top
 * @property string $floor_level
 * @property int $listing_id
 * @property int $id_user
 * @property int $total_clicks
 * @property int $is_published
 * @property int $status
 * @property string $created_at
 *
 * @property User $user
 * @property PropertyHasAmenities[] $propertyHasAmenities
 * @property PropertyAmenities[] $amenities
 * @property PropertyHasFacilities[] $propertyHasFacilities
 * @property PropertyFacilities[] $facilities
 * @property PropertyImage[] $propertyImages
 * @property PropertyListing[] $propertyListings
 */
class Property extends \yii\db\ActiveRecord
{
    public $prop_facilities = [];
    public $prop_amenities = [];

    const STATUS_NOT_APPROVED = 0, STATUS_NOT_ACTIVE = 0;
    const STATUS_APPROVED = 1, STATUS_ACTIVE = 1;
    const STATUS_DELETED = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'price', 'location', 'type', 'tenure'], 'required'],
            [['description'], 'string'],
            [['price', 'listing_id', 'id_user', 'total_clicks', 'is_published', 'status'], 'integer'],
            [['created_at'], 'safe'],
            [['title', 'slug', 'furnishing'], 'string', 'max' => 50],
            [['location', 'type', 'tenure', 'developer'], 'string', 'max' => 100],
            [['floor_size', 'psf'], 'string', 'max' => 20],
            [['land_size'], 'string', 'max' => 50],
            [['top'], 'string', 'max' => 10],
            [['floor_level'], 'string', 'max' => 30],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_property' => 'Id Property',
            'title' => 'Title',
            'description' => 'Description',
            'slug' => 'Slug',
            'price' => 'Price',
            'location' => 'Location',
            'type' => 'Type',
            'tenure' => 'Tenure',
            'floor_size' => 'Floor Size',
            'developer' => 'Developer',
            'land_size' => 'Land Size',
            'psf' => 'Psf',
            'furnishing' => 'Furnishing',
            'top' => 'Top',
            'floor_level' => 'Floor Level',
            'listing_id' => 'Listing ID',
            'id_user' => 'Id User',
            'total_clicks' => 'Total Clicks',
            'is_published' => 'Is Published',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                // 'slugAttribute' => 'slug',
                'immutable' => true,
                'ensureUnique'=>true,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyHasAmenities()
    {
        return $this->hasMany(PropertyHasAmenities::className(), ['id_property' => 'id_property']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmenities()
    {
        return $this->hasMany(PropertyAmenities::className(), ['id_amenities' => 'id_amenities'])->viaTable('property_has_amenities', ['id_property' => 'id_property']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyHasFacilities()
    {
        return $this->hasMany(PropertyHasFacilities::className(), ['id_property' => 'id_property']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacilities()
    {
        return $this->hasMany(PropertyFacilities::className(), ['id_facilities' => 'id_facilities'])->viaTable('property_has_facilities', ['id_property' => 'id_property']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyImages()
    {
        return $this->hasMany(PropertyImage::className(), ['id_property' => 'id_property']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyListings()
    {
        return $this->hasMany(PropertyListing::className(), ['id_property' => 'id_property']);
    }

    /**
     * load the post's facilities (*2)
     */
    public function loadFacilities()
    {
        $this->category_ids = [];
        if (!empty($this->id)) {
            $rows = PropertyFacilities::find()
                ->select(['category_id'])
                ->where(['post_id' => $this->id])
                ->asArray()
                ->all();
            foreach($rows as $row) {
               $this->category_ids[] = $row['category_id'];
            }
        }
    }

    /**
     * save the post's facilities (*3)
     */
    public function saveFacilities($id_property, $facilities)
    {
        /* clear the facilities of the post before saving */
        PropertyHasFacilities::deleteAll(['id_property' => $id_property]);
        if (is_array($facilities)) {
            foreach($facilities as $id_facilities) {
                $prop_facil = new PropertyHasFacilities();
                $prop_facil->id_property = $id_property;
                $prop_facil->id_facilities = $id_facilities;
                $prop_facil->save();
            }
        }
        /* Be careful, $this->prop_facilities can be empty */
    }

    /**
     * save the post's amenities (*3)
     */
    public function saveAmenities($id_property, $amenities)
    {
        /* clear the amenities of the post before saving */
        PropertyHasAmenities::deleteAll(['id_property' => $id_property]);
        if (is_array($amenities)) {
            foreach($amenities as $id_amenities) {
                $prop_amen = new PropertyHasAmenities();
                $prop_amen->id_property = $id_property;
                $prop_amen->id_amenities = $id_amenities;
                $prop_amen->save();
            }
        }
        /* Be careful, $this->prop_amenities can be empty */
    }

    public static function getStatusButton($id, $status, $slug)
    {
        $preview = Html::a('<i class="fa fa-eye"></i>', ['/agents/property/','slug'=>$slug], ['class'=>'btn btn-primary m-btn m-btn--icon m-btn--icon-only', 'target'=>'_blank', 'title'=>'Preview']);

        if ($status == self::STATUS_APPROVED) {
            $approved = '';
        } else {
            $approved = Html::button('<i class="fa fa-check"></i>', [
                'class'=>'btn btn-success m-btn m-btn--icon m-btn--icon-only',
                'onclick'=>'setStatusProp(\'' . $id . '\',\'' . 'approved' . '\')',
                'title'=>'Approved ',
            ]);
        }
        // $not_approved = Html::a('<i class="fa fa-times"></i>', ['#'], [
        //     'class'=>'btn btn-danger m-btn m-btn--icon m-btn--icon-only',
        //     'onclick'=>'setStatus(\'' . $id . '\',\'' . 'not-approved' . '\')',
        //     'title'=>'Not Approved ',
        // ]);

        // return $approved . ' ' .$not_approved;
        return $preview . ' ' . $approved;
    }

    public static function getActionButton($id)
    {
        $view = Html::a('<i class="fa fa-search"></i>', ['view','id'=>$id], ['class'=>'btn btn-secondary m-btn m-btn--icon m-btn--icon-only']);
        $update = Html::a('<i class="fa fa-pencil-alt"></i>', ['update','id'=>$id], ['class'=>'btn btn-success m-btn m-btn--icon m-btn--icon-only']);
        $delete = Html::a('<i class="fa fa-trash"></i>', ['delete','id'=>$id],
            [
                'class'=>'btn btn-danger m-btn m-btn--icon m-btn--icon-only',
                'data'=>[
                    'method'=>'post',
                    'confirm'=>'Are you sure to delete this data?',
                ]
            ]);

        return $view . ' ' . $update . ' ' .$delete;
    }

    public function getStatusLabel(){
        if($this->is_published==self::STATUS_APPROVED)
            return '<span class="m-badge m-badge--success m-badge--wide">Published</span>';
        else
            return '<span class="m-badge m-badge--warning m-badge--wide">Not Published</span>';
    }

    /**
     * @param $slug
     * @return array|null|\yii\db\ActiveRecord
     */
    public function findBySlug($slug)
    {
        return self::find()->where([
            'slug' => $slug,
        ])->one();
    }

    public static function getAllProperty(){
        $data = self::find()->all();
        $listData = ArrayHelper::map($data,'id_property','title');

        return $listData;
    }

    public static function getAllDistrict()
    {
        return [
            // 'D01'=>'Boat Quay / Raffles Place / Marina',
            // 'D02'=>'Chinatown / Tanjong Pagar',
            // 'D03'=>'Alexandra / Commonwealth',
            // 'D04'=>'Harbourfront / Telok Blangah',
            // 'D05'=>'Buona Vista / West Coast / Clementi New Town',
            // 'D06'=>'City Hall / Clarke Quay',
            // 'D07'=>'Beach Road / Bugis / Rochor',
            // 'D08'=>'Farrer Park / Serangoon Rd',
            // 'D09'=>'Orchard / River Valley',
            // 'D10'=>'Tanglin / Holland / Bukit Timah',
            // 'D11'=>'Newton / Novena',
            // 'D12'=>'Balestier / Toa Payoh',
            // 'D13'=>'Macpherson / Potong Pasir',
            // 'D14'=>'Eunos / Geylang / Paya Lebar',
            // 'D15'=>'East Coast / Marine Parade',
            // 'D16'=>'Bedok / Upper East Coast',
            // 'D17'=>'Changi Airport / Changi Village',
            // 'D18'=>'Pasir Ris / Tampines',
            // 'D19'=>'Hougang / Punggol / Sengkang',
            // 'D20'=>'Ang Mo Kio / Bishan / Thomson',
            // 'D21'=>'Clementi Park / Upper Bukit Timah',
            // 'D22'=>'Boon Lay / Jurong / Tuas',
            // 'D23'=>'Dairy Farm / Bukit Panjang / Choa Chu Kang',
            // 'D24'=>'Lim Chu Kang / Tengah',
            // 'D25'=>'Admiralty / Woodlands',
            // 'D26'=>'Mandai / Upper Thomson',
            // 'D27'=>'Sembawang / Yishun',
            // 'D28'=>'Seletar / Yio Chu Kang',
            'Boat Quay / Raffles Place / Marina'=>'Boat Quay / Raffles Place / Marina',
            'Chinatown / Tanjong Pagar'=>'Chinatown / Tanjong Pagar',
            'Alexandra / Commonwealth'=>'Alexandra / Commonwealth',
            'Harbourfront / Telok Blangah'=>'Harbourfront / Telok Blangah',
            'Buona Vista / West Coast / Clementi New Town'=>'Buona Vista / West Coast / Clementi New Town',
            'City Hall / Clarke Quay'=>'City Hall / Clarke Quay',
            'Beach Road / Bugis / Rochor'=>'Beach Road / Bugis / Rochor',
            'Farrer Park / Serangoon Rd'=>'Farrer Park / Serangoon Rd',
            'Orchard / River Valley'=>'Orchard / River Valley',
            'Tanglin / Holland / Bukit Timah'=>'Tanglin / Holland / Bukit Timah',
            'Newton / Novena'=>'Newton / Novena',
            'Balestier / Toa Payoh'=>'Balestier / Toa Payoh',
            'Macpherson / Potong Pasir'=>'Macpherson / Potong Pasir',
            'Eunos / Geylang / Paya Lebar'=>'Eunos / Geylang / Paya Lebar',
            'East Coast / Marine Parade'=>'East Coast / Marine Parade',
            'Bedok / Upper East Coast'=>'Bedok / Upper East Coast',
            'Changi Airport / Changi Village'=>'Changi Airport / Changi Village',
            'Pasir Ris / Tampines'=>'Pasir Ris / Tampines',
            'Hougang / Punggol / Sengkang'=>'Hougang / Punggol / Sengkang',
            'Ang Mo Kio / Bishan / Thomson'=>'Ang Mo Kio / Bishan / Thomson',
            'Clementi Park / Upper Bukit Timah'=>'Clementi Park / Upper Bukit Timah',
            'Boon Lay / Jurong / Tuas'=>'Boon Lay / Jurong / Tuas',
            'Dairy Farm / Bukit Panjang / Choa Chu Kang'=>'Dairy Farm / Bukit Panjang / Choa Chu Kang',
            'Lim Chu Kang / Tengah'=>'Lim Chu Kang / Tengah',
            'Admiralty / Woodlands'=>'Admiralty / Woodlands',
            'Mandai / Upper Thomson'=>'Mandai / Upper Thomson',
            'Sembawang / Yishun'=>'Sembawang / Yishun',
            'Seletar / Yio Chu Kang'=>'Seletar / Yio Chu Kang',
        ];
    }

    public static function getAllType()
    {
        return [
            'Condo'=>'Condo',
            'Landed'=>'Landed',
            'HDB'=>'HDB',
        ];
    }

    public static function getAllStatus()
    {
        return [
            self::STATUS_ACTIVE=>'Active',
            self::STATUS_NOT_ACTIVE=>'Not Active',
        ];
    }
}
