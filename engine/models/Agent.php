<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "agent".
 *
 * @property int $id_agent
 * @property string $name
 * @property string $username
 * @property string $password
 * @property string $photo
 * @property string $registration_number
 * @property string $title
 * @property string $description
 * @property string $phone_number
 * @property string $office
 * @property string $fax
 * @property string $gender
 * @property string $email
 * @property string $award
 * @property string $speciality
 * @property string $country_operability
 * @property string $investment_deposit_range
 * @property string $facebook
 * @property string $twitter
 * @property string $linkedin
 * @property int $total_clicks
 * @property int $status
 * @property int $is_approved
 * @property string $created_at
 *
 * @property User $user
 * @property PropertyListing[] $propertyListings
 */
class Agent extends ActiveRecord implements IdentityInterface
{
    const STATUS_NOT_APPROVED = 0, STATUS_NOT_ACTIVE = 0;
    const STATUS_APPROVED = 1, STATUS_ACTIVE = 1;

    public $confirm_password;
    public $confirm_password_login;
    public $new_password;
    public $authKey;
    public $accessToken;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone_number', 'username', 'password', 'email', 'speciality', 'gender'], 'required'],
            [['description'], 'string'],
            [['total_clicks', 'status', 'is_approved'], 'integer'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['gender'], 'string', 'max' => 10],
            [['username', 'email', 'country_operability'], 'string', 'max' => 50],
            [['registration_number', 'title', 'speciality', 'investment_deposit_range'], 'string', 'max' => 30],
            [['password'], 'string', 'max' => 32],
            [['office', 'fax'], 'string', 'max' => 20],
            [['photo', 'award', 'facebook', 'twitter', 'linkedin'], 'string', 'max' => 255],
            [['phone_number'], 'string', 'max' => 15],
            [['password'], 'string', 'length' => [6, 100]],
            [['new_password','confirm_password', 'confirm_password_login'], 'string', 'length' => [6, 100]],
            [['confirm_password'], 'compare', 'compareAttribute' => 'new_password'],
            // [['confirm_password_login'], 'compare', 'compareAttribute' => 'password'],
            [['photo'],
                'file',
                'mimeTypes' => 'image/jpeg, image/png, image/jpg',
                'checkExtensionByMimeType' => true,
                'wrongMimeType' => \Yii::t('app','Please try to upload an image.'),
                'skipOnEmpty' => !$this->isNewRecord,
                'when' => function ($model) {
                    return !empty($model->photo);
                }
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_agent' => 'Id Agent',
            'name' => 'Full Name',
            'username' => 'Username',
            'password' => 'Password',
            'confirm_password' => 'Confirm Password',
            'new_password' => 'New Password',
            'photo' => 'Photo',
            'registration_number' => 'Registration Number',
            'title' => 'Title',
            'description' => 'Description',
            'phone_number' => 'Phone Number',
            'office' => 'Office Number',
            'fax' => 'Fax Number',
            'gender' => 'Gender',
            'email' => 'Email',
            'award' => 'Award',
            'speciality' => 'Speciality',
            'country_operability' => 'Country',
            'investment_deposit_range' => 'Investment Deposit Range',
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'linkedin' => 'Linkedin',
            'total_clicks' => 'Total Clicks',
            'status' => 'Status',
            'is_approved' => 'Is Approved',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return bool
     */
    public function afterValidate()
    {
        parent::afterValidate();

        $this->handleUploadedImage();

        return true;
    }

    /**
     * handleUploadedImage
     */
    protected function handleUploadedImage()
    {
        if ($this->hasErrors()) {
            return;
        }

        if (!($photo = UploadedFile::getInstance($this, 'photo'))) {
            return;
        }

        $storagePath = Yii::getAlias('@webroot/uploads/images/agent');
        if (!file_exists($storagePath) || !is_dir($storagePath)) {
            if (!@mkdir($storagePath, 0777, true)) {
                \Yii::t('app', 'The images storage directory({path} does not exists and cannot be created!', ['path' =>$storagePath]);
                return;
            }
        }

        $newAvatarName = $photo->name;
        $photo->saveAs($storagePath . '/' . $newAvatarName);
        if (!is_file($storagePath . '/' . $newAvatarName)) {
            \Yii::t('app', 'Cannot move the avatar into the correct storage folder!');
            return;
        }
        $existing_file = $storagePath . '/' . $newAvatarName;
        $newAvatarName = substr(sha1(time()),0,6) . $newAvatarName;
        rename($existing_file , $storagePath . '/' . $newAvatarName );
        $avatar = str_replace('Upload', '', $photo);
        $this->photo = Yii::getAlias('/uploads/images/agent/' . $newAvatarName);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyListings()
    {
        return $this->hasMany(PropertyListing::className(), ['id_agent' => 'id_agent']);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id_agent' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$agents as $agent) {
            if ($agent['accessToken'] === $token) {
                return new static($agent);
            }
        }

        return null;
    }

    /**
     * Finds agent by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'is_approved' => self::STATUS_APPROVED]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id_agent;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current agent
     */
    public function validatePassword($password)
    {
        return $this->password === md5($password);
    }

    public static function getStatusButton($id, $status)
    {
        $preview = Html::a('<i class="fa fa-eye"></i>', ['/agents/detail/','id'=>$id], ['class'=>'btn btn-primary m-btn m-btn--icon m-btn--icon-only', 'target'=>'_blank', 'title'=>'Preview']);

        if ($status == self::STATUS_APPROVED) {
            $approved = '';
        } else {
            $approved = Html::button('<i class="fa fa-check"></i>', [
                'class'=>'btn btn-success m-btn m-btn--icon m-btn--icon-only',
                'onclick'=>'setStatus(\'' . $id . '\',\'' . 'approved' . '\')',
                'title'=>'Approved ',
            ]);
        }
        // $not_approved = Html::a('<i class="fa fa-times"></i>', ['#'], [
        //     'class'=>'btn btn-danger m-btn m-btn--icon m-btn--icon-only',
        //     'onclick'=>'setStatus(\'' . $id . '\',\'' . 'not-approved' . '\')',
        //     'title'=>'Not Approved ',
        // ]);

        // return $approved . ' ' .$not_approved;
        return $preview . ' ' . $approved;
    }

    public static function getActionButton($id)
    {
        $view = Html::a('<i class="fa fa-search"></i>', ['view','id'=>$id], ['class'=>'btn btn-secondary m-btn m-btn--icon m-btn--icon-only']);
        $update = Html::a('<i class="fa fa-pencil-alt"></i>', ['update','id'=>$id], ['class'=>'btn btn-success m-btn m-btn--icon m-btn--icon-only']);
        $delete = Html::a('<i class="fa fa-trash"></i>', ['delete','id'=>$id],
            [
                'class'=>'btn btn-danger m-btn m-btn--icon m-btn--icon-only',
                'data'=>[
                    'method'=>'post',
                    'confirm'=>'Are you sure to delete this data?',
                ]
            ]);

        return $view . ' ' . $update . ' ' .$delete;
    }

    public function getStatusLabel(){
        if($this->is_approved==self::STATUS_APPROVED)
            return '<span class="m-badge m-badge--success m-badge--wide">Approved</span>';
        else
            return '<span class="m-badge m-badge--warning m-badge--wide">Not Approved</span>';
    }

    public function getThumbnail(){
        if($this->photo != NULL) {
            $path = Yii::getAlias('@web').$this->photo;
            // if (file_exists($path)) {
                return '<img src="'.$path.'" alt="agent" class="mw-100 img-fluid img-thumbnail mx-auto" style="width: 65px; height:75px;" >';
            // } else {
            //     return '<img src="'.Url::base().'/assets/img/avatar1.jpg" alt="agent" class="mw-100 w-100 img-fluid img-thumbnail" style="height:75px;" >';
            // }
        } else {
            return '<img src="'.Url::base().'/assets/img/avatar1.jpg" alt="agent" class="mw-100 img-fluid img-thumbnail" style="height:75px;" >';
        }
    }

    public function getThumbnailAgent(){
        if($this->photo != NULL) {
            $path = Yii::getAlias('@web').$this->photo;
            return '<img src="'.$path.'" alt="agent" class="mw-100 w-100" style="height:178px;" >';
        } else {
            return '<img src="'.Url::base().'/assets/propinside/img/team/t1.jpg" alt="team" class="mw-100 w-100">';
        }
    }

    public function getShortDescription($length=100){
        $description = $this->description;
        return substr($description,0,$length);
    }

    public static function getAllAgent(){
        $data = self::find()->all();
        $listData = ArrayHelper::map($data,'id_agent','name');

        return $listData;
    }

    public static function getAllGender()
    {
        return [
            'Male'=>'Male',
            'Female'=>'Female',
        ];
    }

    public function isAgent()
    {        
        $agent = static::findOne(['id_agent' => $this->id_agent, 'status' => self::STATUS_ACTIVE]);
        // die(var_dump($agent));
        if(!$agent) {
            return FALSE;
        } else {
            return $agent->status == self::STATUS_ACTIVE ? TRUE : FALSE;   
        }
    }
}
