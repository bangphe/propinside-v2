<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "property_listing".
 *
 * @property int $id_listing
 * @property int $id_property
 * @property int $id_agent
 * @property string $created_at
 *
 * @property Property $property
 * @property Agent $agent
 */
class PropertyListing extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property_listing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_property', 'id_agent'], 'integer'],
            [['created_at'], 'safe'],
            [['id_property'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['id_property' => 'id_property']],
            [['id_agent'], 'exist', 'skipOnError' => true, 'targetClass' => Agent::className(), 'targetAttribute' => ['id_agent' => 'id_agent']],
            // [['id_property', 'id_agent'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_listing' => 'Id Listing',
            'id_property' => 'Id Property',
            'id_agent' => 'Id Agent',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id_property' => 'id_property']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgent()
    {
        return $this->hasOne(Agent::className(), ['id_agent' => 'id_agent']);
    }

    public static function getActionButton($id)
    {
        $view = Html::a('<i class="fa fa-search"></i>', ['view','id'=>$id], ['class'=>'btn btn-secondary m-btn m-btn--icon m-btn--icon-only']);
        $update = Html::a('<i class="fa fa-pencil-alt"></i>', ['update','id'=>$id], ['class'=>'btn btn-success m-btn m-btn--icon m-btn--icon-only']);
        // $delete = Html::a('<i class="fa fa-trash"></i>', ['delete','id'=>$id],
        //     [
        //         'class'=>'btn btn-danger m-btn m-btn--icon m-btn--icon-only',
        //         'data'=>[
        //             'method'=>'post',
        //             'confirm'=>'Apakah Anda yakin ingin menghapus data ini?',
        //         ]
        //     ]);

        return $view . ' ' . $update;
    }
}
