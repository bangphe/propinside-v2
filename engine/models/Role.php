<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "role".
 *
 * @property int $id_role
 * @property string $role
 * @property int $status
 * @property string $created_at
 *
 * @property User[] $users
 */
class Role extends \yii\db\ActiveRecord
{
    const ROLE_ADMIN = 1;
    const ROLE_USER = 2;
    const ROLE_AGENT = 3;
    const ROLE_INVESTOR = 4;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['role'], 'required'],
            [['status'], 'integer'],
            [['created_at'], 'safe'],
            [['role'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_role' => 'Id Role',
            'role' => 'Role',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id_role' => 'id_role']);
    }
}
