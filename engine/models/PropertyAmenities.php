<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "property_amenities".
 *
 * @property int $id_amenities
 * @property string $amenities
 * @property string $icons
 * @property int $status
 * @property string $created_at
 *
 * @property PropertyHasAmenities[] $propertyHasAmenities
 * @property Property[] $properties
 */
class PropertyAmenities extends \yii\db\ActiveRecord
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property_amenities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['amenities'], 'required'],
            [['amenities'], 'unique'],
            [['status'], 'integer'],
            [['created_at'], 'safe'],
            [['amenities'], 'string', 'max' => 150],
            [['icons'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_amenities' => 'Id Amenities',
            'amenities' => 'Amenities',
            'icons' => 'Icons',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyHasAmenities()
    {
        return $this->hasMany(PropertyHasAmenities::className(), ['id_amenities' => 'id_amenities']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(Property::className(), ['id_property' => 'id_property'])->viaTable('property_has_amenities', ['id_amenities' => 'id_amenities']);
    }

    public static function listAmenities() {
        return ArrayHelper::map(self::findAll(['status' => 1]), 'id_amenities', 'amenities');
    }

    public static function getActionButton($id)
    {
        $view = Html::a('<i class="fa fa-search"></i>', ['view','id'=>$id], ['class'=>'btn btn-secondary m-btn m-btn--icon m-btn--icon-only']);
        $update = Html::a('<i class="fa fa-pencil-alt"></i>', ['update','id'=>$id], ['class'=>'btn btn-success m-btn m-btn--icon m-btn--icon-only']);
        $delete = Html::a('<i class="fa fa-trash"></i>', ['#'],
            [
                'class'=>'btn btn-danger m-btn m-btn--icon m-btn--icon-only',
                'onclick'=>'deleteRow('.$id.')',
            ]);

        return $view . ' ' . $update . ' ' . $delete;
    }

    public function getStatusLabel()
    {
        if($this->status==self::STATUS_ACTIVE)
            return '<span class="m-badge m-badge--success m-badge--wide">Active</span>';
        else
            return '<span class="m-badge m-badge--warning m-badge--wide">Not Active</span>';
    }
}
