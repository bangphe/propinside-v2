<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "property_news_gallery".
 *
 * @property int $id_property_news_gallery
 * @property int $id_property_news
 * @property string $image_path
 * @property int $sort_order
 * @property string $created_at
 *
 * @property PropertyNews $propertyNews
 */
class PropertyNewsGallery extends \yii\db\ActiveRecord
{
    public $imagesGallery;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property_news_gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // [['id_property_news'], 'required'],
            [['id_property_news', 'sort_order'], 'integer'],
            [['created_at'], 'safe'],
            [['image_path'], 'string', 'max' => 255],
            [['id_property_news'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyNews::className(), 'targetAttribute' => ['id_property_news' => 'id_property_news']],
            [['image_path'],
                'file',
                'mimeTypes' => 'image/jpeg, image/png, image/jpg',
                'checkExtensionByMimeType' => true,
                'wrongMimeType' => \Yii::t('app','Please try to upload an image.'),
                'skipOnEmpty' => !$this->isNewRecord,
                'when' => function ($model) {
                    return !empty($model->image_path);
                }
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_property_news_gallery' => 'Id Property News Gallery',
            'id_property_news' => 'Id Property News',
            'image_path' => 'Image Path',
            'sort_order' => 'Sort Order',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyNews()
    {
        return $this->hasOne(PropertyNews::className(), ['id_property_news' => 'id_property_news']);
    }

    public function handleUploadedImagesOld($id_property_news)
    {
        $result = [];

        if ($this->hasErrors()) {
            return [];
        }

        $storagePath = Yii::getAlias('@webroot/uploads/images/news');
        if (!file_exists($storagePath) || !is_dir($storagePath)) {
            if (!@mkdir($storagePath, 0777, true)) {
                throw new \Exception(Yii::t('app', 'The images storage directory({path}) does not exists and cannot be created!' . $storagePath));
            }
        }

        $existingImages = self::find()->where(['id_property_news' => $id_property_news])->orderBy('sort_order ASC')->all();

        if (empty($existingImages)) {
            if (!($imagesGallery = UploadedFile::getInstances($this, 'imagesGallery'))) {
                throw new \Exception(Yii::t('app', 'Please add at least one image to your Property News'));
            }
        } else {
            if (!($imagesGallery = UploadedFile::getInstances($this, 'imagesGallery'))) {
                return [];
            }
        }

        $sort = 0;

        foreach ($imagesGallery as $imageGallery) {
            $sort++;
            $newGalleryImageName = $imageGallery->name;
            if (!$imageGallery->saveAs($storagePath . '/' . $newGalleryImageName)) {
                throw new \Exception(Yii::t('app', 'Something went wrong while saving images, please try again later.'));
            }
            if (!is_file($storagePath . '/' . $newGalleryImageName)) {
                throw new \Exception(Yii::t('app', 'Cannot move the image into the correct storage folder!'));
            }

            $existing_file = $storagePath . '/' . $newGalleryImageName;

            $newGalleryImageName = substr(sha1(time()), 0, 6) . $newGalleryImageName;
            rename($existing_file, $storagePath . '/' . $newGalleryImageName);

            $model = new static();
            $model->id_property_news = $id_property_news;
            // $model->image_path = Yii::getAlias('@web/uploads/images/news/' . $newGalleryImageName);
            $model->image_path = Yii::getAlias('/uploads/images/news/' . $newGalleryImageName);
            $model->sort_order = $sort;
            $model->created_at = date('Y-m-d H:i:s');

            $model->save();

            $result[] = $model;
        }

        return $result;
    }

    public function handleUploadedImages($id_property_news)
    {
        $result = [];
        $session = \Yii::$app->session;
        $new_images = $session->get('new_images', []);

        if ($this->hasErrors()) {
            return [];
        }

        $storagePath = Yii::getAlias('@webroot/uploads/images/news');
        if (!file_exists($storagePath) || !is_dir($storagePath)) {
            if (!@mkdir($storagePath, 0777, true)) {
                throw new \Exception(Yii::t('app', 'The images storage directory({path}) does not exists and cannot be created!' . $storagePath));
            }
        }

        if($id_property_news == null){
            if (!($imagesGallery = UploadedFile::getInstances($this, 'imagesGallery'))) {
                throw new \Exception(Yii::t('app', 'Please add at least one image'));
            }

            foreach ($imagesGallery as $imageGallery) {

                $newGalleryImageName = $imageGallery->name;
                if (!$imageGallery->saveAs($storagePath . '/' . $newGalleryImageName)) {
                    throw new \Exception(Yii::t('app', 'Something went wrong while saving images, please try again later.'));
                }
                if (!is_file($storagePath . '/' . $newGalleryImageName)) {
                    throw new \Exception(Yii::t('app', 'Cannot move the image into the correct storage folder!'));
                }

                $existing_file = $storagePath . '/' . $newGalleryImageName;

                // $this->image_fix_orientation($existing_file);

                $newGalleryImageName = substr(sha1(time()), 0, 6) . $newGalleryImageName;
                rename($existing_file, $storagePath . '/' . $newGalleryImageName);

                $model = new static();
                $model->image_path = Yii::getAlias('/uploads/images/news/' . $newGalleryImageName);
                $model->save(false);

                $result[] = $model;

                $new_images[] = $model->id_property_news_gallery;

                $session->set('new_images', $new_images);
            }
        } else {
            $sort = 0;
            if ($new_images && $models = self::find()->where(['id_property_news_gallery' => $new_images])->all()) {
                foreach ($models as $model) {
                    $sort++;
                    $model->id_property_news = $id_property_news;
                    $model->sort_order = $sort;
                    $model->save();

                    $result[] = $model;
                }

                $session->remove('new_images');
            }

            // $existingImages = self::find()->where(['id_property_news' => $id_property_news])->orderBy('sort_order ASC')->all();

            // if (empty($existingImages)) {
            //     if (!($imagesGallery = UploadedFile::getInstances($this, 'imagesGallery'))) {
            //         throw new \Exception(Yii::t('app', 'Please add at least one image to your Property'));
            //     }
            // } else {
                if (!($imagesGallery = UploadedFile::getInstances($this, 'imagesGallery'))) {
                    return [];
                }
            // }

            // $sort = empty($existingImages) ? 0 : end($existingImages)->sort_order;
            $sort = 0;

            foreach ($imagesGallery as $imageGallery) {
                $sort++;
                $newGalleryImageName = $imageGallery->name;
                if (!$imageGallery->saveAs($storagePath . '/' . $newGalleryImageName)) {
                    throw new \Exception(Yii::t('app', 'Something went wrong while saving images, please try again later.'));
                }
                if (!is_file($storagePath . '/' . $newGalleryImageName)) {
                    throw new \Exception(Yii::t('app', 'Cannot move the image into the correct storage folder!'));
                }

                $existing_file = $storagePath . '/' . $newGalleryImageName;

                // $this->image_fix_orientation($existing_file);

                $newGalleryImageName = substr(sha1(time()), 0, 6) . $newGalleryImageName;
                rename($existing_file, $storagePath . '/' . $newGalleryImageName);

                $model = new static();
                $model->id_property_news = $id_property_news;
                $model->image_path = Yii::getAlias('/uploads/images/news/' . $newGalleryImageName);
                $model->sort_order = $sort;
                $model->created_at = date('Y-m-d H:i:s');

                $model->save();

                $result[] = $model;
            }
        }

        return $result;
    }
}
