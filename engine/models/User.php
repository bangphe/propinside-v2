<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "user".
 *
 * @property int $id_user
 * @property int $id_role
 * @property string $username
 * @property string $email
 * @property string $password
 * @property int $status
 * @property string $created_at
 *
 * @property Agent[] $agents
 * @property Property[] $properties
 * @property Role $role
 */

class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    public $authKey;
    public $accessToken;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_role', 'username', 'email', 'password'], 'required'],
            [['id_role', 'status'], 'integer'],
            [['email'], 'email'],
            [['created_at'], 'safe'],
            [['username', 'email', 'password'], 'string', 'max' => 50],
            [['id_role'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['id_role' => 'id_role']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_user' => 'Id User',
            'id_role' => 'Id Role',
            'username' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
            'confirm_password' => 'Confirm Password',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(Property::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id_role' => 'id_role']);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id_user' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id_user;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === md5($password);
    }

    public static function getActionButton($id)
    {
        $view = Html::a('<i class="fa fa-search"></i>',['view','id'=>$id],['class'=>'btn btn-secondary m-btn m-btn--icon m-btn--icon-only']);
        $update = Html::a('<i class="fa fa-pencil-alt"></i>',['update','id'=>$id],['class'=>'btn btn-success m-btn m-btn--icon m-btn--icon-only']);
        $delete = Html::a('<i class="fa fa-trash"></i>',['delete','id'=>$id],
            [
                'class'=>'btn btn-danger m-btn m-btn--icon m-btn--icon-only',
                'data'=>[
                    'method'=>'post',
                    'confirm'=>'Apakah Anda yakin ingin menghapus data ini?',
                ]
            ]);

        return $view . ' ' . $update . ' ' .$delete;
    }

    public function isAdmin()
    {        
        $user = static::findOne(['id_user' => $this->id_user, 'status' => self::STATUS_ACTIVE]);
        if(!$user) {
            return FALSE;
        } else {
          return $user->role == Role::ROLE_ADMIN ? TRUE : FALSE;   
        }
    }
}
