<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "property_has_amenities".
 *
 * @property int $id_property
 * @property int $id_amenities
 *
 * @property Property $property
 * @property PropertyAmenities $amenities
 */
class PropertyHasAmenities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property_has_amenities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_property', 'id_amenities'], 'required'],
            [['id_property', 'id_amenities'], 'integer'],
            [['id_property', 'id_amenities'], 'unique', 'targetAttribute' => ['id_property', 'id_amenities']],
            [['id_property'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['id_property' => 'id_property']],
            [['id_amenities'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyAmenities::className(), 'targetAttribute' => ['id_amenities' => 'id_amenities']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_property' => 'Id Property',
            'id_amenities' => 'Id Amenities',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id_property' => 'id_property']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmenities()
    {
        return $this->hasOne(PropertyAmenities::className(), ['id_amenities' => 'id_amenities']);
    }
}
