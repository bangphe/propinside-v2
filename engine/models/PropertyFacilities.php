<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "property_facilities".
 *
 * @property int $id_facilities
 * @property string $facilities
 * @property string $icons
 * @property int $status
 * @property string $created_at
 *
 * @property PropertyHasFacilities[] $propertyHasFacilities
 * @property Property[] $properties
 */
class PropertyFacilities extends \yii\db\ActiveRecord
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property_facilities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['facilities'], 'required'],
            [['facilities'], 'unique'],
            [['status'], 'integer'],
            [['created_at'], 'safe'],
            [['facilities'], 'string', 'max' => 100],
            [['icons'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_facilities' => 'Id Facilities',
            'facilities' => 'Facilities',
            'icons' => 'Icons',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyHasFacilities()
    {
        return $this->hasMany(PropertyHasFacilities::className(), ['id_facilities' => 'id_facilities']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(Property::className(), ['id_property' => 'id_property'])->viaTable('property_has_facilities', ['id_facilities' => 'id_facilities']);
    }

    public static function listFacilities() {
        return ArrayHelper::map(self::findAll(['status' => 1]), 'id_facilities', 'facilities');
    }

    public static function getActionButton($id)
    {
        $view = Html::a('<i class="fa fa-search"></i>', ['view','id'=>$id], ['class'=>'btn btn-secondary m-btn m-btn--icon m-btn--icon-only']);
        $update = Html::a('<i class="fa fa-pencil-alt"></i>', ['update','id'=>$id], ['class'=>'btn btn-success m-btn m-btn--icon m-btn--icon-only']);
        $delete = Html::a('<i class="fa fa-trash"></i>', ['#'],
            [
                'class'=>'btn btn-danger m-btn m-btn--icon m-btn--icon-only',
                'onclick'=>'deleteRow('.$id.')',
            ]);

        return $view . ' ' . $update . ' ' . $delete;
    }

    public function getStatusLabel()
    {
        if($this->status==self::STATUS_ACTIVE)
            return '<span class="m-badge m-badge--success m-badge--wide">Active</span>';
        else
            return '<span class="m-badge m-badge--warning m-badge--wide">Not Active</span>';
    }
}
