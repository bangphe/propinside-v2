<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Property;

/**
 * PropertySearch represents the model behind the search form of `app\models\Property`.
 */
class PropertySearch extends Property
{
    public $keyword;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_property', 'price', 'listing_id', 'id_user', 'total_clicks', 'is_published', 'status'], 'integer'],
            [['title', 'description', 'slug', 'location', 'type', 'tenure', 'floor_size', 'developer', 'land_size', 'psf', 'furnishing', 'top', 'floor_level', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Property::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => 6,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            // 'is_published' => $this->is_published,
            'type' => $this->type,
            'location' => $this->location,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);
            // ->andFilterWhere(['like', 'description', $this->description])
            // ->andFilterWhere(['like', 'type', $this->type])
            // ->andFilterWhere(['like', 'developer', $this->developer]);

        // die(var_dump($query->createCommand()->getRawSql()));
        return $dataProvider;
    }
}
