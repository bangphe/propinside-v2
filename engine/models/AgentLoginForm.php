<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * AgentLoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class AgentLoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_agent = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $agent = $this->getAgent();

            if (!$agent || !$agent->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password. Or the user is not yet approved by Administrator.');
            }
        }
    }

    /**
     * Logs in a agent using the provided username and password.
     * @return bool whether the agent is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->agent->login($this->getAgent(), $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    /**
     * Finds agent by [[username]]
     *
     * @return User|null
     */
    public function getAgent()
    {
        if ($this->_agent === false) {
            $this->_agent = Agent::findByUsername($this->username);
        }

        return $this->_agent;
    }
}
