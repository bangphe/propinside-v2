<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "property_has_facilities".
 *
 * @property int $id_property
 * @property int $id_facilities
 *
 * @property Property $property
 * @property PropertyFacilities $facilities
 */
class PropertyHasFacilities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property_has_facilities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_property', 'id_facilities'], 'required'],
            [['id_property', 'id_facilities'], 'integer'],
            [['id_property', 'id_facilities'], 'unique', 'targetAttribute' => ['id_property', 'id_facilities']],
            [['id_property'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['id_property' => 'id_property']],
            [['id_facilities'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyFacilities::className(), 'targetAttribute' => ['id_facilities' => 'id_facilities']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_property' => 'Id Property',
            'id_facilities' => 'Id Facilities',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id_property' => 'id_property']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacilities()
    {
        return $this->hasOne(PropertyFacilities::className(), ['id_facilities' => 'id_facilities']);
    }
}
