<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "property_image".
 *
 * @property int $id_image
 * @property int $id_property
 * @property string $image_path
 * @property int $sort_order
 * @property string $created_at
 *
 * @property Property $property
 */
class PropertyImage extends \yii\db\ActiveRecord
{
    public $imagesGallery;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property_image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image_path'], 'required'],
            [['id_property', 'sort_order'], 'integer'],
            [['created_at'], 'safe'],
            [['image_path'], 'string', 'max' => 255],
            [['id_property'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['id_property' => 'id_property']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_image' => 'Id Image',
            'id_property' => 'Id Property',
            'image_path' => 'Image Path',
            'sort_order' => 'Sort Order',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id_property' => 'id_property']);
    }

    /**
     * @param null $id_property
     * @return PropertyImage[]
     * @throws \Exception
     */
    public function handleUploadedImages($id_property)
    {
        $result = [];
        $session = \Yii::$app->session;
        $new_images = $session->get('new_images', []);

        if ($this->hasErrors()) {
            return [];
        }

        $storagePath = Yii::getAlias('@webroot/uploads/images/property');
        if (!file_exists($storagePath) || !is_dir($storagePath)) {
            if (!@mkdir($storagePath, 0777, true)) {
                throw new \Exception(Yii::t('app', 'The images storage directory({path}) does not exists and cannot be created!' . $storagePath));
            }
        }
        // die(var_dump($id_property == null));

        if($id_property == null){
            if (!($imagesGallery = UploadedFile::getInstances($this, 'imagesGallery'))) {
                throw new \Exception(Yii::t('app', 'Please add at least one image'));
            }

            foreach ($imagesGallery as $imageGallery) {

                $newGalleryImageName = $imageGallery->name;
                if (!$imageGallery->saveAs($storagePath . '/' . $newGalleryImageName)) {
                    throw new \Exception(Yii::t('app', 'Something went wrong while saving images, please try again later.'));
                }
                if (!is_file($storagePath . '/' . $newGalleryImageName)) {
                    throw new \Exception(Yii::t('app', 'Cannot move the image into the correct storage folder!'));
                }

                $existing_file = $storagePath . '/' . $newGalleryImageName;

                // $this->image_fix_orientation($existing_file);

                $newGalleryImageName = substr(sha1(time()), 0, 6) . $newGalleryImageName;
                rename($existing_file, $storagePath . '/' . $newGalleryImageName);

                $model = new static();
                $model->image_path = Yii::getAlias('/uploads/images/property/' . $newGalleryImageName);
                $model->save();

                $result[] = $model;

                $new_images[] = $model->id_image;

                $session->set('new_images', $new_images);
            }
        } else {
            $sort = 0;
            if ($new_images && $models = self::find()->where(['id_image' => $new_images])->all()) {
                foreach ($models as $model) {
                    $sort++;
                    $model->id_property = $id_property;
                    $model->sort_order = $sort;
                    $model->save();

                    $result[] = $model;
                }

                $session->remove('new_images');
            }

            // $existingImages = self::find()->where(['id_property' => $id_property])->orderBy('sort_order ASC')->all();

            // if (empty($existingImages)) {
            //     if (!($imagesGallery = UploadedFile::getInstances($this, 'imagesGallery'))) {
            //         throw new \Exception(Yii::t('app', 'Please add at least one image to your Property'));
            //     }
            // } else {
                if (!($imagesGallery = UploadedFile::getInstances($this, 'imagesGallery'))) {
                    return [];
                }
            // }

            // $sort = empty($existingImages) ? 0 : end($existingImages)->sort_order;
            $sort = 0;

            foreach ($imagesGallery as $imageGallery) {
                $sort++;
                $newGalleryImageName = $imageGallery->name;
                if (!$imageGallery->saveAs($storagePath . '/' . $newGalleryImageName)) {
                    throw new \Exception(Yii::t('app', 'Something went wrong while saving images, please try again later.'));
                }
                if (!is_file($storagePath . '/' . $newGalleryImageName)) {
                    throw new \Exception(Yii::t('app', 'Cannot move the image into the correct storage folder!'));
                }

                $existing_file = $storagePath . '/' . $newGalleryImageName;

                // $this->image_fix_orientation($existing_file);

                $newGalleryImageName = substr(sha1(time()), 0, 6) . $newGalleryImageName;
                rename($existing_file, $storagePath . '/' . $newGalleryImageName);

                $model = new static();
                $model->id_property = $id_property;
                $model->image_path = Yii::getAlias('/uploads/images/property/' . $newGalleryImageName);
                $model->sort_order = $sort;
                $model->created_at = date('Y-m-d H:i:s');

                $model->save();

                $result[] = $model;
            }
        }

        return $result;
    }

    protected function image_fix_orientation($filename) {
        $exif = exif_read_data($filename);
        if (!empty($exif['Orientation'])) {
            $image = imagecreatefromjpeg($filename);
            switch ($exif['Orientation']) {
                case 3:
                    $image = imagerotate($image, 180, 0);
                    break;

                case 6:
                    $image = imagerotate($image, -90, 0);
                    break;

                case 8:
                    $image = imagerotate($image, 90, 0);
                    break;
            }

            imagejpeg($image, $filename, 90);
        }
    }
}
