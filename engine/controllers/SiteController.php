<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use app\components\AccessRule;
use app\components\MyFormatter;
use app\models\AgentLoginForm;
use app\models\ContactForm;
use app\models\Agent;
use app\models\User;
use app\models\Role;
use app\models\Property;
use app\models\PropertyNews;
use app\models\PropertySearch;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    // public function behaviors()
    // {
    //     return [
    //         'access' => [
    //             'class' => AccessControl::className(),
    //             'ruleConfig' => [
    //                 'class' => AccessRule::className(),
    //             ],
    //             'rules' => [
    //                 [
    //                     'actions' => ['login', 'logout'],
    //                     'allow' => true,
    //                 ],
    //                 // [
    //                 //     'actions' => ['logout'],
    //                 //     'allow' => true,
    //                 //     'roles' => ['@'],
    //                 // ],
    //             ],
    //         ],
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'logout' => ['post'],
    //             ],
    //         ],
    //     ];
    // }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionSearch()
    {
        $searchModel = new PropertySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataDistrict = Property::getAllDistrict();
        $dataType = Property::getAllType();
        $dataStatus = Property::getAllStatus();

        return $this->render('search_result', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataDistrict' => $dataDistrict,
            'dataType' => $dataType,
            'dataStatus' => $dataStatus,
        ]); 
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        // if (Yii::$app->agent->isGuest == true) {
            // return $this->redirect(['site/login']);
        // } else {
            // return $this->redirect(['agents/profile']);
        // }
        // $id = Yii::$app->agent->id;
        $searchModel = new PropertySearch();
        $model = new ContactForm();
        $dataProvider = new ActiveDataProvider([
            'query' => PropertyNews::find()->joinWith('property p')->where(['not in', 'p.status', Property::STATUS_DELETED]),
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' => [
                'defaultPageSize' => 5,
            ],
        ]);
        $dataDistrict = Property::getAllDistrict();
        $dataType = Property::getAllType();
        $dataStatus = Property::getAllStatus();
        return $this->render('index', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'dataDistrict' => $dataDistrict,
            'dataType' => $dataType,
            'dataStatus' => $dataStatus,
        ]);
    }

    public function actionAboutUs()
    {
        return $this->render('about_us');
    }

    public function actionContactUs()
    {
        $model = new ContactForm();

        if ($model->load(Yii::$app->request->post())) {
            $emailto = [Yii::$app->params['admin.email'], $model->email];
            if ($model->contactUs($emailto)) {
                Yii::$app->session->setFlash('contactFormSubmitted');

                return $this->refresh();
            }
        }
        return $this->render('contact_us', [
            'model' => $model,
        ]);
    }

    public function actionDetail()
    {
        return $this->render('detail');
    }

    public function actionAgentsdetail()
    {
        return $this->render('agents_detail');
    }

    public function actionProperty()
    {
        return $this->render('property');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $modelUser = new User();
        $modelAgent = new Agent();
        
        if (!Yii::$app->agent->isGuest) {
            return $this->goHome();
        }

        $model = new AgentLoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['agents/profile']);
        }

        if ($modelAgent->load(Yii::$app->request->post())) {
            // die(var_dump($_POST));
            // $modelUser->password = md5($modelUser->password);
            // $modelUser->id_role = Role::ROLE_AGENT;
            // $modelUser->created_at = date('Y-m-d H:i:s');
            // if ($modelUser->validate()) {
                // if ($modelUser->save()) {
                    $modelAgent->password = md5($modelAgent->password);
                    $modelAgent->confirm_password_login = md5($modelAgent->password);
                    $modelAgent->created_at = date('Y-m-d H:i:s');
                    if ($modelAgent->validate() && $modelAgent->save()) {
                        Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Congrats!</strong> Data is successfully saved.'));
                        return $this->refresh();
                    }
                // }
            // }
        }

        return $this->render('login', [
            'model' => $model,
            'modelUser' => $modelUser,
            'modelAgent' => $modelAgent,
        ]);
    }

    public function actionLoginprocess()
    {
        $model = new AgentLoginForm();

        // if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        //     Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        //     return ActiveForm::validate($model);
        // }

        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $data = [
                "_csrf" => $post["_csrf"],
                "AgentLoginForm" => $post,
            ];
            // die(var_dump($model->load($data)));
            // die(var_dump($model->login()));
            if ($model->load($data) && $model->login()) {
                // login success
                $message = ["result" => true, "redirectUrl" => Url::to(['agents/profile'])];
                Yii::$app->response->format = Response::FORMAT_JSON; 
                return $message;
            } else {
                // login failure
                $message = ["result" => false];
                Yii::$app->response->format = Response::FORMAT_JSON; 
                return $message;
            }
        } else {
            $message = ["result" => false];
            Yii::$app->response->format = Response::FORMAT_JSON; 
            return $message;
        }
    }

    public function actionRegister()
    {
        $modelUser = new User();
        $modelAgent = new Agent();
        
        if (!Yii::$app->agent->isGuest) {
            return $this->goHome();
        }

        $model = new AgentLoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['agents/profile']);
        }

        if ($modelAgent->load(Yii::$app->request->post())) {
            // die(var_dump($_POST));
            // $modelUser->password = md5($modelUser->password);
            // $modelUser->id_role = Role::ROLE_AGENT;
            // $modelUser->created_at = date('Y-m-d H:i:s');
            // if ($modelUser->validate()) {
                // if ($modelUser->save()) {
                    $modelAgent->password = md5($modelAgent->password);
                    $modelAgent->confirm_password_login = md5($modelAgent->password);
                    $modelAgent->created_at = date('Y-m-d H:i:s');
                    if ($modelAgent->validate() && $modelAgent->save()) {
                        Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Congrats!</strong> Data is successfully saved.'));
                        return $this->refresh();
                    }
                // }
            // }
        }

        return $this->render('register', [
            'model' => $model,
            'modelUser' => $modelUser,
            'modelAgent' => $modelAgent,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->agent->logout();

        // return $this->goHome();
        return $this->redirect(['site/index']);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        $id = Yii::$app->agent->id;
        $agent = $this->findAgent($id);
        $email_agent = '';
        if (!Yii::$app->agent->isGuest) {
            $email_agent = $agent->email;
        }
        $emailto = [Yii::$app->params['admin.email'], $email_agent];
        if ($model->load(Yii::$app->request->post()) && $model->contact($emailto)) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->redirect(['site/index']);
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    protected function findAgent($id)
    {
        if (($model = Agent::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
