<?php

namespace app\controllers;

use Yii;
use app\models\Agent;
use app\models\ContactForm;
use app\models\Property;
use app\models\PropertyImage;
use app\models\PropertyListing;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\AccessRule;
use app\components\MyFormatter;
use yii\db\Expression;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * AgentController implements the CRUD actions for Agent model.
 */
class AgentsController extends Controller
{
    const PROPS_PER_PAGE = 5;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // 'ruleConfig' => [
                //     'class' => AccessRule::className(),
                // ],
                'only' => ['profile', 'my-properties', 'submit-property', 'upload', 'remove-prop-image'],
                'rules' => [
                    [
                        'actions' => ['profile', 'my-properties', 'submit-property', 'upload', 'remove-prop-image'],
                        'allow' => true,
                        // 'roles' => ['@'],
                        // 'roles' => ['$agent->getIsAgent'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        if ($action->id !== "upload") {
            $this->enableCsrfValidation = false;
        } elseif ($action->id !== "remove-prop-image") {
            $this->enableCsrfValidation = false;
        } else {
            $this->enableCsrfValidation = true;
        }
        // $this->enableCsrfValidation = ($action->id !== "upload");
        return parent::beforeAction($action);
    }

    /**
     * Lists all Agent models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Agent::find()->where('is_approved=:app', [':app'=>Agent::STATUS_APPROVED]),
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' => [
                'defaultPageSize' => 3,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionProfile()
    {
        // die(var_dump(Yii::$app->agent->isAgent));
        if (Yii::$app->agent->isGuest == true) {
            return $this->redirect(['/site/login']);
        } else {
            $id = Yii::$app->agent->id;
            $model = $this->findModel($id);
            $propCount = PropertyListing::find()
                ->where('id_agent=:id', [':id'=>$id])
                ->count();

            if ($model->load(Yii::$app->request->post())) {
                if ($model->new_password != NULL && $model->confirm_password != NULL) {
                    $model->password = md5($model->new_password);
                }
                if ($model->save()) {
                    Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Congrats!</strong> Data is successfully saved.'));
                    return $this->refresh();
                }
            }

            return $this->render('profile', [
                'model' => $model,
                'propCount' => $propCount,
            ]);
        }
        
    }

    public function actionProperty($slug)
    {
        // if (Yii::$app->agent->isGuest == true) {
            // return $this->redirect(['site/login']);
        // } else {
            $model = $this->findPropertyBySlug($slug);
            $modelContact = new ContactForm();

            return $this->render('detail_property', [
                'model' => $model,
                'modelContact' => $modelContact,
            ]);
        // }
    }

    public function actionMyProperties()
    {
        if (Yii::$app->agent->isGuest == true) {
            return $this->redirect(['site/login']);
        } else {
            $id = Yii::$app->agent->id;
            $agent = $this->findModel($id);
            $dataProvider = new ActiveDataProvider([
                'query' => PropertyListing::find()->joinWith('property')->where('id_agent=:id', [':id'=>$id])->andWhere(['not in', 'status', Property::STATUS_DELETED]),
                'sort' => ['defaultOrder' => ['id_property' => SORT_DESC]],
                'pagination' => [
                    'defaultPageSize' => self::PROPS_PER_PAGE,
                ],
            ]);

            return $this->render('properties', [
                'agent' => $agent,
                'dataProvider' => $dataProvider,
            ]);
        }

        
    }

    public function actionSubmitProperty()
    {
        if (Yii::$app->agent->isGuest == true) {
            return $this->redirect(['site/login']);
        } else {
            $id = Yii::$app->agent->id;
            $agent = $this->findModel($id);
            $property = new Property();
            $property_images = new PropertyImage();
            $property_listing = new PropertyListing();
            $uploadedImages = PropertyImage::find()
                ->where(['id_image' => \Yii::$app->session->get('new_images', [])])
                ->orderBy('sort_order ASC, id_image ASC')
                ->all();
            $dataDistrict = Property::getAllDistrict();
            $dataType = Property::getAllType();

            if ($property->load(Yii::$app->request->post())) {
                // die(var_dump($_POST['Property']['prop_facilities']));
                $property->created_at = date('Y-m-d H:i:s');
                if ($property->save()) {
                    $modelImages = $property_images->handleUploadedImages($property->id_property);
                    $property->saveFacilities($property->id_property, $_POST['Property']['prop_facilities']);
                    $property->saveAmenities($property->id_property, $_POST['Property']['prop_amenities']);
                    $property_listing->id_agent = $id;
                    $property_listing->id_property = $property->id_property;
                    $property_listing->created_at = date('Y-m-d H:i:s');
                    $property_listing->save();
                    Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Congrats!</strong> Data is successfully saved.'));
                    return $this->refresh();
                }
            }

            return $this->render('submit_property', [
                'agent' => $agent,
                'property' => $property,
                'property_images' => $property_images,
                'dataDistrict' => $dataDistrict,
                'dataType' => $dataType,
            ]);
        }
    }

    public function actionUpload()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $error = null;
        $models = [];
        $images = new PropertyImage();

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $models = $images->handleUploadedImages((int)Yii::$app->request->post('id', 0));
            $transaction->commit();

        } catch (\Exception $e) {
            $error = $e->getMessage() . ' at ' . $e->getFile() . ":" . $e->getLine() . PHP_EOL . $e->getTraceAsString();
            $transaction->rollBack();
        }

        if (!$error && $models) {
            $model = reset($models);
            $file_explode = explode('/', $model->image_path);
            $file_name = $file_explode[4];
            $resp = [
                'initialPreview' => [
                    Yii::getAlias('@web'.$model->image_path),
                ],
                'initialPreviewConfig' => [[
                    'caption' => $file_name,
                    'url' => Url::to(['/agents/remove-prop-image']),
                    'key' => $model->id_image
                ]]
            ];
        } else {
            $resp = ['error' => $error ? $error : ($images->hasErrors() ? $images->getFirstErrors() : 'Unknown')];
        }

        return $resp;
    }

    /**
     * @return array|Response
     */
    public function actionRemovePropImage()
    {
        /* allow only ajax calls */
        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['agents/submit-property']);
        }

        /* set the output to json */
        Yii::$app->response->format = Response::FORMAT_JSON;

        $imageId = Yii::$app->request->post('key');
        if (empty($imageId)) {
            return ['result' => 'error', 'html' => Yii::t('app', 'Something went wrong...')];
        }

        $imageId = PropertyImage::findOne(['id_image' => $imageId]);
        if (empty($imageId)) {
            return ['result' => 'error', 'html' => Yii::t('app', 'Something went wrong...')];
        }

        // if all images are deleted then the prop is not active anymore
        // $countImages = PropertyImage::find()->where(['id_property' => $imageId->id_property])->count();
        // if ($countImages == 1) {
        //     $prop = Property::find()->where(['id_property' => $imageId->id_property])->one();
        //     $prop->status = Property::STATUS_NOT_ACTIVE;
        //     $prop->save(false);
        // }

        $imageId->delete();

        return ['result' => 'success', 'response' => $imageId];
    }

    /**
     * Displays a single Agent model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionDetail($id)
    {
        $model = $this->findModel($id);
        $modelContact = new ContactForm();
        
        $emailto = [Yii::$app->params['admin.email'], $model->email];
        if ($modelContact->load(Yii::$app->request->post()) && $modelContact->contact($emailto)) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }

        return $this->render('agents_detail', [
            'model' => $this->findModel($id),
            'modelContact' => $modelContact,
        ]);
    }

    /**
     * Creates a new Agent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Agent();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_agent]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Agent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $id_agent = Yii::$app->agent->id;
        $property = $this->findModelProperty($id);
        $agent = $this->findModel($id_agent);
        $property_images = new PropertyImage();

        $uploadedImages = PropertyImage::find()
            ->where(['id_property' => $id])
            ->orderBy('sort_order ASC')
            ->all();

        if ($property->load(Yii::$app->request->post())) {
            if ($property->save()) {
                $modelImages = $property_images->handleUploadedImages($property->id_property);
                $property->saveFacilities($property->id_property, $_POST['Property']['prop_facilities']);
                $property->saveAmenities($property->id_property, $_POST['Property']['prop_amenities']);
                Yii::$app->session->setFlash('info', MyFormatter::success('<strong>Congrats!</strong> Data is successfully saved.'));
                return $this->refresh();
            }
        }

        return $this->render('submit_property', [
            'agent' => $agent,
            'property' => $property,
            'property_images' => $property_images,
            'uploadedImages' => $uploadedImages,
        ]);
    }

    /**
     * Deletes an existing Agent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModelProperty($id);
        $model->status = Property::STATUS_DELETED;
        if ($model->save()) {
            Yii::$app->session->setFlash('info', MyFormatter::success2('<strong>Congrats!</strong> Data is successfully deleted.'));
            return $this->redirect(['my-properties']);
        }
    }

    /**
     * Finds the Agent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Agent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Agent::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelProperty($id)
    {
        if (($model = Property::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $slug
     * @return Listing
     * @throws NotFoundHttpException
     */
    protected function findPropertyBySlug($slug)
    {
        if (($property = Property::findOne(['slug' => $slug])) !== null) {
            return $property;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
