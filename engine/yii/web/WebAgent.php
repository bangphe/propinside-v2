<?php
namespace app\yii\web;

use Yii;
use yii\web\IdentityInterface;
use yii\web\User as CoreUser;
use yii\db\Expression;

/**
 * User component
 */

class WebAgent extends CoreUser
{
    /**
     * Check if user is logged in
     *
     * @return bool
     */
    public function getIsLoggedIn()
    {
        return !$this->getIsGuest();
    }

    /**
     * Check if user is logged in
     *
     * @return bool
     */
    public function getIsAgent()
    {
        return $this->identity->isAgent();
        //return $this->getIsAdmin();
    }

    /**
     * @inheritdoc
     */
    public function afterLogin($identity, $cookieBased, $duration)
    {
        /** @var \amnah\yii2\user\models\User $identity */
        //$identity->updateLoginMeta();
        parent::afterLogin($identity, $cookieBased, $duration);
    }
}