<footer class="footer-main">
    <!-- <div class="footer-info">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 pb-lg-0 pb-3">
                    <h4>About Us</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in volup
                    </p>
                    <p>
                        tate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed u
                    </p>
                    <a href="#">Read more</a>
                </div>
                <div class="col-lg-4 pb-lg-0 pb-3">
                    <h4>Recent Blog Posts</h4>
                    <ul class="blog-posts">
                        <li>
                            Quisque sit amet sollicitudin augue. Sed dui quam, molestie at semper vitae, sagittis eu justo.
                            <b>22nd May 2013</b>
                        </li>
                        <li>
                            Quisque sit amet sollicitudin augue. Sed dui quam, molestie at semper vitae, sagittis eu justo.
                            <b>22nd May 2013</b>
                        </li>
                        <li>
                            Quisque sit amet sollicitudin augue. Sed dui quam, molestie at semper vitae, sagittis eu justo.
                            <b>22nd May 2013</b>
                        </li>
                    </ul>
                    <a href="#">Read more</a>
                </div>
                <div class="col-lg-4 pb-lg-0 pb-3">
                    <h4>Contact Information</h4>
                    <ul class="contact-list">
                        <li><span class="pr-2">P |</span>(123) 456 7980</li>
                        <li><span class="pr-2">P |</span>Namehere@gmail.com</li>
                        <li><span class="pr-2">P |</span>www.namehere.com</li>
                        <li><span class="pr-2">P |</span>501 Your Street to go here</li>
                    </ul>
                    <h4>Newsletter Sign-up</h4>
                    <form action="#" class="newsletter d-flex align-items-center">
                        <input type="text" class="form-control" name="newsletter" placeholder="Enter your email here">
                        <div class="input-group-prepend">
                            <button class="input-group-text">
                                Subscribe
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> -->

    <div class="footer-copyright bg-dark footer-p">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 pb-lg-0 pb-3">
                    Copyright © <?= date('Y'); ?>.  PropInside. All right Reserved.
                </div>
                <div class="col-lg-4 pb-lg-0 pb-3">
                    <ul class="d-flex align-items-center">
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms of Use</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 pb-lg-0 pb-3 d-flex align-items-center">
                    <p style="color: #fff; font-size: 12px;">Connect with us:</p>
                    <ul class="pl-2 d-flex">
                        <li><a href="#">Facebook</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Facebook</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>