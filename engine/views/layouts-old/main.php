<?php

use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);

$this->title = 'Homepage';
$theme = Url::base().'/assets';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- site meta -->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="title" content="p1-property" />
        <meta name="description" content="p1-property">
        <meta name="keywords" content="p1-property, study, TEFL">
        <meta name="author" content="p1-property">

        <!-- site favicon -->
        <link rel="icon" href="<?= $theme; ?>/favicon.png" type="image/png">

        <!-- site title -->
        <title>Prop Inside - <?= $this->title; ?></title>
        <?php $this->head() ?>
        <link href="<?= $theme; ?>/castle/css/style.css" rel="stylesheet" type="text/css" />
        <link href="<?= $theme; ?>/castle/css/search.css" rel="stylesheet" type="text/css" />
        <link href="<?= $theme; ?>/castle/css/reality-icon.css" rel="stylesheet" type="text/css" />
        <link href="<?= $theme; ?>/lightslider/css/lightslider.css" rel="stylesheet" type="text/css" />
        <link href="<?= $theme; ?>/lightgallery/css/lightgallery.css" rel="stylesheet" type="text/css" />
        <!-- <link href="<?= $theme; ?>/lightslider/css/custom-lightslider.css" rel="stylesheet" type="text/css" /> -->
        <link href="<?= $theme; ?>/propertyguru/css/desktop.css" rel="stylesheet" type="text/css" />
        <link href="<?= $theme; ?>/metronic/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?= $theme; ?>/castle/css/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="<?= $theme; ?>/castle/css/owl.transitions.css">
        <!-- ==== HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries ==== -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
        .lg-backdrop.in {
            opacity: 0.85;
        }
        .fixed-size.lg-outer .lg-inner {
          background-color: #FFF;
        }
        .fixed-size.lg-outer .lg-sub-html {
          position: absolute;
          text-align: left;
        }
        .fixed-size.lg-outer .lg-toolbar {
          background-color: transparent;
          height: 0;
        }
        .fixed-size.lg-outer .lg-toolbar .lg-icon {
          color: #FFF;
        }
        .fixed-size.lg-outer .lg-img-wrap {
          padding: 12px;
        }
        .select2-search__field {
            font-size: 14px !important;
        }
        .select2-selection__placeholder {
            font-size: 14px !important;
        }
        .select2-selection__rendered {
            font-size: 14px !important;
        }
        .select2-selection__choice {
            font-size: 14px !important;
        }
        .select2-selection__clear {
            top: 4px !important;
        }
        .select2-selection__choice__remove {
            margin: -2px 0 0 3px !important;
        }
        .no-result {
            height: 400px;
        }
        </style>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <!-- start site header -->
        <?php require_once('_header.php') ?>
        <!-- end site header -->

        <!-- start content -->
        <?= $content ?>
        <!-- end content -->

        <!-- start footer -->
        <?php require_once('_footer.php') ?>
        <script src="<?= $theme; ?>/lightslider/js/lightslider.js" type="text/javascript"></script>
        <script src="<?= $theme; ?>/lightgallery/js/lightgallery-all.min.js" type="text/javascript"></script>
        <script src="<?= $theme; ?>/metronic/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>
        <script src="<?= $theme; ?>/castle/js/owl.carousel.min.js"></script> 
        <!-- end footer -->
        <?php $this->endBody() ?>
        <script type="text/javascript">
        jQuery(function($) {
            // "use strict";
            // ********* Check Box 
            $('input[name="radio-btn"]').wrap('<div class="radio-btn"><i></i></div>');
            $(".radio-btn").on("click", function() {
            var _this = $(this),
                block = _this.parent().parent();
                block.find('input:radio').attr('checked', false);
                block.find(".radio-btn").removeClass('checkedRadio');
                _this.addClass('checkedRadio');
                _this.find('input:radio').attr('checked', true);
            });
            // $('input[type="checkbox"]').wrap('<div class="check-box"><i></i></div>');
            $.fn.toggleCheckbox = function() {
                this.attr('checked', !this.attr('checked'));
            }
            $('.check-box').on("click", function() {
                $(this).find(':checkbox').toggleCheckbox();
                $(this).toggleClass('checkedBox');
            });

            $(".advanced").on("click", function() {
                $(".opened").slideToggle();
                return false;
            });

            $('.imageGallery').lightSlider({
                gallery:true,
                item: 1,
                loop: true,
                thumbItem: 9,
                slideMargin: 0,
                enableDrag: false,
                // currentPagerPosition:'left',
                width: '700px',
                height: '170px',
                addClass: 'fixed-size',

                thumbItem: 10,
                pager: true,
                gallery: false,
                galleryMargin: 5,
                thumbMargin: 5,
                currentPagerPosition: 'middle',
                onSliderLoad: function(el) {
                    // console.log("el :: ", el);
                    el.lightGallery({
                        selector: '.imageGallery .lslide',
                    });
                }
            });

            $('.propGallery').lightSlider({
                auto: true,
                gallery:true,
                item: 1,
                loop: true,
                thumbItem: 5,
                slideMargin: 0,
                enableDrag: false,

                pager: true,
                gallery: false,
                galleryMargin: 5,
                thumbMargin: 5,
                currentPagerPosition: 'middle',
                onSliderLoad: function(el) {
                    el.lightGallery({
                        selector: '.propGallery .lslide',
                    });
                },
            });

            $("#agent-2-slider").owlCarousel({
                autoPlay: false,
                stopOnHover: true,
                navigation: true,
                navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                slideSpeed: 300,
                pagination: false,
                singleItem: true
            });
        });

        // function handle(e){
        //     e.preventDefault(); // Otherwise the form will be submitted
        //     LoginDo();
        // }
        $('#login-password').on("keypress", function (e) {
            if (e.keyCode == 13) {
                LoginDo();
            }
        });

        function LoginDo() {
            var url = "<?= Url::to(['/site/loginprocess']); ?>";
            var username = document.getElementById('login-userid').value;
            var pass = document.getElementById('login-password').value;
            var csrf = $('[name="_csrf"]').val();

            $.ajax({
                url: url,
                type: "POST",
                data: {username: username, password: pass, _csrf: csrf},
                dataType: "json",
                success: function(data) {
                    if (data.result == false) {
                        swal({ title: "Error!", text: "Incorrect username or password. Or the user is not yet approved by Administrator.", type: "error" });
                        // alert("Error");
                    } else {
                        // console.log(data);
                        window.location = data.redirectUrl;
                    }
                }
            });
        };

        function addAlert() {
          swal({ title: "Attention!", text: "Property Data that can be submitted only 5. Please check again in your Properties List.", type: "warning" });
        };
        </script>
    </body>
</html>
<?php $this->endPage() ?>