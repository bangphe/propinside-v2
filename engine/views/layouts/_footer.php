<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<!-- footer main -->
<footer class="footer-main text-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-1">
                <div class="footer-logo">
                    <a href="<?= Url::home(); ?>"><img src="<?= Url::base(); ?>/assets/propinside/img/footer-logo.png" alt="logo" class="mw-100"></a>
                </div>
            </div>
            <div class="col-lg-11">
                <div class="footer-info">
                    <p class="color-white">
                        <!-- It's really about you <br>

                        PropInside provides news about the property market in Singapore by showcasing new and upcoming launches. This is done through videos and curated content to help users make an informed decision when it comes to investing or buying a property in Singapore.<br>

                        We also aim to connect buyers to the property listings via agents and their email, mobile or social profiles such as Facebook and Instagram. The social profiles empowers the agents to connect in a more personalised way and users a chance to understand the agent better in the process too. -->
                        &nbsp;&nbsp;&nbsp; <br>
                        &nbsp;&nbsp;&nbsp; <br>
                        &nbsp;&nbsp;&nbsp; <br>
                    </p>
                </div>
            </div>
            <!-- <div class="col-lg-7">
                <div class="row">
                    &nbsp;&nbsp;&nbsp;
                </div>
            </div> -->
            <div class="col-lg-12">
                <div class="divider">
                    <hr>
                </div>
            </div>
            <div class="col-lg-6 col-sm-8">
                <div class="footer-copyright">
                    <!-- <p class="color-white">
                        Lequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dol
                    </p> -->
                    <p class="color-white">
                        Copyright © <?= date('Y'); ?> PropInside
                    </p>
                </div>
            </div>
            <div class="col-lg-6 col-sm-4 d-flex align-items-center justify-content-sm-end justify-content-start">
                <ul class="social-link d-flex align-items-center">
                    <li>
                        <a href="#" class="d-flex align-items-center justify-content-center">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="d-flex align-items-center justify-content-center">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="d-flex align-items-center justify-content-center">
                            <i class="fa fa-google"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- footer main -->