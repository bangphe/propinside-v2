<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\components\MyFormatter;
use yii\widgets\ActiveForm;
use app\models\AgentLoginForm;

$model = new AgentLoginForm();
?>

<!-- header -->
<header class="header-main">

    <!-- topbar -->
    <section class="topbar bg-info text-white d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p>
                        Inside new launches
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- topbar -->

    <!-- head-main -->
    <div class="head-main pt-4 bg-header position-relative">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col">
                    <div class="site-logo">
                        <a href="<?= Url::home(); ?>"><img src="<?= Url::base(); ?>/assets/propinside/img/logo.png" alt="logo" class="mw-100"></a>
                    </div>
                </div>
                <div class="col-lg-10 col mt-5 d-flex align-items-sm-center align-items-end justify-content-end">
                    <nav class="d-none d-lg-inline-block">
                        <ul id="phn-menu" class="MyriadPro main-menu d-flex align-items-center justify-content-center">
                            <li><a href="<?= Url::home(); ?>" class="<?= MyFormatter::active(['site'], 'a'); ?>">New Launches</a></li>
                            <li><a href="<?= Url::to(['/agents']); ?>" class="<?= MyFormatter::active(['agents'], 'c'); ?>">Agents</a></li>
                            <li><a href="<?= Url::to(['/site/about-us']); ?>" class="<?= MyFormatter::active(['site/about-us'], 'a'); ?>">About Us</a></li>
                            <li><a href="<?= Url::to(['/site/contact-us']); ?>" class="<?= MyFormatter::active(['site/contact-us'], 'a'); ?>">Contact Us</a></li>
                            <?php if (Yii::$app->agent->identity == null) { ?>
                            <li>
                                <!-- <a class="pgicon pgicon-user" href="#" title="Log In" id="modal-login-nav" data-toggle="modal" data-target="#modal-login">
                                    <span class="hidden-xs hidden-sm"> Log In </span>
                                </a> -->
                                <a href="#" class="pr-lg-4 pr-2 text-white MyriadPro" title="Log In" id="modal-login-nav" data-toggle="modal" data-target="#modal-login">
                                    <img src="<?= Url::base(); ?>/assets/propinside/img/icons/login.png" alt="login" class="mw-100 mr-sm-2 mr-0">
                                    <span class="d-sm-inline d-none">Log In</span>
                                </a>
                            </li>
                            <!-- <li>
                                <a class="header-ab-test pgicon pgicon-users" href="<?//= Url::to(['site/register']); ?>" title="Sign Up" id="modal-login-nav">
                                    <span class="hidden-xs hidden-sm"> Sign Up</span>
                                </a>
                            </li> -->
                            <?php } else { ?>
                            <li>
                                <?//= Html::a('Logout', ['site/logout'],[
                                  //      'class'=>'pgicon pgicon-user',
                                  //      'data'=>[
                                  //          'method'=>'post'
                                  //      ]
                                  //  ]);
                                ?>
                                <a href="<?= Url::to(['site/logout']); ?>" class="pr-lg-4 pr-2 text-white MyriadPro" title="Log Out" data-method="post">
                                    <img src="<?= Url::base(); ?>/assets/propinside/img/icons/login.png" alt="login" class="mw-100 mr-sm-2 mr-0">
                                    <span class="d-sm-inline d-none">Log Out</span>
                                </a>
                            </li>
                            <<!-- li>
                                <a href="<?//= Url::to(['agents/profile']); ?>" title="Sign Up" id="modal-login-nav" style="margin-left: 20px;">
                                    <span class="hidden-xs hidden-sm"><?//= Yii::$app->agent->identity->name; ?></span>
                                </a>
                            </li> -->
                            <?php } ?>
                        </ul>
                    </nav>
                    <form action="#" class="position-relative search-form">
                        <button type="submit"><i class="fa fa-search"></i></button>
                        <input type="text" class="search-box position-absolute">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- head-main -->

</header>
<!-- header -->

<div class="modal modal-user-box" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="modal-login-title" aria-hidden="true" data-backdrop="static">
    <?php $form = ActiveForm::begin([
        'action' => 'javascript:;',
        'id' => 'login_form',
        'enableClientValidation' => true,
        // 'enableAjaxValidation' => true,
        'options' => [
            'class' => 'form-horizontal modal-form',
            'role' => 'form',
            'method' => 'POST',
            'name' => 'login_form',
            // 'onsubmit' => 'handle(event)',
        ],
    ]); ?>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="modal-login-title" style="margin: 0 75px;">Login to <br>PropInside Website</h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="login-wrapper">
                            <div class="alert alert-danger alert-dismissable hide" id="login-error-message">
                                There seems to be an issue on our end. Please try again later
                            </div>
                            <?php $fieldOptionsUsername = [
                                'options' => ['class' => 'form-group'],
                                'template' => '<i class="pgicon pgicon-user-o"></i>{input}<span class="error-msg">{error}</span>',
                            ]; ?>
                            <?= $form->field($model, 'username', $fieldOptionsUsername)->textInput(['id'=>'login-userid', 'class'=>'form-controls', 'placeholder'=>'Username', 'autocomplete'=>'off'])->label(false) ?>

                            <?php $fieldOptionsPassword = [
                                'options' => ['class' => 'form-group'],
                                'template' => '<i class="pgicon pgicon-key"></i>{input}<span class="error-msg">{error}</span>',
                            ]; ?>
                            <?= $form->field($model, 'password', $fieldOptionsPassword)->passwordInput(['id'=>'login-password', 'class'=>'form-controls', 'placeholder'=>'Password', 'autocomplete'=>'off'])->label(false) ?>
                            <div class="actions">
                                <div class="pull-left stay-login">
                                    <label class="checkbox">
                                        <input name="remember_me" type="checkbox" checked="checked" value=""> Remember Me
                                    </label>
                                </div>
                            </div>
                            <div class="actions">
                                <?= Html::button('Log In', ['id' => 'btn_login', 'name' => 'btn_login', 'class' => 'btn btn-primary btn-md', 'style' => 'width:100%;', 'onclick' => 'LoginDo(); return false;']) ?>
                                <p class="orLabel"><span>Or</span></p>
                            </div>
                            <div class="actions socialNetLogin">
                                <a id="link-facebook-modal-login" class="btn btn-main btn-md pgicon pgicon-facebook pull-left" href="#" disabled>Facebook</a>
                                <a id="link-google-modal-login" class="btn btn-grey btn-md btn-img-google-icon" href="#" disabled>Google</a>
                            </div>
                        
                    </div>
                </div>
                <div class="modal-footer" style="justify-content:center;">
                    <p>Don't have an account? <a id="link-sign-up-modal-login" href="<?= Url::to(['site/register']); ?>" title="Sign Up" target="_blank">Sign Up</a></p>
                    <!-- <p>Are you an agent? <a id="link-login-agentnet-modal-login" href="http://agentnet.propertyguru.com.sg" alt="Log In to AgentNet">Log In to AgentNet</a></p> -->
                </div>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
    <!-- </form> -->
</div>