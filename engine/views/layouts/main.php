<?php

use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);

$theme = Url::base().'/assets';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- site meta -->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="title" content="P3-propinside" />
        <meta name="description" content="P3-propinside">
        <meta name="keywords" content="P3-propinside">
        <meta name="author" content="P3-propinside">

        <!-- site favicon -->
        <link rel="icon" href="<?= $theme; ?>/propinside/favicon.png" type="image/png">

        <!-- site title -->
        <title>Prop Inside - <?= $this->title; ?></title>

        <!-- bootstrap css -->
        <link rel="stylesheet" href="<?= $theme; ?>/propinside/css/bootstrap.min.css">

        <!-- font-awesome css -->
        <link rel="stylesheet" href="<?= $theme; ?>/propinside/css/font-awesome.min.css">

        <!-- Owl Carousel Css -->
        <link rel="stylesheet" href="<?= $theme; ?>/propinside/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<?= $theme; ?>/propinside/css/owl.theme.default.min.css">

        <!-- slicknav css -->
        <link rel="stylesheet" href="<?= $theme; ?>/propinside/css/slicknav.min.css">

        <!-- main stylesheet -->
        <link rel="stylesheet" href="<?= $theme; ?>/propinside/style.css">

        <!-- responsive stylesheet -->
        <link rel="stylesheet" href="<?= $theme; ?>/propinside/css/responsive.css">

        <!-- swal -->
        <link href="<?= $theme; ?>/metronic/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />

        <!-- ==== HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries ==== -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php $this->head() ?>
    </head>
    <body class="MyriadPro">
        <?php $this->beginBody() ?>
        <!-- start site header -->
        <?php require_once('_header.php') ?>
        <!-- end site header -->

        <!-- start content -->
        <?= $content ?>
        <!-- end content -->

        <!-- start footer -->
        <?php require_once('_footer.php') ?>

        <script src="<?= $theme; ?>/metronic/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>

        <!-- Owl Carousel Script -->
        <script type="text/javascript" src="<?= $theme; ?>/propinside/js/owl.carousel.min.js"></script>

        <!-- end footer -->
        <?php $this->endBody() ?>
        <script type="text/javascript">
        jQuery(function($) {
            // "use strict";
            // ********* Check Box 
            $('input[name="radio-btn"]').wrap('<div class="radio-btn"><i></i></div>');
            $(".radio-btn").on("click", function() {
            var _this = $(this),
                block = _this.parent().parent();
                block.find('input:radio').attr('checked', false);
                block.find(".radio-btn").removeClass('checkedRadio');
                _this.addClass('checkedRadio');
                _this.find('input:radio').attr('checked', true);
            });
            // $('input[type="checkbox"]').wrap('<div class="check-box"><i></i></div>');
            $.fn.toggleCheckbox = function() {
                this.attr('checked', !this.attr('checked'));
            }
            $('.check-box').on("click", function() {
                $(this).find(':checkbox').toggleCheckbox();
                $(this).toggleClass('checkedBox');
            });

            $(".advanced").on("click", function() {
                $(".opened").slideToggle();
                return false;
            });

            $('.imageGallery').lightSlider({
                gallery:true,
                item: 1,
                loop: true,
                thumbItem: 9,
                slideMargin: 0,
                enableDrag: false,
                // currentPagerPosition:'left',
                width: '700px',
                height: '170px',
                addClass: 'fixed-size',

                thumbItem: 10,
                pager: true,
                gallery: false,
                galleryMargin: 5,
                thumbMargin: 5,
                currentPagerPosition: 'middle',
                prevHtml : '<a class="lSPrev"><i class="fa fa-angle-left" style="margin-left: -5px; color: white; font-weight: 800;"></i></a>', 
                nextHtml : '<a class="lSNext"><i class="fa fa-angle-right" style="margin-right: -5px; color: white; font-weight: 800;"></i></a>', 
                onSliderLoad: function(el) {
                    // console.log("el :: ", el);
                    el.lightGallery({
                        selector: '.imageGallery .lslide',
                    });
                }
            });

            $('.propGallery').lightSlider({
                auto: true,
                gallery:true,
                item: 1,
                loop: true,
                thumbItem: 5,
                slideMargin: 0,
                enableDrag: false,

                pager: true,
                gallery: false,
                galleryMargin: 5,
                thumbMargin: 5,
                currentPagerPosition: 'middle',
                onSliderLoad: function(el) {
                    el.lightGallery({
                        selector: '.propGallery .lslide',
                    });
                },
            });

            $("#agent-2-slider").owlCarousel({
                autoPlay: false,
                stopOnHover: true,
                navigation: true,
                navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                slideSpeed: 300,
                pagination: false,
                singleItem: true
            });

            // owl-product-promo
            $("#owl-product-promo").owlCarousel({
                // loop:true,
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                nav:true,
                navText: ['<i class="fa fa-caret-left" aria-hidden="true"></i>','<i class="fa fa-caret-right" aria-hidden="true"></i>'],
                dots:false,
                margin: 28,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:2
                    },
                    1000:{
                        items:4
                    }
                }

            }); 
        });

        $('#login-password').on("keypress", function (e) {
            if (e.keyCode == 13) {
                LoginDo();
            }
        });

        function LoginDo() {
            var url = "<?= Url::to(['/site/loginprocess']); ?>";
            var username = document.getElementById('login-userid').value;
            var pass = document.getElementById('login-password').value;
            var csrf = $('[name="_csrf"]').val();

            $.ajax({
                url: url,
                type: "POST",
                data: {username: username, password: pass, _csrf: csrf},
                dataType: "json",
                success: function(data) {
                    if (data.result == false) {
                        swal({ title: "Error!", text: "Incorrect username or password. Or the user is not yet approved by Administrator.", type: "error" });
                    } else {
                        window.location = data.redirectUrl;
                    }
                }
            });
        };

        function addAlert() {
          swal({ title: "Attention!", text: "Property Data that can be submitted only 5. Please check again in your Properties List.", type: "warning" });
        };
        </script>
    </body>
</html>
<?php $this->endPage() ?>