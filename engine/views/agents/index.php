<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Agents';
?>

<style type="text/css">
.test {
    width: 350px;
    height: 263px;
}
.demo-gallery-poster {
    background-color: rgba(0, 0, 0, 0.1);
    margin-top: -165px;
    -webkit-transition: background-color 0.15s ease 0s;
    -o-transition: background-color 0.15s ease 0s;
    transition: background-color 0.15s ease 0s;
}
</style>

<section class="property-main">
    <div class="container bg-white">
        <div class="row">
            <div class="col-lg-12">
                <h3><?= $this->title; ?></h3>
            </div>
        </div>
    </div>
</section>

<!-- searching box -->
<!-- <section class="searching-box">
    <div class="container">
        <form action="#" class="searching-form d-flex align-items-center justify-content-center">
            <div class="row w-100 align-items-center">
                <div class="col-sm-5">
                    <input type="text" class="w-100" placeholder="Search by Location">
                </div>
                <div class="col-sm-5">
                    <input type="text" class="w-100" placeholder="Search by keyword">
                </div>
                <div class="col-sm-2 text-center text-sm-right">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </form>
    </div>
</section> -->
<!-- searching box -->

<!-- agents section -->
<section class="agent-details-box pt-4">
    <!-- <div class="container">
        <div class="row">
            <div class="col-lg-12 d-flex" style="margin-left: -15px;">
                <div class="agent-details-title-box text-uppercase px-4 bg-primary text-white d-flex align-items-center justify-content-center">Agents Detail</div>
            </div>
        </div>
    </div> -->
    <div class="container">
        <div class="row">
            <?= \yii\widgets\ListView::widget([
                'id'           => 'agents-index',
                'dataProvider' => $dataProvider,
                'itemView'     => '_list_agent',
                'layout'       => '
                    {items}
                    <section class="pagination-main">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 d-flex justify-content-end">
                                  {pager}
                                </div>
                            </div>
                        </div>
                    </section>
                ',
                // 'itemOptions' => [
                  // 'class' => 'col-md-4',
                // ],
                'emptyText' => '<p>'.\Yii::t('app', 'No results found').'</p>',
                'emptyTextOptions' => ['tag' => 'div', 'class' => 'container text-center'],
                'pager' => [
                    'prevPageLabel' => '<',
                    'nextPageLabel' => '>',
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last',
                    'maxButtonCount' => 3,
                    
                    // Customzing options for pager container tag
                    'options' => [
                        'tag' => 'ul',
                        'class' => 'pagination-list d-flex align-items-center justify-content-end',
                        'id' => 'pager-container',
                    ],
                ],
            ]); ?>
        </div>
    </div>
</section>
<!-- agents section -->