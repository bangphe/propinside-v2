<?php
use yii\helpers\Url;
use yii\helpers\Html;

?>

<!-- agent-details-items -->
<div class="agent-details-items bg-white">
    <div class="row">
        <div class="col-sm-4">
            <div class="agent-details-img text-center mb-sm-0 mb-4">
                <?php if($model->photo != NULL) { ?>
                    <img src="<?= Yii::getAlias('@web').$model->photo; ?>" alt="agents" class="mw-100" width="200px" height="250px">
                <?php } else { ?>
                    <img src="<?= Yii::getAlias('@web/assets/propinside/img/team/t2.png'); ?>" alt="team" class="w-100">
                <?php } ?>
                <div class="d-flex justify-content-center mt-4">
                    <ul class="social-link d-flex align-items-center justify-content-center">
                        <li>
                            <?php if ($model->facebook != NULL) { ?>
                            <a href="<?= $model->facebook; ?>" class="d-flex align-items-center justify-content-center" target="_blank">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <?php } else { ?>
                            <a href="javascript:;" class="d-flex align-items-center justify-content-center" style="color: gray;">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <?php } ?>
                        </li>
                        <li>
                            <?php if ($model->twitter != NULL) { ?>
                            <a href="<?= $model->twitter; ?>" class="d-flex align-items-center justify-content-center" target="_blank">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <?php } else { ?>
                            <a href="javascript:;" class="d-flex align-items-center justify-content-center" style="color: gray;">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <?php } ?>
                        </li>
                        <li>
                            <?php if ($model->twitter != NULL) { ?>
                            <a href="<?= $model->linkedin; ?>" class="d-flex align-items-center justify-content-center" target="_blank">
                                <i class="fa fa-linkedin"></i>
                            </a>
                            <?php } else { ?>
                            <a href="javascript:;" class="d-flex align-items-center justify-content-center" style="color: gray;">
                                <i class="fa fa-linkedin"></i>
                            </a>
                            <?php } ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="agent-details-info">
                <h4><?= strtoupper($model->name); ?></h4>
                <p class="text-justify">
                    <?= $model->description != NULL ? $model->description : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'; ?>
                </p>
                <ul class="d-sm-flex align-items-center justify-content-between mb-5">
                    <li><span class="text-info">Mobile :</span> <?= $model->phone_number; ?></li>
                    <li><span class="text-info">Office :</span> <?= $model->office != null ? $model->office : '-'; ?></li>
                    <li><span class="text-info">Fax :</span> <?= $model->fax != null ? $model->fax : '-'; ?></li>
                </ul>
                <div class="d-flex justify-content-start justify-content-sm-end">
                    <a href="<?= Url::to(['/agents/detail?id='.$model->id_agent]); ?>" class="btn btn-info MyriadPro d-inline-flex align-items-center justify-content-center px-4 mt-lg-5">
                        Learn More
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<!-- agent-details-items -->