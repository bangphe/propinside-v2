<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<!-- property main section -->
<section class="property-main">
	<div class="container">

		<div class="row property-img my-5">
			<!-- <div class="col-sm-8 property-thum position-relative">
				<img src="<?//= Url::base(); ?>/assets/img/properties/2.png" alt="properties" class="mw-100 w-100 h-100 position-absolute">
			</div>
			<div class="col-sm-4 property-img-multi">
				<img src="<?//= Url::base(); ?>/assets/img/properties/3.png" alt="properties" class="mw-100">
				<img src="<?//= Url::base(); ?>/assets/img/properties/4.png" alt="properties" class="mw-100">
			</div> -->
			<div class="col-md-12 listing1 property-details">
		        <ul class="propGallery">
			        <?php
			        	if ($model->propertyImages) {
			        		foreach ($model->propertyImages as $v) {
			        ?>
	                <li class="img-fluid img-thumbnail" data-src="<?= Yii::getAlias('@web').$v->image_path; ?>">
	                    <img class="img-fluid img-thumbnail mx-auto d-block" src="<?= Yii::getAlias('@web').$v->image_path; ?>" style="width: 350px;" />
	                </li>
	            	<?php } } else { ?>
	            	<?php for ($i=2; $i < 5; $i++) { ?>
	            		<li class="img-fluid img-thumbnail" data-src="<?= Url::base().'/assets/img/properties/'.$i.'.png'; ?>">
		                    <img class="img-fluid img-thumbnail mx-auto d-block" src="<?= Url::base().'/assets/img/properties/'.$i.'.png'; ?>" style="width: 350px;" />
		                </li>
	            	<?php } } ?>
	            </ul>
			</div>
		</div>

		<div class="row">

			<div class="col-lg-9">
				<!-- property-price-size -->
				<div class="property-price-size">
					<h4 class="PalatinoLinotypeBold d-flex align-items-center">
						$ <?= number_format($model->price, 0); ?>
						<ul class="ml-5 d-inline-flex align-items-center">
							<li>STARTING FROM</li>
							<li>Est. Mortgage $3,110 /mo</li>
							<li>Find your eligibility</li>
						</ul>
					</h4>
					<div class="pro-size-info d-flex align-items-center">
						<ul class="pro-size d-flex align-items-center">
							<li>Beds 2</li>
							<li>Beds 2</li>
						</ul>
						<span class="ml-4 mr-4">sqft</span>
						<ul class="pro-price d-flex align-items-center">
							<li><span>$</span> 1,548.39</li>
							<li><span>psf</span> <?= $model->psf; ?></li>
						</ul>
					</div>
				</div>
				<!-- property-price-size -->
			</div>

			<div class="col-lg-12">
				<!-- property-address -->
				<div class="pro-address">
					<h4 class="PalatinoLinotypeBold">Lakeville</h4>
					<p>
						<?= $model->location; ?>
						<i class="fa fa-map-marker text-warning"></i>
					</p>
					<p class="text-danger">
						Discover more about Jurong
					</p>
				</div>
				<!-- property-address -->
			</div>

			<div class="col-lg-10">
				<!-- property detail -->
				<div class="pro-detail">
					<h4>Details</h4>
					<div class="row">
						<div class="col-sm-6">
							<ul class="form-read-only">
								<li>
									<h6>Type</h6>
									<p><?= $model->type; ?></p>
								</li>
								<li>
									<h6>Tenure</h6>
									<p><?= $model->tenure; ?></p>
								</li>
								<li>
									<h6>Floor Size</h6>
									<p><?= $model->floor_size; ?></p>
								</li>
								<li>
									<h6>Developer</h6>
									<p><?= $model->developer; ?></p>
								</li>
								<li>
									<h6>Land Size</h6>
									<p><?= $model->land_size; ?></p>
								</li>
							</ul>
						</div>
						<div class="col-sm-6">
							<ul class="form-read-only">
								<li>
									<h6>Psf</h6>
									<p><?= $model->psf; ?></p>
								</li>
								<li>
									<h6>Furnishing</h6>
									<p><?= $model->furnishing; ?></p>
								</li>
								<li>
									<h6>Top</h6>
									<p><?= $model->top; ?></p>
								</li>
								<li>
									<h6>Floor Levek</h6>
									<p><?= $model->floor_level; ?></p>
								</li>
								<li>
									<h6>Type</h6>
									<p>Condominium For Sale</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- property detail -->
			</div>

			<div class="col-lg-10">
				<!-- property description -->
				<div class="pro-description">
					<h5>
						Description
					</h5>
					<p>
						<?= $model->description; ?>
					</p>
				</div>
				<!-- property description -->
			</div>

			<div class="col-lg-10">
				<!-- property facilities & info -->
				<div class="pro-facilities" id="accordion">
					<h5>Facilities & Amenities</h5>
					<ul class="facilities-accordin row mx-0">
						<li class="col px-0">
							<button data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Facilities</button>
						</li>
						<li class="col px-0">
							<button data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Amenities</button>
						</li>
					</ul>
					<div class="collapse show" id="collapseOne" aria-labelledby="headingOne" data-parent="#accordion">
						<div class="row fac-accordin-body">
							<?php foreach ($model->propertyHasFacilities as $v) { ?>
							<div class="col-sm-6 d-flex align-items-center">
								<div><img src="<?= Url::base(); ?>/assets/img/icons/star.png" alt="star" class="mw-100"></div>
								<p><?= $v->facilities->facilities; ?></p>
							</div>
							<?php } ?>
						</div>
					</div>
					<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
						<div class="row fac-accordin-body">
							<?php foreach ($model->propertyHasAmenities as $v) { ?>
							<div class="col-sm-6 d-flex align-items-center">
								<div><img src="<?= Url::base(); ?>/assets/img/icons/star.png" alt="star" class="mw-100"></div>
								<p><?= $v->amenities->amenities; ?></p>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="pro-info">
					<h5>Project Info</h5>
					<p><?= $model->title; ?></p>
					<div class="row">
						<div class="col-sm-6 d-flex align-items-center">
							<img src="<?= Url::base(); ?>/assets/img/properties/5.png" alt="properties" class="mw-100 w-100">
						</div>
						<div class="col-sm-6 d-flex align-items-center">
							<ul class="form-read-only">
								<li>
									<h6>Type</h6>
									<p><?= $model->type; ?></p>
								</li>
								<li>
									<h6>Tenure</h6>
									<p><?= $model->tenure; ?></p>
								</li>
								<li>
									<h6>Floor Size</h6>
									<p><?= $model->floor_size; ?></p>
								</li>
								<li>
									<h6>Developer</h6>
									<p><?= $model->developer; ?></p>
								</li>
								<li>
									<h6>Land Size</h6>
									<p><?= $model->land_size; ?></p>
								</li>
							</ul>
						</div>
					</div>
					<div class="pro-actions">
						<ul class="d-flex align-items-center">
							<li><a href="#" class="text-danger">&lt; Lakeville Project Details &gt;</a></li>
							<li><a href="#" class="text-danger">Read Review of Lakeville</a></li>
						</ul>
						<ul class="d-flex align-items-center">
							<li><a href="#" class="text-danger">View other listing in Lakeville</a></li>
						</ul>
					</div>
				</div>
				<!-- property facilities & info -->
			</div>

			<div class="col-lg-12">
				<!-- pricing insights -->
				<div class="pricing-insights-main">
					<!-- <h4 class="d-inline-flex align-items-center justify-content-center"><i class="fa fa-flag d-inline-flex align-items-center justify-content-center"></i> Pricing Insights</h4> -->
					<div class="row">

						<!-- <div class="col-lg-4">
								<div class="pricing-insights-head">
									<img src="<?= Url::base(); ?>/assets/img/properties/6.png" alt="properties" class="mw-100 w-100">
								</div>
								<div class="pricing-insights-info">
									<p>
										Single Family Home
									</p>
									<h4>S$954,000</h4>
									<ul class="d-flex align-items-center justify-content-center">
										<li>2,503Sq Ft</li>
										<li>5 Beds</li>
										<li>2 Baths</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="pricing-insights text-center">
								<div class="pricing-insights-head">
									<img src="<?= Url::base(); ?>/assets/img/properties/6.png" alt="properties" class="mw-100 w-100">
								</div>
								<div class="pricing-insights-info">
									<p>
										Single Family Home
									</p>
									<h4>S$954,000</h4>
									<ul class="d-flex align-items-center justify-content-center">
										<li>2,503Sq Ft</li>
										<li>5 Beds</li>
										<li>2 Baths</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="pricing-insights text-center">
								<div class="pricing-insights-head">
									<img src="<?= Url::base(); ?>/assets/img/properties/6.png" alt="properties" class="mw-100 w-100">
								</div>
								<div class="pricing-insights-info">
									<p>
										Single Family Home
									</p>
									<h4>S$954,000</h4>
									<ul class="d-flex align-items-center justify-content-center">
										<li>2,503Sq Ft</li>
										<li>5 Beds</li>
										<li>2 Baths</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="pricing-insights text-center">
								<div class="pricing-insights-head">
									<img src="<?= Url::base(); ?>/assets/img/properties/6.png" alt="properties" class="mw-100 w-100">
								</div>
								<div class="pricing-insights-info">
									<p>
										Single Family Home
									</p>
									<h4>S$954,000</h4>
									<ul class="d-flex align-items-center justify-content-center">
										<li>2,503Sq Ft</li>
										<li>5 Beds</li>
										<li>2 Baths</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="pricing-insights text-center">
								<div class="pricing-insights-head">
									<img src="<?= Url::base(); ?>/assets/img/properties/6.png" alt="properties" class="mw-100 w-100">
								</div>
								<div class="pricing-insights-info">
									<p>
										Single Family Home
									</p>
									<h4>S$954,000</h4>
									<ul class="d-flex align-items-center justify-content-center">
										<li>2,503Sq Ft</li>
										<li>5 Beds</li>
										<li>2 Baths</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="pricing-insights text-center">
								<div class="pricing-insights-head">
									<img src="<?= Url::base(); ?>/assets/img/properties/6.png" alt="properties" class="mw-100 w-100">
								</div>
								<div class="pricing-insights-info">
									<p>
										Single Family Home
									</p>
									<h4>S$954,000</h4>
									<ul class="d-flex align-items-center justify-content-center">
										<li>2,503Sq Ft</li>
										<li>5 Beds</li>
										<li>2 Baths</li>
									</ul>
								</div>
							</div>
						</div> -->

						<!-- <div class="col-lg-12 text-center"> -->
							<!-- <a href="<?//= Url::to(['/site/property']); ?>" class="btn btn-primary d-lg-inline-flex d-flex align-items-center justify-content-center"> -->
								<!-- VIEW ALL LISTINGS -->
							<!-- </a> -->
						<!-- </div> -->
					</div>
					<p class="pricing-insights-warn">risk level of investment (based on property expert)</p>
				</div>
				<!-- pricing insights -->
			</div>

			<div class="col-lg-12">

				<!-- agents section -->
				<div class="agents-main">
					<div class="agents-head text-center">
						<h4>Contact Agent About This Property</h4>
						<p>
							Ut et, ex sed veniam tempor, irure eu, magna nisi. Minim id esse laboris magna do. Qui id ad. Labore exercitation do commodo dolore. Labore anim mollit in enim, sit est, amet nulla eiusmod magna sint in deserunt, in aliquip.
						</p>
					</div>
					<div class="row">
						<?php foreach ($model->propertyListings as $val) { ?>
						<div class="col-lg-2 col-sm-4">
							<!-- agents item -->
							<div class="agents-item">
								<a href="<?= Url::to(['/agents/detail?id='.$val->id_agent]); ?>">
									<?php if($val->agent->photo != NULL) { ?>
			                            <img src="<?= Yii::getAlias('@web').$val->agent->photo; ?>" alt="properties" class="mw-100 img-fluid" width="100%">
			                        <?php } else { ?>
			                            <img src="<?= Url::base(); ?>/assets/img/testimonial/1.png" alt="testimonial" class="mw-100">
			                        <?php } ?>
									<h6><?= $val->agent->name; ?></h6>
									<p>
										<?= $val->agent->description; ?>
									</p>
									<ul class="social-link d-flex align-items-center">
										<li>
											<a href="<?= $val->agent->facebook; ?>" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-facebook"></i>
											</a>
										</li>
										<li>
											<a href="<?= $val->agent->twitter; ?>" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-twitter"></i>
											</a>
										</li>
										<li>
											<a href="<?= $val->agent->linkedin; ?>" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-linkedin"></i>
											</a>
										</li>
									</ul>
								</a>
							</div>
							<!-- agents item -->
						</div>
						<?php } ?>
					</div>
				</div>
				<!-- agents section -->

			</div>

		</div>
	</div>
</section>
<!-- property main section -->