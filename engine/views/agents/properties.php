<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Agents Properties';
$theme = Url::base().'/assets/castle';
?>

<style type="text/css">
.prop-img {
  width: 360px;
  height: 238px;
}
</style>

<!-- agents properties page -->
<section id="agent-2-peperty" class="my-pro padding_half">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="f-p-links bottom40">
          <li><a href="<?= Url::to(['agents/profile']); ?>"><i class="icon-icons230"></i>Profile</a></li>
          <li><a href="#" class="active"><i class="icon-icons215"></i> My Properties</a></li>
          <li>
          <?php if ($dataProvider->getTotalCount() >= 5) { ?>
            <a href="#" onclick="addAlert()"><i class="icon-icons202"></i> Submit Property</a>
          <?php } else { ?>
            <a href="<?= Url::to(['agents/submit-property']); ?>"><i class="icon-icons202"></i> Submit Property</a>
          <?php } ?>
          </li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-center">
        <h2 class="text-uppercase">My Properties</h2>
        <?= Yii::$app->session->getFlash('info');?>
      </div>
    </div>
    <?= \yii\widgets\ListView::widget([
          'id'           => 'agent-property',
          'dataProvider' => $dataProvider,
          'itemView'     => '_property_item',
          'layout'       => '
              {items}
              <div class="row">
                <div class="col-md-12">
                  {pager}
                </div>
              </div>
          ',
          'itemOptions' => [
            'class' => 'container',
          ],
          'emptyText' => '<p>'.\Yii::t('app', 'No results found').'</p>',
          'emptyTextOptions' => ['tag' => 'div', 'class' => 'container text-center'],
          'pager' => [
                'prevPageLabel' => 'Previous',
                'nextPageLabel' => 'Next',
                'maxButtonCount' => 3,
                
                // Customzing options for pager container tag
                'options' => [
                    'tag' => 'ul',
                    'class' => 'pagination justify-content-end',
                    'id' => 'pager-container',
                ],
            ],
      ]); ?>
  </div>
</section>
<!-- agents properties page -->