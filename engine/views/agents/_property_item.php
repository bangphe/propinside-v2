<?php
use yii\helpers\Url;
use yii\helpers\Html;

?>

<div class="row property-list-item">
  <div class="col-sm-4 d-flex align-items-center">
    <?php if($model->property->propertyImages != NULL && $model->property->propertyImages[0]->image_path != NULL) { ?>
        <img src="<?= Yii::getAlias('@web').$model->property->propertyImages[0]->image_path; ?>" alt="properties" class="mw-100 img-fluid img-thumbnail prop-img">
    <?php } else { ?>
        <img src="<?= Url::base(); ?>/assets/img/properties/9.png" alt="properties" class="mw-100 img-fluid img-thumbnail prop-img">
    <?php } ?>
  </div>
  <div class="col-sm-8 mt-sm-0 mt-4 d-flex align-items-center">
    <div class="property-list-info">
      <h4><?= $model->property->title ? $model->property->title : 'Title'; ?></h4>
      <h5><?= 'Location : ' . $model->property->location; ?>. Posted by: <?= $model->agent->name; ?></h5>
      <p>
        <?= $model->property->description; ?>
      </p>
      <a href="<?= Url::to(['agents/property/'.$model->property->slug]); ?>" class="btn btn-info d-inline-flex align-items-center justify-content-center" target="_blank">Learn More</a>
      <div class="float-right">
        <?= Html::a('<i class="fa fa-pencil"></i>', ['update','id'=>$model->id_property], ['class'=>'btn btn-success m-btn m-btn--icon m-btn--icon-only']); ?>
        <?= Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id_property],
            [
                'class'=>'btn btn-danger m-btn m-btn--icon m-btn--icon-only',
                // 'onclick'=>'deleteRow('.$model->id_property.')',
                'data' => [
                  'confirm' => 'Are you sure you want to delete this item?',
                  'method' => 'post',
                ],
            ]);
        ?>
      </div>
    </div>
  </div>
</div>