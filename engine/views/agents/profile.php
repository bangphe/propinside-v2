<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
// use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Agents Profile';
$theme = Url::base().'/assets/castle';
// $avatarImage = ($model->photo) ? Yii::getAlias('@web').$model->photo : Yii::getAlias('@web/assets/castle/images/default.jpg');
$avatarImage = ($model->photo) ? Yii::getAlias('@web').$model->photo : Url::base().'/assets/img/testimonial/7.png';
?>

<!-- agents profile page -->
<section id="agent-2-peperty" class="profile padding_half">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="f-p-links bottom30">
          <li><a href="#" class="active"><i class="icon-icons230"></i>Profile</a></li>
          <li><a href="<?= Url::to(['agents/my-properties']); ?>"><i class="icon-icons215"></i> My Properties</a></li>
          <li>
            <?php if ($propCount >= 5) { ?>
              <a href="#" onclick="addAlert()"><i class="icon-icons202"></i> Submit Property</a>
            <?php } else { ?>
              <a href="<?= Url::to(['agents/submit-property']); ?>"><i class="icon-icons202"></i> Submit Property</a>
            <?php } ?>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-3">
    <div class="row">
      <?php $form = ActiveForm::begin([
        'id' => 'profile-form',
        'enableClientValidation' => true,
        'options' => [
              'class' => 'callus',
              'enctype'=>'multipart/form-data',
          ],
      ]); ?>
      <div class="col-md-4 col-sm-6 col-xs-12">
        <h2 class="text-uppercase bottom30 text-center">my profile</h2>
        <?= Yii::$app->session->getFlash('info');?>
        <div class="agent-p-img">
          <!-- <img src="<?= $theme; ?>/images/profile.jpg" class="img-responsive" alt="image"> -->
          <!-- <a href="#" class="top10 bottom20">Update Profile Picture</a> -->
          <?= $form->field($model, 'photo')->widget(\kartik\file\FileInput::classname(), [
              'options' => [
                  'accept' => 'image/*',
                  'data-image'=>$avatarImage,
              ],
              'pluginOptions' => [
                  'overwriteInitial'=> true,
                  'maxFileSize'=> 1500,
                  'showClose'=> false,
                  'showRemove' => false,
                  'showCaption'=> false,
                  'showBrowse'=> false,
                  'browseOnZoneClick'=> true,
                  'removeLabel'=> '',
                  'removeIcon'=> '<i class="glyphicon glyphicon-remove"></i>',
                  'removeTitle'=> 'Cancel or reset changes',
                  'elErrorContainer'=> '.image-upload-error',
                  'msgErrorClass'=> 'alert alert-block alert-danger',
                  'defaultPreviewContent'=> '<img src="'.$avatarImage.'" alt="Your Avatar" style="width:160px"><h6 class="text-muted">' . \Yii::t('app','Click to change picture') . '</h6>',
                  'layoutTemplates'=> ['main2'=> '{preview} {remove} {browse}'],
                  'allowedFileTypes' => ["image"],
              ]
          ])->label(false); ?>
          <p class="text-center">Minimum 215px x 215px<span>*</span></p>
        </div>
      </div>
      <div class="col-md-8">
        <div class="profile-form">
          <div class="row">
              <div class="col-sm-4">
                <div class="single-query">
                  <label>Your Name:</label>
                </div>
              </div>
              <div class="col-sm-8">
                <div class="single-query form-group">
                  <?= $form->field($model, 'name')->textInput(['class'=>'keyword-input', 'placeholder'=>'Full Name'])->label(false) ?>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="single-query">
                  <label>Mobile:</label>
                </div>
              </div>
              <div class="col-sm-8">
                <div class="single-query form-group">
                  <?= $form->field($model, 'phone_number')->textInput(['class'=>'keyword-input', 'placeholder'=>'(+01) 34 56 7890', 'type'=>'number'])->label(false) ?>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="single-query">
                  <label>Office:</label>
                </div>
              </div>
              <div class="col-sm-8">
                <div class="single-query form-group">
                  <?= $form->field($model, 'office')->textInput(['class'=>'keyword-input', 'placeholder'=>'(+01) 34 56 7890'])->label(false) ?>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="single-query">
                  <label>Fax:</label>
                </div>
              </div>
              <div class="col-sm-8">
                <div class="single-query form-group">
                  <?= $form->field($model, 'fax')->textInput(['class'=>'keyword-input', 'placeholder'=>'(+01) 34 56 7890'])->label(false) ?>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="single-query">
                  <label>Email Address:</label>
                </div>
              </div>
              <div class="col-sm-8">
                <div class="single-query form-group">
                  <?= $form->field($model, 'email')->textInput(['class'=>'keyword-input', 'placeholder'=>'bohdan@realtyhomes.com'])->label(false) ?>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="single-query">
                  <label>Gender:</label>
                </div>
              </div>
              <div class="col-sm-8">
                <div class="single-query form-group">
                  <?= $form->field($model, 'gender')->dropDownList($model->getAllGender(), ['style'=>'height:15% !important;'])->label(false) ?>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="single-query">
                  <label>Title:</label>
                </div>
              </div>
              <div class="col-sm-8">
                <div class="single-query form-group">
                  <?= $form->field($model, 'title')->textInput(['class'=>'keyword-input', 'placeholder'=>'Title'])->label(false) ?>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="single-query">
                  <label>Country:</label>
                </div>
              </div>
              <div class="col-sm-8">
                <div class="single-query form-group">
                  <?= $form->field($model, 'country_operability')->textInput(['class'=>'keyword-input', 'placeholder'=>'Country'])->label(false) ?>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="single-query">
                  <label>Speciality:</label>
                </div>
              </div>
              <div class="col-sm-8">
                <div class="single-query form-group">
                  <?= $form->field($model, 'speciality')->textInput(['class'=>'keyword-input', 'placeholder'=>'Speciality'])->label(false) ?>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="single-query">
                  <label>About:</label>
                </div>
              </div>
              <div class="col-sm-8">
                <div class="single-query form-group">
                  <?= $form->field($model, 'description')->textarea(['class'=>'form-control', 'placeholder'=>'Write here something about yours'])->label(false) ?>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                <?= Html::submitButton('Save Changes', ['class' => 'btn-blue border_radius']) ?>
              </div>
            <?php ActiveForm::end(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-5 col-sm-6 col-xs-12 profile-form margin20">
        <h3 class="bottom30 margin20">My Social Network</h3>
        <div class="row">
          <?php $form = ActiveForm::begin([
            'id' => 'social-form',
            'enableClientValidation' => true,
            'options' => [
                  'class' => 'callus',
              ],
          ]); ?>
            <div class="col-sm-4">
              <div class="single-query">
                <label>Facebook:</label>
              </div>
            </div>
            <div class="col-sm-8">
              <div class="single-query form-group">
                <?= $form->field($model, 'facebook')->textInput(['class'=>'keyword-input', 'placeholder'=>'http://facebook.com'])->label(false) ?>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="single-query">
                <label>Twitter:</label>
              </div>
            </div>
            <div class="col-sm-8">
              <div class="single-query form-group">
                <?= $form->field($model, 'twitter')->textInput(['class'=>'keyword-input', 'placeholder'=>'http://twitter.com'])->label(false) ?>
              </div>
            </div>
            <!-- <div class="col-sm-4">
              <div class="single-query">
                <label>Google Plus:</label>
              </div>
            </div>
            <div class="col-sm-8">
              <div class="single-query form-group">
                <input type="text" placeholder="http://google-plus.com" class="keyword-input">
              </div>
            </div> -->
            <div class="col-sm-4">
              <div class="single-query">
                <label>Linkedin:</label>
              </div>
            </div>
            <div class="col-sm-8">
              <div class="single-query form-group">
                <?= $form->field($model, 'linkedin')->textInput(['class'=>'keyword-input', 'placeholder'=>'http://linkedin.com'])->label(false) ?>
              </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 text-right">
              <?= Html::submitButton('Save Changes', ['class' => 'btn-blue border_radius']) ?>
            </div>
          <?php ActiveForm::end(); ?>
        </div>
      </div>
      <div class="col-md-2 hidden-xs"></div>
      <div class="col-md-5 col-sm-6 col-xs-12 profile-form margin20">
        <h3 class=" bottom30 margin20">Change Your Password</h3>
        <div class="row">
          <?php $form = ActiveForm::begin([
            'id' => 'password-form',
            'enableClientValidation' => true,
            'options' => [
                  'class' => 'callus',
              ],
          ]); ?>
            <!-- <div class="col-sm-4">
              <div class="single-query">
                <label>Current Password:</label>
              </div>
            </div>
            <div class="col-sm-8">
              <div class="single-query form-group">
                <?//= $form->field($model, 'password')->passwordInput(['class'=>'keyword-input'])->label(false) ?>
              </div>
            </div> -->
            <div class="col-sm-4">
              <div class="single-query">
                <label>New Password:</label>
              </div>
            </div>
            <div class="col-sm-8">
              <div class="single-query form-group">
                <?= $form->field($model, 'new_password')->passwordInput(['class'=>'keyword-input'])->label(false) ?>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="single-query">
                <label>Confirm Password:</label>
              </div>
            </div>
            <div class="col-sm-8">
              <div class="single-query form-group">
                <?= $form->field($model, 'confirm_password')->passwordInput(['class'=>'keyword-input'])->label(false) ?>
              </div>
            </div>
            <div class="col-sm-12 text-right">
              <?= Html::submitButton('Save Changes', ['class' => 'btn-blue border_radius']) ?>
            </div>
          <?php ActiveForm::end(); ?>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- agents profile page -->