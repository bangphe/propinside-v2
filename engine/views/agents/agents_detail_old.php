<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use app\models\Property;
?>

<style type="text/css">
input::-webkit-input-placeholder {
    font-weight: bold;
}
input:-moz-placeholder {
    font-weight: bold;
}
input:-ms-input-placeholder {
    font-weight: bold;
}
</style>

<!-- agents profile section -->
<section class="agents-profile-main">
	<div class="container">
		<div class="row">
			<!-- <div class="col-lg-12">
				<h4 class="agents-profile-title text-center">AGENT PROFILE</h4>
			</div> -->
			<?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
            <div class="col-lg-12">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                  <strong>Congratulations!</strong> Thank you for contacting us. We will respond to you as soon as possible.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
            </div>
	        <?php endif; ?>

			<div class="col-lg-6 offset-lg-1">

				<!-- profile info -->
				<div class="profile-info">
					<div class="row">
						<div class="col-sm-5">
							<div class="profile-about" style="margin-bottom: 10px;">
								<h3>About <?= $model->name; ?></h3>
							</div>
							<div class="profile-img">
								<?php if($model->photo != NULL) { ?>
		                            <img src="<?= Yii::getAlias('@web').$model->photo; ?>" alt="properties" class="mw-100 img-fluid" width="100%">
		                        <?php } else { ?>
		                            <?php if($model->gender == 'Male') { ?>
					                    <img src="<?= Url::base(); ?>/assets/img/man.jpg" alt="agents" class="mw-100 rounded-circle" width="195px" height="190px">
					                <?php } else { ?>
					                    <img src="<?= Url::base(); ?>/assets/img/woman.jpg" alt="agents" class="mw-100 rounded-circle" width="195px" height="190px">
					                <?php } ?>
		                        <?php } ?>
								<h4><?= strtoupper($model->name); ?></h4>
								<p><?= $model->country_operability; ?></p>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="profile-about">
								<!-- <h3>About <?//= $model->name; ?></h3> -->
								<p>
									<?= $model->description; ?>
								</p>
							</div>
						</div>
					</div>
					<ul class="profile-info-list">
						<li>
							<span>Registration Number:</span>
							<span>123456789</span>
						</li>
						<li>
							<span>Agent Number:</span>
							<span><?= $model->id_agent; ?></span>
						</li>
						<li>
							<span>Phone:</span>
							<span><?= $model->phone_number; ?></span>
						</li>
						<li>
							<span>Email:</span>
							<span><?= $model->email; ?></span>
						</li>
						<li>
							<span>Agent Award:</span>
							<span><?= $model->award; ?></span>
						</li>
						<li>
							<span>Agent Specialty:</span>
							<span><?= $model->speciality; ?></span>
						</li>
						<li>
							<span>Country Operability:</span>
							<span><?= $model->country_operability; ?></span>
						</li>
						<li>
							<span>Investment Deposit Range:</span>
							<span><?= $model->investment_deposit_range; ?></span>
						</li>
					</ul>
				</div>
				<!-- profile info -->

				<!-- best offer -->
				<div class="best-offer">
					<h4>My Top 5 Properties</h4>
					<div class="row">
						<?php if(empty($model->propertyListings)) { ?>
							<p style="margin:16px;"><?= "No results found." ?></p>
						<?php } else { ?>
						<?php foreach ($model->propertyListings as $value) { ?>
						<div class="col-sm-6">
							<!-- best-offer-box -->
						<?php if($value->property->is_published == Property::STATUS_APPROVED){ ?>
						<a href="<?= Url::to(['/agents/property/'.$value->property->slug]); ?>">
							<div class="best-offer-box">
								<div class="best-offer-head">
									<div class="best-offer-head-info">
										<h5><?= strtoupper($value->property->type); ?></h5>
										<p>
											<?= $value->property->title; ?>
										</p>
										<span><i class="fa fa-map-marker text-warning"></i> <?= $value->property->location; ?></span>
									</div>
									<div class="best-offer-img position-relative">
										<img src="<?= Url::base(); ?>/assets/img/properties/6.png" alt="properties" class="mw-100">
										<ul class="best-offer-img-action d-flex align-items-center position-absolute">
											<li><a href="#" class="btn btn-warning">REUSED</a></li>
											<li><a href="#" class="btn btn-info">FOR RENT</a></li>
											<li><a href="#" class="btn btn-danger">FOR SALE</a></li>
										</ul>
									</div>
								</div>
								<div class="best-offer-body">
									<ul class="best-offer-room d-flex align-items-center">
										<li>Rooms: 3</li>
										<li>Bed: 1</li>
										<li>Bath: 1</li>
									</ul>
									<div class="row best-offer-price">
										<div class="col-8">
											<h6>$ <?= number_format($value->property->price, 0); ?></h6>
											<!-- <p>$ 690,000</p> -->
										</div>
										<div class="col-4 d-flex align-items-center justify-content-between">
											<input type="checkbox">
											<img src="<?= Url::base(); ?>/assets/img/icons/love.png" alt="love" class="mw-100 ml-2">
										</div>
									</div>
									<ul class="best-offer-agents d-flex align-items-center">
										<li><img src="<?= Url::base(); ?>/assets/img/26.png" alt="agents" class="mw-100 mr-2"></li>
										<li>Mark Johnson</li>
										<li>
											<i class="fa fa-calendar pl-4 pr-2"></i>
											10 months ago
										</li>
									</ul>
								</div>
							</div>
						</a>
						<?php } else { ?>
						<div class="best-offer-box">
								<div class="best-offer-head">
									<div class="best-offer-head-info">
										<h5><?= strtoupper($value->property->type); ?></h5>
										<p>
											<?= $value->property->title; ?>
										</p>
										<span><i class="fa fa-map-marker text-warning"></i> <?= $value->property->location; ?></span>
									</div>
									<div class="best-offer-img position-relative">
										<img src="<?= Url::base(); ?>/assets/img/properties/6.png" alt="properties" class="mw-100">
										<ul class="best-offer-img-action d-flex align-items-center position-absolute">
											<li><a href="#" class="btn btn-warning">REUSED</a></li>
											<li><a href="#" class="btn btn-info">FOR RENT</a></li>
											<li><a href="#" class="btn btn-danger">FOR SALE</a></li>
										</ul>
									</div>
								</div>
								<div class="best-offer-body">
									<ul class="best-offer-room d-flex align-items-center">
										<li>Rooms: 3</li>
										<li>Bed: 1</li>
										<li>Bath: 1</li>
									</ul>
									<div class="row best-offer-price">
										<div class="col-8">
											<h6>$ <?= number_format($value->property->price, 0); ?></h6>
											<!-- <p>S$ 690,000</p> -->
										</div>
										<div class="col-4 d-flex align-items-center justify-content-between">
											<input type="checkbox">
											<img src="<?= Url::base(); ?>/assets/img/icons/love.png" alt="love" class="mw-100 ml-2">
										</div>
									</div>
									<ul class="best-offer-agents d-flex align-items-center">
										<li><img src="<?= Url::base(); ?>/assets/img/26.png" alt="agents" class="mw-100 mr-2"></li>
										<li>Mark Johnson</li>
										<li>
											<i class="fa fa-calendar pl-4 pr-2"></i>
											10 months ago
										</li>
									</ul>
								</div>
							</div>
						<?php } ?>
							<!-- best-offer-box -->
						</div>
					<?php } } ?>
					</div>
				</div>
				<!-- best offer -->

			</div>

			<div class="col-lg-3 offset-lg-1">
				<div class="contact-form">
					<h4>Contact me:</h4>
					<?php $form = ActiveForm::begin([
						// 'action' => ['site/contact'],
				        'id' => 'contact-form',
				        'enableClientValidation' => true,
				        'options' => [
				            'class' => 'contact-form-box',
				        ],
					]); ?>

                    <?= $form->field($modelContact, 'name')->textInput(['autofocus' => true, 'class' => 'form-control mb-3', 'placeholder' => 'Your name'])->label(false) ?>

                    <?= $form->field($modelContact, 'email')->textInput(['autofocus' => true, 'class' => 'form-control mb-3', 'placeholder' => 'E-mail'])->label(false) ?>

                    <?= $form->field($modelContact, 'phone')->textInput(['autofocus' => true, 'class' => 'form-control mb-3', 'placeholder' => 'Phone'])->label(false) ?>

                    <?= $form->field($modelContact, 'message')->textarea(['rows' => 6, 'placeholder' => 'Message'])->label(false) ?>

                    <div class="form-group">
                        <?= Html::submitButton('SEND MESSAGE', ['class' => 'btn btn-info w-100 d-flex align-items-center justify-content-center', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- agents profile section -->