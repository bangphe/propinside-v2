<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use app\models\Property;

$this->title = 'Agent Details';
?>

<style type="text/css">
::-webkit-input-placeholder {
    font-weight: bold;
}
:-moz-placeholder {
    font-weight: bold;
}
:-ms-input-placeholder {
    font-weight: bold;
}
</style>

<!-- searching box -->
<!-- <section class="searching-box">
	<div class="container">
		<form action="#" class="searching-form d-flex align-items-center justify-content-center">
			<div class="row w-100 align-items-center">
				<div class="col-sm-5">
					<input type="text" class="w-100" placeholder="Search by Location">
				</div>
				<div class="col-sm-5">
					<input type="text" class="w-100" placeholder="Search by keyword">
				</div>
				<div class="col-sm-2 text-center text-sm-right">
					<button type="submit"><i class="fa fa-search"></i></button>
				</div>
			</div>
		</form>
	</div>
</section> -->
<!-- searching box -->
<section class="property-main">
    <div class="container bg-white">
        <div class="row">
            <div class="col-lg-12">
                <h3><?= $this->title; ?></h3>
            </div>
        </div>
    </div>
</section>

<!-- agent-details-box -->
<section class="agent-details-box">
	<div class="container">
		<div class="row">
			<?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
            <div class="col-lg-12">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                  <strong>Congratulations!</strong> Thank you for contacting us. We will respond to you as soon as possible.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
            </div>
	        <?php endif; ?>
			<!-- <div class="col-lg-12 d-flex">
				<div class="agent-details-title-box text-uppercase px-4 bg-primary text-white d-flex align-items-center justify-content-center">Agents Detail</div>
			</div> -->
		</div>
	</div>
	<div class="container">

		<!-- agent-details-items -->
		<div class="agent-details-items bg-white">
			<div class="row">
				<div class="col-sm-4">
					<div class="agent-details-img text-center mb-sm-0 mb-4">
						<?php if($model->photo != NULL) { ?>
		                    <img src="<?= Yii::getAlias('@web').$model->photo; ?>" alt="agents" class="mw-100" width="210px" height="250px">
		                <?php } else { ?>
		                    <img src="<?= Yii::getAlias('@web/assets/propinside/img/team/t2.png'); ?>" alt="team" class="w-100">
		                <?php } ?>
						<div class="d-flex justify-content-center mt-4">
							<ul class="social-link d-flex align-items-center justify-content-center">
								<li>
                                    <?php if ($model->facebook != NULL) { ?>
                                    <a href="<?= $model->facebook; ?>" class="d-flex align-items-center justify-content-center" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                    <?php } else { ?>
                                    <a href="javascript:;" class="d-flex align-items-center justify-content-center" style="color: gray;">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                    <?php } ?>
                                </li>
                                <li>
                                    <?php if ($model->twitter != NULL) { ?>
                                    <a href="<?= $model->twitter; ?>" class="d-flex align-items-center justify-content-center" target="_blank">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <?php } else { ?>
                                    <a href="javascript:;" class="d-flex align-items-center justify-content-center" style="color: gray;">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <?php } ?>
                                </li>
                                <li>
                                    <?php if ($model->twitter != NULL) { ?>
                                    <a href="<?= $model->linkedin; ?>" class="d-flex align-items-center justify-content-center" target="_blank">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                    <?php } else { ?>
                                    <a href="javascript:;" class="d-flex align-items-center justify-content-center" style="color: gray;">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                    <?php } ?>
                                </li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-8">
					<div class="agent-details-info">
						<h4><?= $model->name; ?></h4>
						<p><span class="text-info">Title :</span> <?= $model->title; ?></p>
						<p><span class="text-info">Agent Speciality :</span> <?= $model->speciality; ?></p>
						<p><span class="text-info">Country :</span> <?= $model->country_operability; ?></p>
						<p><span class="text-info">Email :</span> <?= $model->email; ?></p>
						<p><span class="text-info">Mobile :</span> <?= $model->phone_number; ?></p>
						<!-- <ul class="d-sm-flex align-items-center justify-content-between mb-5">
							<li><span class="text-info">Email :</span> <?= $model->email; ?></li>
							<li><span class="text-info">Mobile :</span> <?= $model->phone_number; ?></li>
							<li><span class="text-info">Fax :</span> <?= $model->phone_number; ?></li>
						</ul> -->
						<!-- <div class="d-flex justify-content-start justify-content-sm-end"> -->
							<!-- <a href="#" class="btn btn-info MyriadPro d-inline-flex align-items-center justify-content-center px-4 mt-lg-5"> -->
								<!-- Learn More -->
							<!-- </a> -->
						<!-- </div> -->
					</div>
				</div>
			</div>
		</div>
		<!-- agent-details-items -->

	</div>
</section>
<!-- agent-details-box -->

<!-- site section main -->
<section class="site-section-main">
	<div class="container">
		<div class="row">
			<div class="col-lg-7">
				<div class="site-header text-uppercase">
					<!-- <p><?= $model->name; ?></p> -->
					<h4>Introduction</h4>
				</div>
				<div class="site-info">
					<p>
						<?= $model->description != NULL ? $model->description : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium'; ?>
					</p>
				</div>
			</div>
			<div class="col-lg-5">
				<?php $form = ActiveForm::begin([
						// 'action' => ['site/contact'],
				        'id' => 'contact-form',
				        'enableClientValidation' => false,
				        'options' => [
				            'class' => 'contact-form px-sm-5 px-3 py-4 bg-white',
				        ],
					]); ?>

                    <?= $form->field($modelContact, 'name')->textInput(['class' => 'form-controls', 'placeholder' => 'Your name'])->label(false) ?>

                    <?= $form->field($modelContact, 'email')->textInput(['class' => 'form-controls', 'placeholder' => 'E-mail'])->label(false) ?>

                    <?= $form->field($modelContact, 'phone')->textInput(['class' => 'form-controls', 'placeholder' => 'Phone'])->label(false) ?>

                    <?= $form->field($modelContact, 'message')->textarea(['rows' => 2, 'placeholder' => 'Message', 'class' => 'form-controls', 'style'=>'height:5%'])->label(false) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Send message', ['class' => 'btn btn-info MyriadPro d-inline-flex align-items-center justify-content-center px-4', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</section>
<!-- site section main -->

<!-- site section main -->
<section class="site-section-main">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="site-header text-uppercase">
					<!-- <p>Lorem ipsum</p> -->
					<h4>My Top 5 Properties</h4>
				</div>
			</div>
		</div>

		<?php if(empty($model->propertyListings)) { ?>
			<p style="margin:16px;"><?= "No results found." ?></p>
		<?php } else { ?>
		<!-- owl-product-promo item -->
		<div id="owl-product-promo" class="owl-carousel owl-theme owl-loaded owl-drag">
		<?php foreach ($model->propertyListings as $value) { ?>
			<!-- owl-product-promo item -->
			<?php if($value->property->status != Property::STATUS_DELETED) { ?>
				<?php if($value->property->is_published == Property::STATUS_APPROVED) { ?>
				<a href="<?= Url::to(['/agents/property/'.$value->property->slug]); ?>">
					<div class="product-promo my-5">
						<div class="prod-imgs position-relative mb-3">
							<?php if (!empty($value->property->propertyImages)) { $path = Yii::getAlias('@web').$value->property->propertyImages[0]->image_path; ?>
								<img src="<?= $path; ?>" alt="products" class="mw-100 w-100" style="height: 122.98px !important;">
							<?php } else { ?>
								<img src="<?= Yii::getAlias('@web/assets/propinside/img/products/p3.png'); ?>" alt="products" class="mw-100 w-100">
							<?php } ?>
							<div class="position-absolute d-flex align-items-center">
								For Sale
							</div>
							<h4><?= $value->property->title; ?></h4>
							<p><?= $value->property->location; ?></p>
							<p style="font-weight: 700;">$<?= number_format($value->property->price, 0); ?></p>
						</div>
						<!-- <div class="row m-0 prod-price">
							<div class="col-6 pr-sm-0 d-flex align-items-center">
								<div class="star-rating">
									<span style="width:80%"></span>
								</div>
							</div>
							<div class="col-12 pr-0 prod-price-tag text-right">
								$<?//= number_format($value->property->price, 0); ?>
							</div>
						</div> -->
					</div>
				</a>
				<?php } else { ?>
				<div class="product-promo my-5">
					<div class="prod-imgs position-relative mb-3">
						<?php if (!empty($value->property->propertyImages)) { $path = Yii::getAlias('@web').$value->property->propertyImages[0]->image_path; ?>
							<img src="<?= $path; ?>" alt="products" class="mw-100 w-100" style="height: 122.98px !important;">
						<?php } else { ?>
							<img src="<?= Yii::getAlias('@web/assets/propinside/img/products/p3.png'); ?>" alt="products" class="mw-100 w-100">
						<?php } ?>
						<div class="position-absolute d-flex align-items-center">
							For Sale
						</div>
						<h4><?= $value->property->title; ?></h4>
						<p><?= $value->property->location; ?></p>
						<p style="font-weight: 700;">$<?= number_format($value->property->price, 0); ?></p>
					</div>
					<!-- <div class="row m-0 prod-price">
						<div class="col-6 pr-sm-0 d-flex align-items-center">
							<div class="star-rating">
								<span style="width:80%"></span>
							</div>
						</div>
						<div class="col-6 pr-0 prod-price-tag text-right">
							$ <?//= number_format($value->property->price, 0); ?>
						</div>
					</div> -->
				</div>
				<?php } ?>
				<!-- owl-product-promo item -->
			<?php } } ?>
		<?php } ?>
		</div>
	</div>
</section>
<!-- site section main -->