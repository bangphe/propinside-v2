<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\PropertyFacilities;
use app\models\PropertyAmenities;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Agents Submit Property';
$theme = Url::base().'/assets/castle';
?>

<style type="text/css">
.file-caption-main {
  width: 93%;
}
.fileinput-cancel-button {
  display: none;
}
</style>

<!-- agents submit property page -->
<section id="property" class="padding_half listing1">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <ul class="f-p-links bottom40">
          <li><a href="<?= Url::to(['agents/profile']); ?>"><i class="icon-icons230"></i>Profile</a></li>
          <li><a href="<?= Url::to(['agents/my-properties']); ?>"><i class="icon-icons215"></i> My Properties</a></li>
          <li><a href="#" class="active"><i class="icon-icons202"></i> Submit Property</a></li>
          <!-- <li><a href="favorite_properties.html"><i class="icon-icons43"></i> Favorite Properties</a></li> -->
          <!-- <li><a href="login.html"><i class="icon-lock-open3"></i>Logout</a></li> -->
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-1 col-md-2"></div>
      <div class="col-sm-10 col-md-8">
      <h2 class="text-uppercase text-center bottom30">Add Your Property</h2>
      <?= Yii::$app->session->getFlash('info');?>
      <?php $form = ActiveForm::begin([
        'id' => 'property-form',
        'enableClientValidation' => true,
        'options' => [
              'class' => 'callus clearfix border_radius submit_property',
              'enctype'=>'multipart/form-data',
          ],
      ]); ?>
      <div class="row">
        <div class="col-sm-6"> 
          <div class="single-query form-group bottom20">
            <label>Title</label>
            <?= $form->field($property, 'title')->textInput(['class'=>'keyword-input', 'placeholder'=>'Enter your property title'])->label(false) ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="single-query form-group bottom20">
            <label>Location</label>
            <?//= $form->field($property, 'location')->textInput(['class'=>'keyword-input', 'placeholder'=>'Enter your property location'])->label(false) ?>
            <?= $form->field($property, 'location')->widget(Select2::classname(), [
                'data' => $dataDistrict,
                'options' => ['placeholder' => 'Select Property Location'],
                'size' => Select2::MEDIUM,
                'pluginOptions' => [
                    'allowClear' => false
                ],
            ])->label(false); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="single-query bottom20">
            <label>Type</label>
              <?//= $form->field($property, 'type')->textInput(['class'=>'keyword-input', 'placeholder'=>'Enter your property type'])->label(false) ?>
              <?= $form->field($property, 'type')->widget(Select2::classname(), [
                'data' => $dataType,
                'options' => ['placeholder' => 'Select Property Type'],
                'size' => Select2::MEDIUM,
                'pluginOptions' => [
                    'allowClear' => false
                ],
            ])->label(false); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="single-query form-group bottom20">
            <label>Price</label>
            <?= $form->field($property, 'price')->textInput(['class'=>'keyword-input', 'placeholder'=>'23,000', 'type'=>'number'])->label(false) ?>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <h3 class="margin40 bottom15">Property Photos <i class="fa fa-info-circle help" data-toggle="tooltip" title="add images to upload for property!"></i></h3>
          <div class="file_uploader bottom20">
            <?php
              $imagesPreview = [];
              $imagesPreviewConfig = [];
              if (isset($uploadedImages)) {
                  if ($uploadedImages) foreach ($uploadedImages as $key => $image) {
                      $imagesPreview[] = $image->image_path;
                      $imagesPreviewConfig[$key]['caption'] = $image->image_path;
                      $imagesPreviewConfig[$key]['url'] = Url::to(['/agents/remove-prop-image']);
                      $imagesPreviewConfig[$key]['key'] = $image->id_image;
                  }
              }
              echo $form->field($property_images, 'imagesGallery[]')->widget(\kartik\file\FileInput::classname(), [
                'options' => [
                    'multiple' => true,
                    'class' =>'file-loading',
                    'accept' => 'image/*',
                ],
                'pluginOptions' => [
                    'initialPreview'=> $imagesPreview,
                    'initialPreviewConfig' => $imagesPreviewConfig,
                    'initialPreviewAsData'=>true,
                    'deleteUrl' => false,
                    'uploadUrl' => Url::to(['upload']),
                    'uploadAsync' => false,
                    'allowedFileTypes' => ["image"],
                    'showCaption' => false,
                    'showRemove' => true,
                    'showUpload' => false,
                    'showCancel' => false,
                    'browseOnZoneClick' => true,
                    'autoOrientImage' => true,
                    'browseClass' => 'btn btn-primary btn-block',
                    'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                    'browseLabel' =>  'Select Photo',
                    'removeClass' => 'btn btn-default btn-danger btn-block',
                    'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> ',
                    'removeLabel' =>  'Remove',
                    'fileActionSettings' => [
                        'showUpload' => false,
                        'showDrag' => true,
                        'autoOrientImage' => true,
                    ],
                    'overwriteInitial'=>false,
                    'maxFileCount' => 5,
                ],
                'pluginEvents' => [
                    'filebatchselected' => new \yii\web\JsExpression(
                    'function(event, files) {
                        jQuery(event.currentTarget).fileinput("upload");
                    }')
                ]
            ])->label(false);
            ?>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <h3 class="bottom15 margin40">Property Detail</h3>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-4">
          <div class="single-query form-group bottom20">
            <label>Size Prefix</label>
              <?= $form->field($property, 'floor_size')->textInput(['class'=>'keyword-input', 'placeholder'=>'Enter your property floor size'])->label(false) ?>
            </div>
        </div>
      
        <div class="col-sm-4">
          <div class="single-query form-group bottom20">
            <label>Developer</label>
            <?= $form->field($property, 'developer')->textInput(['class'=>'keyword-input', 'placeholder'=>'Enter your property developer'])->label(false) ?>
          </div>
        </div>

        <div class="col-sm-4">
            <div class="single-query  form-group bottom20">
            <label>Land Size</label>
            <?= $form->field($property, 'land_size')->textInput(['class'=>'keyword-input', 'placeholder'=>'Enter your property land size'])->label(false) ?>
          </div>
        </div>
            
        <div class="col-sm-4">
          <div class="single-query form-group bottom20">
            <label>Psf</label>
            <?= $form->field($property, 'psf')->textInput(['class'=>'keyword-input', 'placeholder'=>'Enter your property psf'])->label(false) ?>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="single-query form-group bottom20">
            <label>Furnishing</label>
            <?= $form->field($property, 'furnishing')->textInput(['class'=>'keyword-input', 'placeholder'=>'Enter your property furnishing'])->label(false) ?>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="single-query form-group bottom20">
            <label>Top</label>
            <?= $form->field($property, 'top')->textInput(['class'=>'keyword-input', 'placeholder'=>'Enter your property top'])->label(false) ?>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="single-query form-group bottom20">
            <label>Floor Level</label>
            <?= $form->field($property, 'floor_level')->textInput(['class'=>'keyword-input', 'placeholder'=>'Enter your property floor level'])->label(false) ?>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="single-query form-group bottom20">
            <label>Tenure</label>
            <?= $form->field($property, 'tenure')->textInput(['class'=>'keyword-input', 'placeholder'=>'Enter your property tenure'])->label(false) ?>
          </div>
        </div>

        <div class="col-sm-12">
          <h3 class="bottom15 margin40">Property Description </h3>
          <?= $form->field($property, 'description')->textarea(['class'=>'form-control', 'placeholder'=>'Write here something about property'])->label(false) ?>
        </div>

        <div class="col-sm-12">
          <h3 class="bottom15 margin40">Property Facilities</h3>
          <div class="search-propertie-filters">
            <div class="container-2">
                <?php
                if(!$property->isNewRecord) {
                    $checkedList = [];
                    foreach ($property->propertyHasFacilities as $v) {
                        $checkedList[] = $v->id_facilities;
                    }
                    $property->prop_facilities = $checkedList;
                }

                echo $form->field($property, 'prop_facilities')->checkboxList(
                      PropertyFacilities::listFacilities(),
                      [
                        'class' => 'row',
                        'item' => function($index, $label, $name, $checked, $value) {
                            $checkbox = '<div class="col-md-4 col-sm-4">';
                            $checkbox .= '<div class="search-form-group white">';
                            if ($checked) {
                                $checkbox .= '<div class="check-box checkedBox">';
                                $checkbox .= '<i>';
                                $checkbox .= '<input type="checkbox" name="' . $name . '" value="' . $value . '" checked="checked" tabindex="3">';
                                $checkbox .= '</i>';
                                $checkbox .= '</div>';
                            } else {
                                $checkbox .= '<div class="check-box">';
                                $checkbox .= '<i>';
                                $checkbox .= '<input type="checkbox" name="' . $name . '" value="' . $value . '" tabindex="3">';
                                $checkbox .= '</i>';
                                $checkbox .= '</div>';
                            }
                            $checkbox .= '<span>'.$label.'</span>';
                            $checkbox .= '</div>';
                            $checkbox .= '</div>';

                            return $checkbox;
                        }
                      ]
                   )->label(false);
                ?>
            </div>
          </div>
        </div>

        <div class="col-sm-12">
          <h3 class="bottom15 margin40">Property Amenities</h3>
          <div class="search-propertie-filters">
            <div class="container-2">
              <?php
              if(!$property->isNewRecord) {
                  $checkedList = [];
                  foreach ($property->propertyHasAmenities as $v) {
                      $checkedList[] = $v->id_amenities;
                  }
                  $property->prop_amenities = $checkedList;
              }

              echo $form->field($property, 'prop_amenities')->checkboxList(
                  PropertyAmenities::listAmenities(),
                  [
                    'class' => 'row',
                    'item' => function($index, $label, $name, $checked, $value) {
                        $checkbox = '<div class="col-md-4 col-sm-4">';
                        $checkbox .= '<div class="search-form-group white">';
                        if ($checked) {
                            $checkbox .= '<div class="check-box checkedBox">';
                            $checkbox .= '<i>';
                            $checkbox .= '<input type="checkbox" name="' . $name . '" value="' . $value . '" checked="checked" tabindex="3">';
                            $checkbox .= '</i>';
                            $checkbox .= '</div>';
                        } else {
                            $checkbox .= '<div class="check-box">';
                            $checkbox .= '<i>';
                            $checkbox .= '<input type="checkbox" name="' . $name . '" value="' . $value . '" tabindex="3">';
                            $checkbox .= '</i>';
                            $checkbox .= '</div>';
                        }
                        $checkbox .= '<span>'.$label.'</span>';
                        $checkbox .= '</div>';
                        $checkbox .= '</div>';

                        return $checkbox;
                    }
                  ]
               )->label(false);
            ?>
          </div>
        </div>
      
        <div class="col-md-12">
          <?= Html::submitButton('submit property', ['class' => 'btn-blue border_radius margin40']) ?>
        </div>
      </div>
          <?php ActiveForm::end(); ?>
      </div>
    </div>
  </div>
</section>
<!-- agents submit property page -->