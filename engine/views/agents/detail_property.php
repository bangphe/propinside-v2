<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Property Details';
?>

<section class="property-main">
    <div class="container bg-white">
        <div class="row">
            <div class="col-lg-12">
                <h3><?= $this->title.' - '.ucwords($model->title); ?></h3>
            </div>
        </div>
    </div>
</section>

<!-- property-main -->
<section class="property-main property-list">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">

				<!-- property item -->
				<div class="property-item">
		            <ul class="imageGallery">
			        <?php
			        	if ($model->propertyImages) {
			        		foreach ($model->propertyImages as $v) {
			        ?>
	                <li class="img-fluid" data-src="<?= Yii::getAlias('@web').$v->image_path; ?>">
	                    <img class="img-fluid mx-auto d-block" src="<?= Yii::getAlias('@web').$v->image_path; ?>" />
	                </li>
	            	<?php } } else { ?>
		            	<?php for ($i=2; $i < 5; $i++) { ?>
		            		<li class="img-fluid" data-src="<?= Url::base().'/assets/img/properties/'.$i.'.png'; ?>">
			                    <img class="img-fluid mx-auto d-block" src="<?= Url::base().'/assets/img/properties/'.$i.'.png'; ?>" />
			                </li>
		            	<?php } } ?>
		            </ul>
					<p class="text-justify" style="margin-top: 20px;">
						<?= $model->description; ?>
					</p>
					<ul class="property-type pb-4">
						<li><b>Location : </b><?= $model->location; ?></li>
						<li><b>Price : $<?= number_format($model->price, 0); ?></b></li>
					</ul>

				</div>
				<!-- property item -->

			</div>

			<!-- site-header -->
			<div class="col-lg-12">
				<div class="site-header text-uppercase">
					<!-- <p>Lorem ipsum</p> -->
					<h4>Detail</h4>
				</div>
			</div>
			<!-- site-header -->

			<!-- property-detail-info -->
			<div class="col-sm-6">
				<div class="property-detail-info">
					<label>Type</label>
					<input type="text" class="form-control" disabled="" value="<?= $model->type; ?>">
				</div>
			</div>
			<!-- property-detail-info -->

			<!-- property-detail-info -->
			<div class="col-sm-6">
				<div class="property-detail-info">
					<label>Tenure</label>
					<input type="text" class="form-control" disabled="" value="<?= $model->tenure; ?>">
				</div>
			</div>
			<!-- property-detail-info -->

			<!-- property-detail-info -->
			<div class="col-sm-6">
				<div class="property-detail-info">
					<label>Floor Size</label>
					<input type="text" class="form-control" disabled="" value="<?= $model->floor_size; ?>">
				</div>
			</div>
			<!-- property-detail-info -->

			<!-- property-detail-info -->
			<div class="col-sm-6">
				<div class="property-detail-info">
					<label>Developer</label>
					<input type="text" class="form-control" disabled="" value="<?= $model->developer; ?>">
				</div>
			</div>
			<!-- property-detail-info -->

			<!-- property-detail-info -->
			<div class="col-sm-6">
				<div class="property-detail-info">
					<label>Land Size</label>
					<input type="text" class="form-control" disabled="" value="<?= $model->land_size; ?>">
				</div>
			</div>
			<!-- property-detail-info -->

			<!-- property-detail-info -->
			<div class="col-sm-6">
				<div class="property-detail-info">
					<label>PSF</label>
					<input type="text" class="form-control" disabled="" value="<?= $model->psf; ?>">
				</div>
			</div>
			<!-- property-detail-info -->

			<!-- property-detail-info -->
			<div class="col-sm-6">
				<div class="property-detail-info">
					<label>Furnishing </label>
					<input type="text" class="form-control" disabled="" value="<?= $model->furnishing; ?>">
				</div>
			</div>
			<!-- property-detail-info -->

			<!-- property-detail-info -->
			<div class="col-sm-6">
				<div class="property-detail-info">
					<label>Top </label>
					<input type="text" class="form-control" disabled="" value="<?= $model->top; ?>">
				</div>
			</div>
			<!-- property-detail-info -->

			<!-- site-header -->
			<div class="col-lg-12 mt-4">
				<div class="site-header text-uppercase">
					<!-- <p>Lorem ipsum</p> -->
					<h4>Property Facilities</h4>
				</div>
			</div>
			<!-- site-header -->

			<!-- property facility -->
			<?php foreach ($model->propertyHasFacilities as $v) { ?>
			<div class="col-lg-4 col-sm-6">
				<div class="property-facility d-sm-flex align-items-center">
					<div class="property-facility-icon mr-4"><i class="fa fa-check" style="margin: 12px;"></i></div>
					<h4 class="pl-sm-3" style="font-size: 14px !important;"><?= $v->facilities->facilities; ?></h4>
				</div>
			</div>
			<?php } ?>
			<!-- property facility -->

			<!-- site-header -->
			<div class="col-lg-12 mt-5">
				<div class="site-header text-uppercase">
					<!-- <p>Lorem ipsum</p> -->
					<h4>Property Amenities</h4>
				</div>
			</div>
			<!-- site-header -->

			<!-- property amenities -->
			<?php foreach ($model->propertyHasAmenities as $v) { ?>
			<div class="col-lg-4 col-sm-6">
				<div class="property-facility d-sm-flex align-items-center">
					<div class="property-facility-icon mr-4"><i class="fa fa-check" style="margin: 12px;"></i></div>
					<h4 class="pl-sm-3" style="font-size: 14px !important;"><?= $v->amenities->amenities; ?></h4>
				</div>
			</div>
			<?php } ?>
			<!-- property amenities -->

			<!-- site-header -->
			<div class="col-lg-12 mt-5">
				<div class="site-header text-uppercase">
					<!-- <p>Lorem ipsum</p> -->
					<h4>Our Agents</h4>
				</div>
			</div>
			<!-- site-header -->

			<?php if (!empty($model->propertyListings)) {
				foreach ($model->propertyListings as $val) {
			?>
			<div class="col-12 col-sm-4 col-lg text-center">
				<!-- team item -->
				<div class="team-item">
					<div class="team-img position-relative">
						<a href="<?= Url::to(['/agents/detail?id='.$val->id_agent]); ?>">
						<?php if($val->agent->photo != NULL) { ?>
                            <img src="<?= Yii::getAlias('@web').$val->agent->photo; ?>" alt="properties" class="mw-100 img-fluid" style="height: 178px;" >
                        <?php } else { ?>
                            <img src="<?= Url::base(); ?>/assets/propinside/img/team/t1.jpg" alt="testimonial" class="mw-100">
                        <?php } ?>
                        </a>
						<div class="team-img-overlay position-absolute text-white">
							<ul class="social-link position-absolute d-flex align-items-center justify-content-center">
								<li>
                                    <?php if ($val->agent->facebook != NULL) { ?>
                                    <a href="<?= $val->agent->facebook; ?>" class="d-flex align-items-center justify-content-center" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                    <?php } else { ?>
                                    <a href="javascript:;" class="d-flex align-items-center justify-content-center" style="color: gray;">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                    <?php } ?>
                                </li>
                                <li>
                                    <?php if ($val->agent->twitter != NULL) { ?>
                                    <a href="<?= $val->agent->twitter; ?>" class="d-flex align-items-center justify-content-center" target="_blank">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <?php } else { ?>
                                    <a href="javascript:;" class="d-flex align-items-center justify-content-center" style="color: gray;">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <?php } ?>
                                </li>
                                <li>
                                    <?php if ($val->agent->twitter != NULL) { ?>
                                    <a href="<?= $val->agent->linkedin; ?>" class="d-flex align-items-center justify-content-center" target="_blank">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                    <?php } else { ?>
                                    <a href="javascript:;" class="d-flex align-items-center justify-content-center" style="color: gray;">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                    <?php } ?>
                                </li>
							</ul>
							<h6>
                                <a href="<?= Url::to(['/agents/detail?id='.$val->agent->id_agent]); ?>" style="color: #000000;"><?= ucwords($val->agent->name); ?></a>
                            </h6>
                            <p>
                                <a href="<?= Url::to(['/agents/detail?id='.$val->agent->id_agent]); ?>" style="color: #676767;"><?= ucwords($val->agent->title); ?></a>
                            </p>
						</div>
					</div>
					<a href="#" class="btn btn-info MyriadPro d-inline-flex align-items-center justify-content-center" data-toggle="modal" data-target="#modal-contact">
						Contact Me
					</a>
				</div>
				<!-- team item -->
			</div>
			<?php } ?>
			<?php if (count($model->propertyListings) == 2) { ?>
            <div class="col-12 col-sm-4 col-lg text-center"></div>
            <div class="col-12 col-sm-4 col-lg text-center"></div>
            <div class="col-12 col-sm-4 col-lg text-center"></div>
            <?php } elseif (count($model->propertyListings) == 3) { ?>
            <div class="col-12 col-sm-4 col-lg text-center"></div>
            <div class="col-12 col-sm-4 col-lg text-center"></div>
            <?php } elseif (count($model->propertyListings) == 4) { ?>
            <div class="col-12 col-sm-4 col-lg text-center"></div>
            <?php } else { ?>
            <div class="col-12 col-sm-4 col-lg text-center"></div>
            <div class="col-12 col-sm-4 col-lg text-center"></div>
            <div class="col-12 col-sm-4 col-lg text-center"></div>
            <div class="col-12 col-sm-4 col-lg text-center"></div>
            <?php }
        		} else {
           	?>
           	<div class="col-12 col-sm-4 col-lg">
           		<p>No results found</p>
           	</div>
           	<?php } ?>
		</div>
	</div>
</section>
<!-- property-main -->

<!-- Modal -->
<div class="modal modal-user-box" id="modal-contact" tabindex="-1" role="dialog" aria-labelledby="modal-login-title" aria-hidden="true" data-backdrop="static">
    <?php $form = ActiveForm::begin([
        // 'action' => 'javascript:;',
        'action' => ['site/contact'],
        'id' => 'contact_form',
        'enableClientValidation' => true,
        'options' => [
            'class' => 'form-horizontal modal-form',
            'role' => 'form',
            'method' => 'POST',
            'name' => 'contact_form',
        ],
    ]); ?>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="modal-login-title" style="margin: 0 75px;">Contact Me</h3>
                    <button type="button" id="btn-close-modal-login" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="login-wrapper">
                        <div class="alert alert-danger alert-dismissable hide" id="login-error-message">
                            There seems to be an issue on our end. Please try again later
                        </div>
                        <!-- <input type="hidden" id="_csrf" name="<?//= Yii::$app->request->csrfParam; ?>" value="<?//= Yii::$app->request->csrfToken; ?>" /> -->
                        <?php $fieldOptionsName = [
                            'options' => ['class' => 'form-group'],
                            'template' => '<i class="pgicon pgicon-user-o"></i>{input}<span class="error-msg">{error}</span>',
                        ]; ?>
                        <?= $form->field($modelContact, 'name', $fieldOptionsName)->textInput(['id'=>'login-name', 'class'=>'form-controls', 'placeholder'=>'Full Name', 'autocomplete'=>'off'])->label(false) ?>

                        <?php $fieldOptionsEmail = [
                            'options' => ['class' => 'form-group'],
                            'template' => '<i class="pgicon pgicon-key"></i>{input}<span class="error-msg">{error}</span>',
                        ]; ?>
                        <?= $form->field($modelContact, 'email', $fieldOptionsEmail)->textInput(['id'=>'login-email', 'class'=>'form-controls', 'placeholder'=>'Email Address', 'autocomplete'=>'off'])->label(false) ?>

                        <?php $fieldOptionsPhone = [
                            'options' => ['class' => 'form-group'],
                            'template' => '<i class="pgicon pgicon-phone"></i>{input}<span class="error-msg">{error}</span>',
                        ]; ?>
                        <?= $form->field($modelContact, 'phone', $fieldOptionsPhone)->textInput(['id'=>'login-phone', 'class'=>'form-controls', 'placeholder'=>'Phone Number', 'autocomplete'=>'off'])->label(false) ?>

                        <?php $fieldOptionsMsg = [
                            'options' => ['class' => 'form-group'],
                            'template' => '<i class="pgicon pgicon-tags"></i>{input}<span class="error-msg">{error}</span>',
                        ]; ?>
                        <?= $form->field($modelContact, 'message', $fieldOptionsMsg)->textarea(['id'=>'login-message', 'class'=>'form-controls', 'placeholder'=>'Message', 'autocomplete'=>'off', 'rows'=>2])->label(false) ?>

                        <div class="actions">
                            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn-md', 'style' => 'width:100%;']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
    <!-- </form> -->
</div>
<!-- End Modal -->