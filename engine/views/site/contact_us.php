<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Contact us';
?>

<style type="text/css">
div.g-recaptcha {
 	margin: 0 auto;
 	width: 304px;
}
.field-contactform-recaptcha div.help-block {
	text-align: center !important;
}
</style>

<section class="property-main">
    <div class="container bg-white">
        <div class="row">
            <div class="col-lg-12">
                <h3><?= $this->title; ?></h3>
            </div>
        </div>
    </div>
</section>

<!-- site section main -->
<section class="site-section-main contact-us">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<!-- <div class="site-header text-uppercase">
					<h4>Contact us</h4>
				</div> -->
				<?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                  <strong>Congratulations!</strong> Thank you for contacting us. We will respond to you as soon as possible.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
		        <?php endif; ?>
				<?php $form = ActiveForm::begin([
					'id' => 'contact-form',
					'enableAjaxValidation'=> false,
					'options'=>[
						'class'=>'contact-form',
					],
				]); ?>
				<!-- <div class="row">
					<div class="col-sm-12">
						<?//= $form->errorSummary($model); ?>
					</div>
				</div><br> -->
				<div class="row">
                    <?php
			            $field = $form->field($model, 'name', ['options' => ['class' => 'col-sm-6 mb-sm-5 mb-3']]);
			            echo $field->textInput(['maxlength' => true, 'placeholder' => 'Full Name'])->label(false);
			        ?>

			        <?php
			            $field = $form->field($model, 'email', ['options' => ['class' => 'col-sm-6 mb-sm-5 mb-3']]);
			            echo $field->textInput(['maxlength' => true, 'placeholder' => 'Email'])->label(false);
			        ?>

			        <?php
			            $field = $form->field($model, 'subject', ['options' => ['class' => 'col-sm-6 mb-sm-5 mb-3']]);
			            echo $field->textInput(['maxlength' => true, 'placeholder' => 'Subject'])->label(false);
			        ?>

			        <?php
			            $field = $form->field($model, 'phone', ['options' => ['class' => 'col-sm-6 mb-sm-5 mb-3']]);
			            echo $field->textInput(['maxlength' => true, 'placeholder' => 'Phone', 'rows' => 6])->label(false);
			        ?>

			        <?php
			            $field = $form->field($model, 'message', ['options' => ['class' => 'col-sm-12 mb-sm-5 mb-3']]);
			            echo $field->textarea(['placeholder' => 'Content', 'rows' => 6])->label(false);
			        ?>

                    <?= $form->field($model, 'reCaptcha', ['options' => ['class' => 'col-sm-12 mb-sm-5 mb-3 justify-content-center'],])->widget(
					    \himiklab\yii2\recaptcha\ReCaptcha2::className(),
					    [
					    	// 'options' => ['class' => 'col-sm-offset-3'],
					    ]
					)->label(false) ?>

                    <div class="col-sm-12 d-flex justify-content-center">
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary MyriadPro d-flex align-items-center justify-content-center', 'name' => 'contact-button']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</section>
<!-- site section main -->