<?php
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'About Us';
?>

<section class="property-main">
    <div class="container bg-white">
        <div class="row">
            <div class="col-lg-12">
                <h3><?= $this->title; ?></h3>
            </div>
        </div>
    </div>
</section>

<section class="agent-details-box pt-4">
    <div class="container">
        <div class="row">
        	<div class="col-lg-12">
                <img class="img-fluid mw-100 w-100" src="<?= Yii::getAlias('@web'); ?>/assets/propinside/img/about-us.jpeg"" style="height: 516px !important;" />
        		<div class="d-flex justify-content-start" style="padding-top: 2.0rem!important; padding-bottom: 2.0rem!important;">
                    <p>
                        It's really about you <br>

                        PropInside provides news about the property market in Singapore by showcasing new and upcoming launches. This is done through videos and curated content to help users make an informed decision when it comes to investing or buying a property in Singapore.<br>

                        We also aim to connect buyers to the property listings via agents and their email, mobile or social profiles such as Facebook and Instagram. The social profiles empowers the agents to connect in a more personalised way and users a chance to understand the agent better in the process too.
                    </p>      
                </div>
        	</div>
		</div>
	</div>
</section>