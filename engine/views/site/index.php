<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$this->title = 'New Launches';
function limit_text($text, $limit) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos = array_keys($words);
        $text = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}
?>

<style type="text/css">
.thumbnail-yt {
    margin-top: -330px;
}
</style>

<!-- property-main -->
<section class="property-main">
    <div class="container bg-white">
        <div class="row">
            <div class="col-lg-12">
                <h3><?= $this->title; ?></h3>
            </div>
        </div>
    </div>
    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
    <div class="row agents-register text-center" style="padding-bottom: 0px;">
        <div class="col-lg-12">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Congratulations!</strong> Thank you for contacting us. We will respond to you as soon as possible.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        </div>
    </div>
    <?php endif; ?>
</section>
<?= \yii\widgets\ListView::widget([
    'id'           => 'property-news',
    'dataProvider' => $dataProvider,
    'itemView'     => '_property_news',
    'layout'       => '
        {items}
        <section class="pagination-main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 d-flex justify-content-end">
                      {pager}
                    </div>
                </div>
            </div>
        </section>
    ',
    // 'itemOptions' => [
    //   'class' => 'container',
    // ],
    'emptyText' => '<p>'.\Yii::t('app', 'No results found').'</p>',
    'emptyTextOptions' => ['tag' => 'div', 'class' => 'container text-center'],
    'pager' => [
        'prevPageLabel' => '<',
        'nextPageLabel' => '>',
        'firstPageLabel' => 'First',
        'lastPageLabel' => 'Last',
        'maxButtonCount' => 3,
        
        // Customzing options for pager container tag
        'options' => [
            'tag' => 'ul',
            'class' => 'pagination-list d-flex align-items-center justify-content-end',
            'id' => 'pager-container',
        ],
    ],
]); ?>
<!-- property-main -->

<!-- Modal -->
<div class="modal modal-user-box" id="modal-contact" tabindex="-1" role="dialog" aria-labelledby="modal-login-title" aria-hidden="true" data-backdrop="static">
    <?php $form = ActiveForm::begin([
        // 'action' => 'javascript:;',
        'action' => ['site/contact'],
        'id' => 'contact_form',
        'enableClientValidation' => true,
        'options' => [
            'class' => 'form-horizontal modal-form',
            'role' => 'form',
            'method' => 'POST',
            'name' => 'contact_form',
        ],
    ]); ?>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="modal-login-title" style="margin: 0 75px;">Contact Me</h3>
                    <button type="button" id="btn-close-modal-login" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="login-wrapper">
                        <div class="alert alert-danger alert-dismissable hide" id="login-error-message">
                            There seems to be an issue on our end. Please try again later
                        </div>
                        <!-- <input type="hidden" id="_csrf" name="<?//= Yii::$app->request->csrfParam; ?>" value="<?//= Yii::$app->request->csrfToken; ?>" /> -->
                        <?php $fieldOptionsName = [
                            'options' => ['class' => 'form-group'],
                            'template' => '<i class="pgicon pgicon-user-o"></i>{input}<span class="error-msg">{error}</span>',
                        ]; ?>
                        <?= $form->field($model, 'name', $fieldOptionsName)->textInput(['id'=>'login-name', 'class'=>'form-controls', 'placeholder'=>'Full Name', 'autocomplete'=>'off'])->label(false) ?>

                        <?php $fieldOptionsEmail = [
                            'options' => ['class' => 'form-group'],
                            'template' => '<i class="pgicon pgicon-key"></i>{input}<span class="error-msg">{error}</span>',
                        ]; ?>
                        <?= $form->field($model, 'email', $fieldOptionsEmail)->textInput(['id'=>'login-email', 'class'=>'form-controls', 'placeholder'=>'Email Address', 'autocomplete'=>'off'])->label(false) ?>

                        <?php $fieldOptionsPhone = [
                            'options' => ['class' => 'form-group'],
                            'template' => '<i class="pgicon pgicon-phone"></i>{input}<span class="error-msg">{error}</span>',
                        ]; ?>
                        <?= $form->field($model, 'phone', $fieldOptionsPhone)->textInput(['id'=>'login-phone', 'class'=>'form-controls', 'placeholder'=>'Phone Number', 'autocomplete'=>'off'])->label(false) ?>

                        <?php $fieldOptionsMsg = [
                            'options' => ['class' => 'form-group'],
                            'template' => '<i class="pgicon pgicon-tags"></i>{input}<span class="error-msg">{error}</span>',
                        ]; ?>
                        <?= $form->field($model, 'message', $fieldOptionsMsg)->textarea(['id'=>'login-message', 'class'=>'form-controls', 'placeholder'=>'Message', 'autocomplete'=>'off', 'rows'=>2])->label(false) ?>

                        <div class="actions">
                            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn-md', 'style' => 'width:100%;']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
    <!-- </form> -->
</div>
<!-- End Modal -->

<!-- <script type="text/javascript">
function sendContact(id) {
    var url = '<?//= Url::base(); ?>/site/sendcontact/'+id;
    $.ajax({
        url:url,
        type:'get',
        data:'',
        beforeSend:function(){
            $("#modal").modal("show");
            $("#modal-body").html('Sedang memuat...');
        },
        success: function(data){
            $("#modal-body").html(data);
        }
    });
}
</script> -->