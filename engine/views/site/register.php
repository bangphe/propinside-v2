<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\AppAsset;

AppAsset::register($this);
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->title = 'Login';
?>

<!-- site section main -->
<section class="site-section-main agent-registration bg-grey">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="site-header text-uppercase">
					<p>Lorem ipsum</p>
					<h4>AGENT REGISTRATION</h4>
				</div>
			</div>
			<div class="col-lg-12">
				<?= Yii::$app->session->getFlash('info');?>
			</div>
			<div class="col-lg-10 offset-lg-1">
				<?php $form = ActiveForm::begin([
					'id' => 'register-form',
					'enableClientValidation' => true,
					'options' => [
						'class' => 'contact-form px-sm-5 px-3 py-4 bg-white'
					]
				]); ?>
					<?//= $form->errorSummary([$modelAgent]); ?>
					<?= $form->field($modelAgent, 'name')->textInput(['class'=>'form-control', 'placeholder'=>'Full Name'])->label(false) ?>
					<?= $form->field($modelAgent, 'title')->textInput(['class'=>'form-control', 'placeholder'=>'Title'])->label(false) ?>
					<?= $form->field($modelAgent, 'gender')->dropDownList($modelAgent->getAllGender(), ['class'=>'form-control'])->label(false) ?>
					<?= $form->field($modelAgent, 'phone_number')->textInput(['class'=>'form-control', 'placeholder'=>'Phone Number', 'type'=>'number'])->label(false) ?>
					<?= $form->field($modelAgent, 'email')->textInput(['class'=>'form-control', 'placeholder'=>'Email Address'])->label(false) ?>
					<?= $form->field($modelAgent, 'username')->textInput(['class'=>'form-control', 'placeholder'=>'Username'])->label(false) ?>
					<?= $form->field($modelAgent, 'password')->passwordInput(['class'=>'form-control', 'placeholder'=>'Password'])->label(false) ?>
					<?= $form->field($modelAgent, 'confirm_password_login')->passwordInput(['class'=>'form-control', 'placeholder'=>'Confirm Password'])->label(false) ?>
					<?//= $form->field($modelAgent, 'award')->textInput(['class'=>'form-control', 'placeholder'=>'Agent Award'])->label(false) ?>
					<?= $form->field($modelAgent, 'speciality')->textInput(['class'=>'form-control', 'placeholder'=>'Agent Specialty'])->label(false) ?>
					<?= $form->field($modelAgent, 'country_operability')->textInput(['class'=>'form-control', 'placeholder'=>'Country'])->label(false) ?>
					<?//= $form->field($modelAgent, 'investment_deposit_range')->textInput(['class'=>'form-control', 'placeholder'=>'Investment Deposit Range'])->label(false) ?>
					<?= Html::submitButton('Create An Account', ['class' => 'btn btn-primary w-100 MyriadPro d-flex align-items-center justify-content-center px-4']) ?>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</section>
<!-- site section main -->