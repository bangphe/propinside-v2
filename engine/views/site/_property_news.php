<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\components\MyFormatter;
?>

<section class="property-main">
    <div class="container bg-white">
        <div class="row">
            <div class="col-lg-12">
                <!-- property item -->
                <div class="property-item">
                    <p>Posted on : <?= MyFormatter::formatTanggal($model->created_at); ?></p>
                    <div style="display:none;" id="<?= 'video1_'.$model->id_property_news; ?>">
                        <?= $model->getYoutubeIframe1(700, 315); ?>
                    </div>
                    <ul class="imageGallery">
                        <?php if($model->youtube_1 != NULL) { ?>
                            <li data-html="<?= '#video1_'.$model->id_property_news; ?>" data-poster="<?= $model->getYoutubeThumbnails1(); ?>">
                                <img class="img-fluid mw-100 w-100" src="<?= $model->getYoutubeThumbnails1(); ?>" style="height: 516px !important;" />
                                <div style="height: 300px;">
                                    <img class="mx-auto d-block thumbnail-yt" src="<?= Yii::getAlias('@web'); ?>/assets/lightgallery/img/youtube-play-red.png" width="64px" height="64px" >
                                </div>
                            </li>
                        <?php } ?>
                        <?php if (empty($model->propertyNewsGalleries)) { ?>
                            <li class="img-fluid test" data-src="<?= Yii::getAlias('@web'); ?>/assets/propinside/img/products/p.jpg">
                                <img src="<?= Yii::getAlias('@web'); ?>/assets/propinside/img/products/p.jpg" class="mw-100 w-100">
                            </li>
                            <li class="img-fluid test" data-src="<?= Yii::getAlias('@web'); ?>/assets/propinside/img/products/p1.jpg">
                                <img src="<?= Yii::getAlias('@web'); ?>/assets/propinside/img/products/p1.jpg" class="mw-100 w-100">
                            </li>
                        <?php } else { ?>
                            <?php foreach ($model->propertyNewsGalleries as $v) { ?>
                            <li class="img-fluid" data-src="<?= Yii::getAlias('@web').$v->image_path; ?>">
                                <img class="img-fluid mx-auto d-block" src="<?= Yii::getAlias('@web').$v->image_path; ?>" style="height: 350px;" />
                            </li>
                        <?php } } ?>
                    </ul>
                    <h2><?= $model->property->title ? ucwords($model->property->title) : 'Title'; ?></h2><br>
                    <p class="text-justify"><?= $model->property->description; ?></p>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="<?= Url::to(['agents/property/'.$model->property->slug]); ?>" class="btn btn-info MyriadPro d-inline-flex align-items-center justify-content-center px-4 mb-4">
                                Learn More
                            </a>
                        </div>
                        <div class="col-md-6 d-flex flex-row-reverse">
                            &nbsp;
                        </div>
                    </div>

                    <div class="row d-flex justify-content-center">
                        <?php foreach($model->property->propertyListings as $data) { ?>
                        <div class="col-12 col-sm-4 col-lg text-center">
                            <!-- team item -->
                            <div class="team-item">
                                <div class="team-img position-relative">
                                    <a href="<?= Url::to(['/agents/detail?id='.$data->id_agent]); ?>"><?= $data->agent->getThumbnailAgent(); ?></a>
                                    <div class="team-img-overlay position-absolute text-white">
                                        <ul class="social-link position-absolute d-flex align-items-center justify-content-center">
                                            <li>
                                                <?php if ($data->agent->facebook != NULL) { ?>
                                                <a href="<?= $data->agent->facebook; ?>" class="d-flex align-items-center justify-content-center" target="_blank">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                                <?php } else { ?>
                                                <a href="javascript:;" class="d-flex align-items-center justify-content-center" style="color: gray;">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                                <?php } ?>
                                            </li>
                                            <li>
                                                <?php if ($data->agent->twitter != NULL) { ?>
                                                <a href="<?= $data->agent->twitter; ?>" class="d-flex align-items-center justify-content-center" target="_blank">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                                <?php } else { ?>
                                                <a href="javascript:;" class="d-flex align-items-center justify-content-center" style="color: gray;">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                                <?php } ?>
                                            </li>
                                            <li>
                                                <?php if ($data->agent->twitter != NULL) { ?>
                                                <a href="<?= $data->agent->linkedin; ?>" class="d-flex align-items-center justify-content-center" target="_blank">
                                                    <i class="fa fa-linkedin"></i>
                                                </a>
                                                <?php } else { ?>
                                                <a href="javascript:;" class="d-flex align-items-center justify-content-center" style="color: gray;">
                                                    <i class="fa fa-linkedin"></i>
                                                </a>
                                                <?php } ?>
                                            </li>
                                        </ul>
                                        <h6>
                                            <a href="<?= Url::to(['/agents/detail?id='.$data->id_agent]); ?>" style="color: #000000;"><?= ucwords($data->agent->name); ?></a>
                                        </h6>
                                        <p>
                                            <a href="<?= Url::to(['/agents/detail?id='.$data->id_agent]); ?>" style="color: #676767;"><?= ucwords($data->agent->title); ?></a>
                                        </p>
                                    </div>
                                </div>
                                <a href="#" class="btn btn-info MyriadPro d-inline-flex align-items-center justify-content-center" data-toggle="modal" data-target="#modal-contact">
                                    Contact Me 
                                </a>
                            </div>
                            <!-- team item -->
                        </div>
                        <?php } ?>
                        <?php if (count($model->property->propertyListings) == 2) { ?>
                        <div class="col-12 col-sm-4 col-lg text-center"></div>
                        <div class="col-12 col-sm-4 col-lg text-center"></div>
                        <div class="col-12 col-sm-4 col-lg text-center"></div>
                        <?php } elseif (count($model->property->propertyListings) == 3) { ?>
                        <div class="col-12 col-sm-4 col-lg text-center"></div>
                        <div class="col-12 col-sm-4 col-lg text-center"></div>
                        <?php } elseif (count($model->property->propertyListings) == 4) { ?>
                        <div class="col-12 col-sm-4 col-lg text-center"></div>
                        <?php } else { ?>
                        <div class="col-12 col-sm-4 col-lg text-center"></div>
                        <div class="col-12 col-sm-4 col-lg text-center"></div>
                        <div class="col-12 col-sm-4 col-lg text-center"></div>
                        <div class="col-12 col-sm-4 col-lg text-center"></div>
                        <?php } ?>
                    </div>
                </div>
                <!-- property item -->
            </div>
        </div>
        <hr>
    </div>
</section>