<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\AppAsset;

AppAsset::register($this);
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->title = 'Login';
?>

<!-- agents register -->
<section class="agents-register">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 offset-lg-1 text-center">
				<h4>AGENT REGISTRATION</h4>
				<p style="padding-bottom: 50px;">
					Ut et, ex sed veniam tempor, irure eu, magna nisi. Minim id esse laboris magna do. Qui id ad. Labore exercitation do commodo dolore. Labore anim mollit in enim, sit est, amet nulla eiusmod magna sint in deserunt, in aliquip.
				</p>
			</div>
			<div class="col-lg-12">
				<?= Yii::$app->session->getFlash('info');?>
			</div>
			<div class="col-lg-8 offset-lg-2">
				<div class="agents-register-box" id="accordion">
					<div class="row mb-5 agents-register-head">
						<div class="col-6">
							<button class="btn btn-white d-flex align-items-center justify-content-center w-100" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
								Login
							</button>
						</div>
						<div class="col-6">
							<button class="btn btn-white d-flex align-items-center justify-content-center w-100" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								Register
							</button>
						</div>
					</div>
					<?php $form = ActiveForm::begin([
						'id' => 'collapseTwo',
						'enableClientValidation' => true,
						'options' => [
					        'class' => 'collapse',
					        'aria-labelledby' => 'headingTwo',
					    	'data-parent' => '#accordion',
					    ],
					]); ?>
						<?= $form->field($model, 'username')->textInput(['class'=>'form-control mb-3', 'placeholder'=>'Username', 'autocomplete'=>'off'])->label(false) ?>
						<?= $form->field($model, 'password')->passwordInput(['class'=>'form-control mb-3', 'placeholder'=>'Password', 'autocomplete'=>'off'])->label(false) ?>
						<div class="d-flex align-items-center mb-3 mt-5">
							<input type="checkbox" class="mr-3" id="checkbox">
							<label for="checkbox" class="mb-0 text-white">Remember Me</label>
						</div>
						<?= Html::submitButton('Login', ['class' => 'btn-submit d-flex align-items-center justify-content-center w-100 btn btn-warning']) ?>
					<?php ActiveForm::end(); ?>
					<?php $form = ActiveForm::begin([
						'id' => 'collapseOne',
						'enableClientValidation' => true,
						'options' => [
					        'class' => 'collapse show',
					        'aria-labelledby' => 'headingOne',
					    	'data-parent' => '#accordion',
					    ],
					]); ?>
						<?= $form->errorSummary([$modelAgent]); ?>
						<?= $form->field($modelAgent, 'name')->textInput(['class'=>'form-control mb-3', 'placeholder'=>'Full Name'])->label(false) ?>
						<?= $form->field($modelAgent, 'title')->textInput(['class'=>'form-control mb-3', 'placeholder'=>'Title'])->label(false) ?>
						<?= $form->field($modelAgent, 'phone_number')->textInput(['class'=>'form-control mb-3', 'placeholder'=>'Phone Number', 'type'=>'number'])->label(false) ?>
						<?= $form->field($modelAgent, 'email')->textInput(['class'=>'form-control mb-3', 'placeholder'=>'Email Address'])->label(false) ?>
						<?= $form->field($modelAgent, 'username')->textInput(['class'=>'form-control mb-3', 'placeholder'=>'Username'])->label(false) ?>
						<?= $form->field($modelAgent, 'password')->passwordInput(['class'=>'form-control mb-3', 'placeholder'=>'Password'])->label(false) ?>
						<?= $form->field($modelAgent, 'confirm_password_login')->passwordInput(['class'=>'form-control mb-3', 'placeholder'=>'Confirm Password'])->label(false) ?>
						<?= $form->field($modelAgent, 'award')->textInput(['class'=>'form-control mb-3', 'placeholder'=>'Agent Award'])->label(false) ?>
						<?= $form->field($modelAgent, 'speciality')->textInput(['class'=>'form-control mb-3', 'placeholder'=>'Agent Specialty'])->label(false) ?>
						<?= $form->field($modelAgent, 'country_operability')->textInput(['class'=>'form-control mb-3', 'placeholder'=>'Country Operability'])->label(false) ?>
						<?= $form->field($modelAgent, 'investment_deposit_range')->textInput(['class'=>'form-control mb-3', 'placeholder'=>'Investment Deposit Range'])->label(false) ?>
						<!-- <div class="d-flex align-items-center mb-3 mt-5"> -->
							<!-- <input type="checkbox" class="mr-3" id="checkbox"> -->
							<!-- <label for="checkbox" class="mb-0 text-white">Remember Me</label> -->
						<!-- </div> -->
						<?= Html::submitButton('CREATE AN ACCOUNT', ['class' => 'btn-submit d-flex align-items-center justify-content-center w-100 btn btn-warning']) ?>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- agents register -->