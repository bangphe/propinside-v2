<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $name;
?>

<!-- 404 Error Start -->
<section id="error-404" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="error-image">
          <img src="<?= Url::base(); ?>/assets/castle/images/404.png" alt="image" class="img-responsive"/>
        </div>
        <div class="error-text">
          <h1>Oops!!</h1>
          <h3>Looks like something went wrong. <?= Html::encode($this->title) ?></h3>
          <p><?= nl2br(Html::encode($message)) ?></p>
          <div class="erro-button">
            <a href="<?= Url::home(); ?>" class="btn-blue">go back to home page</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- 404 Error End -->