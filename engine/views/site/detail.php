<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<!-- property main section -->
<section class="property-main">
	<div class="container">

		<div class="row property-img my-5">
			<div class="col-sm-8 property-thum position-relative">
				<img src="<?= Url::base(); ?>/assets/img/properties/2.png" alt="properties" class="mw-100 w-100 h-100 position-absolute">
			</div>
			<div class="col-sm-4 property-img-multi">
				<img src="<?= Url::base(); ?>/assets/img/properties/3.png" alt="properties" class="mw-100">
				<img src="<?= Url::base(); ?>/assets/img/properties/4.png" alt="properties" class="mw-100">
			</div>
		</div>

		<div class="row">

			<div class="col-lg-9">
				<!-- property-price-size -->
				<div class="property-price-size">
					<h4 class="PalatinoLinotypeBold d-flex align-items-center">
						S$ 1.200.000
						<ul class="ml-5 d-inline-flex align-items-center">
							<li>STARTING FROM</li>
							<li>Est. Mortgage S$3,110 /mo</li>
							<li>Find your eligibility</li>
						</ul>
					</h4>
					<div class="pro-size-info d-flex align-items-center">
						<ul class="pro-size d-flex align-items-center">
							<li>Beds 2</li>
							<li>Beds 2</li>
						</ul>
						<span class="ml-4 mr-4">sqft</span>
						<ul class="pro-price d-flex align-items-center">
							<li><span>S$</span> 1,548.39</li>
							<li><span>psf</span> 775</li>
						</ul>
					</div>
				</div>
				<!-- property-price-size -->
			</div>

			<div class="col-lg-12">
				<!-- property-address -->
				<div class="pro-address">
					<h4 class="PalatinoLinotypeBold">Lakeville</h4>
					<p>
						Jurong Lake Link 649999 Boon Lay/ Jurong / Tuas (D22)
						<i class="fa fa-map-marker text-warning"></i>
					</p>
					<p class="text-danger">
						Discover more about Jurong
					</p>
				</div>
				<!-- property-address -->
			</div>

			<div class="col-lg-10">
				<!-- property detail -->
				<div class="pro-detail">
					<h4>Details</h4>
					<div class="row">
						<div class="col-sm-6">
							<ul class="form-read-only">
								<li>
									<h6>Type</h6>
									<p>Condominium For Sale</p>
								</li>
								<li>
									<h6>Type</h6>
									<p>Condominium For Sale</p>
								</li>
								<li>
									<h6>Type</h6>
									<p>Condominium For Sale</p>
								</li>
								<li>
									<h6>Type</h6>
									<p>Condominium For Sale</p>
								</li>
								<li>
									<h6>Type</h6>
									<p>Condominium For Sale</p>
								</li>
							</ul>
						</div>
						<div class="col-sm-6">
							<ul class="form-read-only">
								<li>
									<h6>Type</h6>
									<p>Condominium For Sale</p>
								</li>
								<li>
									<h6>Type</h6>
									<p>Condominium For Sale</p>
								</li>
								<li>
									<h6>Type</h6>
									<p>Condominium For Sale</p>
								</li>
								<li>
									<h6>Type</h6>
									<p>Condominium For Sale</p>
								</li>
								<li>
									<h6>Type</h6>
									<p>Condominium For Sale</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- property detail -->
			</div>

			<div class="col-lg-10">
				<!-- property description -->
				<div class="pro-description">
					<h5>
						Description
					</h5>
					<p>
						Lakeville is a condominimum located in Jurong Lake link, in district D22. This condominium is primarily used for Condominium rental anda sale. This Condominium space is 0,57 km away from EW26 Lakeside MRT Station. You can also see from the map above how to get there via other means of transpo
					</p>
					<p>
						The tenure of this condominium is 99-year leasehold
					</p>
					<p>
						Other information on this condominium is available in the detail description above or you can contact the property owner to ask for more details on this condominium
						Read an in-depth, unbiased Lakeville Review and get our take on the location, project, price potensial, complete with high resolution photos, 360-degree views even 3d virtual walkthroughs
					</p>
				</div>
				<!-- property description -->
			</div>

			<div class="col-lg-10">
				<!-- property facilities & info -->
				<div class="pro-facilities" id="accordion">
					<h5>Facilities & Amenities</h5>
					<ul class="facilities-accordin row mx-0">
						<li class="col px-0">
							<button data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Key Features</button>
						</li>
						<li class="col px-0">
							<button data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Facilities</button>
						</li>
					</ul>
					<div class="collapse show" id="collapseOne" aria-labelledby="headingOne" data-parent="#accordion">
						<div class="row fac-accordin-body">
							<div class="col-sm-6 d-flex align-items-center">
								<div><img src="<?= Url::base(); ?>/assets/img/icons/star.png" alt="star" class="mw-100"></div>
								<p>Air-Conditioning</p>
							</div><div class="col-sm-6 d-flex align-items-center">
								<div><img src="<?= Url::base(); ?>/assets/img/icons/star.png" alt="star" class="mw-100"></div>
								<p>Air-Conditioning</p>
							</div><div class="col-sm-6 d-flex align-items-center">
								<div><img src="<?= Url::base(); ?>/assets/img/icons/star.png" alt="star" class="mw-100"></div>
								<p>Air-Conditioning</p>
							</div>
						</div>
					</div>
					<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
						<div class="row fac-accordin-body">
							<div class="col-sm-6 d-flex align-items-center">
								<div><img src="<?= Url::base(); ?>/assets/img/icons/star.png" alt="star" class="mw-100"></div>
								<p>Air-Conditioning</p>
							</div><div class="col-sm-6 d-flex align-items-center">
								<div><img src="<?= Url::base(); ?>/assets/img/icons/star.png" alt="star" class="mw-100"></div>
								<p>Air-Conditioning</p>
							</div><div class="col-sm-6 d-flex align-items-center">
								<div><img src="<?= Url::base(); ?>/assets/img/icons/star.png" alt="star" class="mw-100"></div>
								<p>Air-Conditioning</p>
							</div>
						</div>
					</div>
				</div>
				<div class="pro-info">
					<h5>Project Info</h5>
					<p>Lakeville</p>
					<div class="row">
						<div class="col-sm-6 d-flex align-items-center">
							<img src="<?= Url::base(); ?>/assets/img/properties/5.png" alt="properties" class="mw-100 w-100">
						</div>
						<div class="col-sm-6 d-flex align-items-center">
							<ul class="form-read-only">
								<li>
									<h6>Type</h6>
									<p>Condominium For Sale</p>
								</li>
								<li>
									<h6>Type</h6>
									<p>Condominium For Sale</p>
								</li>
								<li>
									<h6>Type</h6>
									<p>Condominium For Sale</p>
								</li>
								<li>
									<h6>Type</h6>
									<p>Condominium For Sale</p>
								</li>
								
							</ul>
						</div>
					</div>
					<div class="pro-actions">
						<ul class="d-flex align-items-center">
							<li><a href="#" class="text-danger">&lt; Lakeville Project Details &gt;</a></li>
							<li><a href="#" class="text-danger">Read Review of Lakeville</a></li>
						</ul>
						<ul class="d-flex align-items-center">
							<li><a href="#" class="text-danger">View other listing in Lakeville</a></li>
						</ul>
					</div>
				</div>
				<!-- property facilities & info -->
			</div>

			<div class="col-lg-12">
				<!-- pricing insights -->
				<div class="pricing-insights-main">
					<h4 class="d-inline-flex align-items-center justify-content-center"><i class="fa fa-flag d-inline-flex align-items-center justify-content-center"></i> Pricing Insights</h4>
					<div class="row">

						<div class="col-lg-4">
							<!-- pricing insights item -->
							<div class="pricing-insights text-center">
								<div class="pricing-insights-head">
									<img src="<?= Url::base(); ?>/assets/img/properties/6.png" alt="properties" class="mw-100 w-100">
								</div>
								<div class="pricing-insights-info">
									<p>
										Single Family Home
									</p>
									<h4>S$954,000</h4>
									<ul class="d-flex align-items-center justify-content-center">
										<li>2,503Sq Ft</li>
										<li>5 Beds</li>
										<li>2 Baths</li>
									</ul>
								</div>
							</div>
							<!-- pricing insights item -->
						</div>
						<div class="col-lg-4">
							<!-- pricing insights item -->
							<div class="pricing-insights text-center">
								<div class="pricing-insights-head">
									<img src="<?= Url::base(); ?>/assets/img/properties/6.png" alt="properties" class="mw-100 w-100">
								</div>
								<div class="pricing-insights-info">
									<p>
										Single Family Home
									</p>
									<h4>S$954,000</h4>
									<ul class="d-flex align-items-center justify-content-center">
										<li>2,503Sq Ft</li>
										<li>5 Beds</li>
										<li>2 Baths</li>
									</ul>
								</div>
							</div>
							<!-- pricing insights item -->
						</div>
						<div class="col-lg-4">
							<!-- pricing insights item -->
							<div class="pricing-insights text-center">
								<div class="pricing-insights-head">
									<img src="<?= Url::base(); ?>/assets/img/properties/6.png" alt="properties" class="mw-100 w-100">
								</div>
								<div class="pricing-insights-info">
									<p>
										Single Family Home
									</p>
									<h4>S$954,000</h4>
									<ul class="d-flex align-items-center justify-content-center">
										<li>2,503Sq Ft</li>
										<li>5 Beds</li>
										<li>2 Baths</li>
									</ul>
								</div>
							</div>
							<!-- pricing insights item -->
						</div>
						<div class="col-lg-4">
							<!-- pricing insights item -->
							<div class="pricing-insights text-center">
								<div class="pricing-insights-head">
									<img src="<?= Url::base(); ?>/assets/img/properties/6.png" alt="properties" class="mw-100 w-100">
								</div>
								<div class="pricing-insights-info">
									<p>
										Single Family Home
									</p>
									<h4>S$954,000</h4>
									<ul class="d-flex align-items-center justify-content-center">
										<li>2,503Sq Ft</li>
										<li>5 Beds</li>
										<li>2 Baths</li>
									</ul>
								</div>
							</div>
							<!-- pricing insights item -->
						</div>
						<div class="col-lg-4">
							<!-- pricing insights item -->
							<div class="pricing-insights text-center">
								<div class="pricing-insights-head">
									<img src="<?= Url::base(); ?>/assets/img/properties/6.png" alt="properties" class="mw-100 w-100">
								</div>
								<div class="pricing-insights-info">
									<p>
										Single Family Home
									</p>
									<h4>S$954,000</h4>
									<ul class="d-flex align-items-center justify-content-center">
										<li>2,503Sq Ft</li>
										<li>5 Beds</li>
										<li>2 Baths</li>
									</ul>
								</div>
							</div>
							<!-- pricing insights item -->
						</div>
						<div class="col-lg-4">
							<!-- pricing insights item -->
							<div class="pricing-insights text-center">
								<div class="pricing-insights-head">
									<img src="<?= Url::base(); ?>/assets/img/properties/6.png" alt="properties" class="mw-100 w-100">
								</div>
								<div class="pricing-insights-info">
									<p>
										Single Family Home
									</p>
									<h4>S$954,000</h4>
									<ul class="d-flex align-items-center justify-content-center">
										<li>2,503Sq Ft</li>
										<li>5 Beds</li>
										<li>2 Baths</li>
									</ul>
								</div>
							</div>
							<!-- pricing insights item -->
						</div>

					<div class="col-lg-12 text-center">
							<a href="<?= Url::to(['/site/property']); ?>" class="btn btn-primary d-lg-inline-flex d-flex align-items-center justify-content-center">
								VIEW ALL LISTINGS
							</a>
						</div></div>
					<p class="pricing-insights-warn">risk level of investment (based on property expert)</p>
				</div>
				<!-- pricing insights -->
			</div>

			<div class="col-lg-12">

				<!-- agents section -->
				<div class="agents-main">
					<div class="agents-head text-center">
						<h4>Contact Agent About This Property</h4>
						<p>
							Ut et, ex sed veniam tempor, irure eu, magna nisi. Minim id esse laboris magna do. Qui id ad. Labore exercitation do commodo dolore. Labore anim mollit in enim, sit est, amet nulla eiusmod magna sint in deserunt, in aliquip.
						</p>
					</div>
					<div class="row">

						<div class="col-lg-2 col-sm-4">
							<!-- agents item -->
							<div class="agents-item">
								<a href="<?= Url::to(['/site/agentsdetail']); ?>">
									<img src="<?= Url::base(); ?>/assets/img/testimonial/1.png" alt="testimonial" class="mw-100 w-100">
									<h6>MARK JOHNSON</h6>
									<p>
										Cupidatat, adipisicing commodo pariatur magna.
									</p>
									<ul class="social-link d-flex align-items-center">
										<li>
											<a href="#" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-facebook"></i>
											</a>
										</li>
										<li>
											<a href="#" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-twitter"></i>
											</a>
										</li>
										<li>
											<a href="#" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-linkedin"></i>
											</a>
										</li>
									</ul>
								</a>
							</div>
							<!-- agents item -->
						</div>
						<div class="col-lg-2 col-sm-4">
							<!-- agents item -->
							<div class="agents-item">
								<a href="<?= Url::to(['/site/agentsdetail']); ?>">
									<img src="<?= Url::base(); ?>/assets/img/testimonial/1.png" alt="testimonial" class="mw-100 w-100">
									<h6>MARK JOHNSON</h6>
									<p>
										Cupidatat, adipisicing commodo pariatur magna.
									</p>
									<ul class="social-link d-flex align-items-center">
										<li>
											<a href="#" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-facebook"></i>
											</a>
										</li>
										<li>
											<a href="#" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-twitter"></i>
											</a>
										</li>
										<li>
											<a href="#" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-linkedin"></i>
											</a>
										</li>
									</ul>
								</a>
							</div>
							<!-- agents item -->
						</div>
						<div class="col-lg-2 col-sm-4">
							<!-- agents item -->
							<div class="agents-item">
								<a href="<?= Url::to(['/site/agentsdetail']); ?>">
									<img src="<?= Url::base(); ?>/assets/img/testimonial/1.png" alt="testimonial" class="mw-100 w-100">
									<h6>MARK JOHNSON</h6>
									<p>
										Cupidatat, adipisicing commodo pariatur magna.
									</p>
									<ul class="social-link d-flex align-items-center">
										<li>
											<a href="#" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-facebook"></i>
											</a>
										</li>
										<li>
											<a href="#" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-twitter"></i>
											</a>
										</li>
										<li>
											<a href="#" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-linkedin"></i>
											</a>
										</li>
									</ul>
								</a>
							</div>
							<!-- agents item -->
						</div>
						<div class="col-lg-2 col-sm-4">
							<!-- agents item -->
							<div class="agents-item">
								<a href="<?= Url::to(['/site/agentsdetail']); ?>">
									<img src="<?= Url::base(); ?>/assets/img/testimonial/1.png" alt="testimonial" class="mw-100 w-100">
									<h6>MARK JOHNSON</h6>
									<p>
										Cupidatat, adipisicing commodo pariatur magna.
									</p>
									<ul class="social-link d-flex align-items-center">
										<li>
											<a href="#" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-facebook"></i>
											</a>
										</li>
										<li>
											<a href="#" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-twitter"></i>
											</a>
										</li>
										<li>
											<a href="#" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-linkedin"></i>
											</a>
										</li>
									</ul>
								</a>
							</div>
							<!-- agents item -->
						</div>
						<div class="col-lg-2 col-sm-4">
							<!-- agents item -->
							<div class="agents-item">
								<a href="<?= Url::to(['/site/agentsdetail']); ?>">
									<img src="<?= Url::base(); ?>/assets/img/testimonial/1.png" alt="testimonial" class="mw-100 w-100">
									<h6>MARK JOHNSON</h6>
									<p>
										Cupidatat, adipisicing commodo pariatur magna.
									</p>
									<ul class="social-link d-flex align-items-center">
										<li>
											<a href="#" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-facebook"></i>
											</a>
										</li>
										<li>
											<a href="#" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-twitter"></i>
											</a>
										</li>
										<li>
											<a href="#" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-linkedin"></i>
											</a>
										</li>
									</ul>
								</a>
							</div>
							<!-- agents item -->
						</div>
						<div class="col-lg-2 col-sm-4">
							<!-- agents item -->
							<div class="agents-item">
								<a href="<?= Url::to(['/site/agentsdetail']); ?>">
									<img src="<?= Url::base(); ?>/assets/img/testimonial/1.png" alt="testimonial" class="mw-100 w-100">
									<h6>MARK JOHNSON</h6>
									<p>
										Cupidatat, adipisicing commodo pariatur magna.
									</p>
									<ul class="social-link d-flex align-items-center">
										<li>
											<a href="#" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-facebook"></i>
											</a>
										</li>
										<li>
											<a href="#" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-twitter"></i>
											</a>
										</li>
										<li>
											<a href="#" class="d-inline-flex align-items-center justify-content-center">
												<i class="fa fa-linkedin"></i>
											</a>
										</li>
									</ul>
								</a>
							</div>
							<!-- agents item -->
						</div>

					</div>
				</div>
				<!-- agents section -->

			</div>

		</div>
	</div>
</section>
<!-- property main section -->