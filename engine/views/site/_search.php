<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\PropertySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row blue_navi">
    <div class="col-md-12">
        <div class="property-query-area">
            <!-- <form class="callus clearfix"> -->
            <?php $form = ActiveForm::begin([
                'action' => ['site/search'],
                'id' => 'search-form',
                'options' => [
                    'class' => 'callus clearfix',
                ],
                'method' => 'get',
            ]); ?>
              <div class="col-md-4 col-sm-6">
                <div class="single-query form-group">
                  <!-- <input type="text" class="keyword-input" placeholder="Keyword (e.g. 'office')"> -->
                  <?= $form->field($searchModel, 'title')->textInput(['class' => 'keyword-input', 'placeholder' => "Keyword (e.g. 'office')"])->label(false); ?>
                </div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="single-query form-group">
                  <div class="intro">
                    <?php
                    $searchModel->location = empty($_GET['PropertySearch']['location']) ? [] : $_GET['PropertySearch']['location']; // initial value
                    echo $form->field($searchModel, 'location')->widget(Select2::classname(), [
                        'name' => 'location',
                        'data' => $dataDistrict,
                        'size' => Select2::MEDIUM,
                        'options' => ['placeholder' => 'Location', 'multiple' => true],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false); ?>
                  </div>
                </div>
              </div>
              <div class="col-md-2 col-sm-12 text-right">
                <?= Html::submitButton('<i class="icon-search"></i> Search', ['class' => 'border_radius btn-yellow text-center text-uppercase', 'style' => 'width: 100%; color: #fff;']) ?>
              </div>
              <div class="col-md-2 col-sm-12 text-right">
                <button type="button" class="advanced text-center text-uppercase border_radius"><i class="icon-settings"></i>advanced</button>
              </div>
            <!-- </form> -->
            
            <div class="opened" style="<?= !empty($_GET['PropertySearch']['type']) || !empty($_GET['PropertySearch']['status']) ? 'display: block;' : 'display: none;'; ?>">
              <!-- <form class="callus clearfix"> -->
                <div class="col-md-6 col-sm-6">
                  <div class="single-query form-group">
                    <div class="intro">
                      <?= $form->field($searchModel, 'type')->widget(Select2::classname(), [
                          'data' => $dataType,
                          'options' => ['placeholder' => 'Select Property Type'],
                          'size' => Select2::MEDIUM,
                          'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ])->label(false); ?>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <div class="single-query form-group">
                    <div class="intro">
                      <?= $form->field($searchModel, 'status')->widget(Select2::classname(), [
                          'data' => $dataStatus,
                          'options' => ['placeholder' => 'Select Property Status'],
                          'size' => Select2::MEDIUM,
                          'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ])->label(false); ?>
                    </div>
                  </div>
                </div>
                <!-- <div class="col-md-3 col-sm-6">
                  <div class="row search-2">
                    <div class="col-md-6 col-sm-6">
                      <div class="single-query form-group">
                        <div class="intro">
                          <select>
                            <option class="active">Min Beds</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                      <div class="single-query form-group">
                        <div class="intro">
                          <select>
                            <option class="active">Min Baths</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> -->
                <!-- <div class="col-md-3 col-sm-6">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="single-query form-group">
                        <input type="text" class="keyword-input" placeholder="Min Area (sq ft)">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="single-query form-group">
                        <input type="text" class="keyword-input" placeholder="Max Area (sq ft)">
                      </div>
                    </div>
                  </div>
                </div> -->
              <!-- </form> -->
              <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    </div>