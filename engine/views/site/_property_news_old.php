<?php
use yii\helpers\Url;
use yii\helpers\Html;

?>

<div class="row property-list-item">
    <div class="col-sm-4 d-flex align-items-center">
        <?php //if($model->image_path != NULL) { ?>
            <!-- <img src="<?//= Yii::getAlias('@web').$model->image_path; ?>" alt="properties" class="mw-100 img-fluid" width="100%"> -->
        <?php //} else { ?>
            <?//= $model->getYoutubeIframe(700, 250); ?>
        <?php //} ?>
        <div style="display:none;" id="<?= 'video1_'.$model->id_property_news; ?>">
            <?= $model->getYoutubeIframe1(700, 315); ?>
        </div>
        <!-- <div style="display:none;" id="<?//= 'video2_'.$model->id_property_news; ?>">
            <?//= $model->getYoutubeIframe2(700, 315); ?>
        </div> -->
        <ul class="imageGallery">
            <?php if($model->youtube_1 != NULL) { ?>
                <li data-html="<?= '#video1_'.$model->id_property_news; ?>" data-poster="<?= $model->getYoutubeThumbnails1(); ?>">
                    <img class="img-fluid img-thumbnail" src="<?= $model->getYoutubeThumbnails1(); ?>" />
                    <div style="height: 150px;">
                        <img class="mx-auto d-block" src="<?= Yii::getAlias('@web'); ?>/assets/lightgallery/img/video-play.png" style="margin-top: -165px;">
                    </div>
                </li>
            <?php } ?>
            <?php //if ($model->youtube_2 != NULL) { ?>
                <!-- <li data-html="<?//= '#video2_'.$model->id_property_news; ?>" data-poster="<?//= $model->getYoutubeThumbnails2(); ?>">
                    <img class="img-fluid img-thumbnail" src="<?//= $model->getYoutubeThumbnails2(); ?>" />
                    <div style="height: 150px;">
                        <img class="mx-auto d-block" src="<?//= Yii::getAlias('@web'); ?>/assets/lightgallery/img/video-play.png" style="margin-top: -165px;">
                    </div>
                </li> -->
            <?php //} ?>
            <?php if (empty($model->propertyNewsGalleries)) { ?>
                <li class="img-fluid img-thumbnail test" data-src="<?= Yii::getAlias('@web'); ?>/assets/lightslider/demo/img/cS-1.jpg">
                    <img class="img-fluid img-thumbnail mx-auto d-block test" src="<?= Yii::getAlias('@web'); ?>/assets/lightslider/demo/img/cS-1.jpg" />
                </li>
                <li class="img-fluid img-thumbnail test" data-src="<?= Yii::getAlias('@web'); ?>/assets/lightslider/demo/img/cS-2.jpg">
                    <img class="img-fluid img-thumbnail mx-auto d-block test" src="<?= Yii::getAlias('@web'); ?>/assets/lightslider/demo/img/cS-2.jpg" />
                </li>
                <li class="img-fluid img-thumbnail test" data-src="<?= Yii::getAlias('@web'); ?>/assets/lightslider/demo/img/cS-3.jpg">
                    <img class="img-fluid img-thumbnail mx-auto d-block test" src="<?= Yii::getAlias('@web'); ?>/assets/lightslider/demo/img/cS-3.jpg" />
                </li>
            <?php } else { ?>
                <?php foreach ($model->propertyNewsGalleries as $v) { ?>
                <li class="img-fluid img-thumbnail" data-src="<?= Yii::getAlias('@web').$v->image_path; ?>">
                    <img class="img-fluid img-thumbnail mx-auto d-block" src="<?= Yii::getAlias('@web').$v->image_path; ?>" style="width: 350px;" />
                </li>
            <?php } } ?>
        </ul>
    </div>
    <div class="col-sm-8 mt-sm-0 mt-4 d-flex align-items-center">
        <div class="property-list-info">
            <h4><?= $model->title; ?></h4>
            <h5><?= 'Location : ' . $model->property->location; ?>. Posted by: <?= $model->user->username; ?></h5>
            <p><?= $model->property->description; ?></p>
            <a href="<?= Url::to(['agents/property/'.$model->property->slug]); ?>" class="btn btn-info d-inline-flex align-items-center justify-content-center float-right">Learn More</a>
            <div class="row">
                <?php foreach($model->property->propertyListings as $data) { ?>
                <div class="col-md-2" style="max-width: 25% !important;">
                    <!-- agents item -->
                    <div class="agents-item">
                        <a href="<?= Url::to(['/agents/detail?id='.$data->id_agent]); ?>">
                            <?= $data->agent->getThumbnail(); ?>
                            <h6><?= $data->agent->name; ?></h6>
                            <p style="max-width: 30px;">
                                <?= $data->agent->description != NULL ? limit_text($data->agent->description, 3) : limit_text('Cupidatat, adipisicing commodo pariatur magna.', 3); ?>
                            </p>
                        </a>
                        <!-- <a href="#" class="btn btn-info d-inline-flex align-items-center justify-content-center" title="Contact agent" data-toggle="modal" onclick="sendContact('<?//= $data->agent->id_agent; ?>')">Contact</a> -->
                        <a href="#" class="btn btn-info d-inline-flex align-items-center justify-content-center" title="Contact agent" data-toggle="modal" data-target="#modal-contact">Contact</a>
                    </div>
                    <!-- agents item -->
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
