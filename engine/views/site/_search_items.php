<?php
use yii\helpers\Url;
use yii\helpers\Html;

?>

<div class="col-sm-4">
  <div class="property_item heading_space">
    <div class="image">
      <?php if($model->propertyImages != NULL && $model->propertyImages[0]->image_path != NULL) { ?>
          <a href="#"><img src="<?= Yii::getAlias('@web').$model->propertyImages[0]->image_path; ?>" alt="property" class="img-responsive" style="width: 360px; height: 260px;"></a>
      <?php } else { ?>
          <a href="#"><img src="<?= Url::base(); ?>/assets/castle/images/listing.jpg" alt="property" class="img-responsive"></a>
      <?php } ?>
      <div class="price clearfix"> 
        <span class="tag pull-right"><?= '$'.number_format($model->price, 2); ?></span>
      </div>
      <span class="tag_t">For Sale</span> 
      <span class="tag_l">Featured</span>
    </div>
    <div class="proerty_content">
      <div class="proerty_text">
        <h4 class="captlize"><a href="#"><?= ucfirst($model->title); ?></a></h4>
        <p><?= $model->location; ?></p>
      </div>
      <div class="property_meta transparent"> 
        <span><i class="icon-select-an-objecto-tool"></i><?= $model->land_size ? $model->land_size : '0'; ?> sq ft</span> 
        <span><i class="icon-tag"></i><?= ucfirst($model->type); ?></span>
        <span><i class="icon-safety-shower"></i><?= ucfirst($model->tenure); ?></span>
      </div>
      <div class="property_meta transparent bottom30">
        <?php foreach ($model->propertyHasFacilities as $k => $v) { ?>
          <span><i class="icon-bed"></i><?= $v->facilities->facilities; ?></span>
        <?php } ?>
      </div>
      <div class="favroute clearfix">
        <p class="pull-md-left"><?= time_elapsed_string($model->created_at); ?> &nbsp; <i class="icon-calendar2"></i></p>
      </div>
    </div>
  </div>
</div>