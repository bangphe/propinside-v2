<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<!-- start site slider -->
<section class="slider-main position-relative d-flex align-items-center justify-content-center mb-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 offset-lg-3">
                <!-- start slider search -->
                <div class="slider-search" id="accordion">
                    <h3>Search </h3>
                    <ul class="accordion-button d-flex align-items-center">
                        <li>
                            <button data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Locations</button>
                        </li>
                        <li>
                            <button data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Keyword</button>
                        </li>
                    </ul>
                    <div class="row accordion-info">
                        <div class="col-12 collapse show" id="collapseOne" aria-labelledby="headingOne" data-parent="#accordion">
                            <form action="#" class="input-group">
                                <input type="text" class="form-control" name="Locations" placeholder="Enter Location: City, State or Zip code here">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <button type="submit"><img src="<?= Url::base(); ?>/assets/img/icons/search.png" alt="search" class="mw-100"></button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-12 collapse" id="collapseTwo" aria-labelledby="headingTwo" data-parent="#accordion">
                            <form action="#" class="input-group">
                                <input type="text" class="form-control" name="Keyword" placeholder="Enter Keyword: Hello World">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <button type="submit"><img src="<?= Url::base(); ?>/assets/img/icons/search.png" alt="search" class="mw-100"></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- end slider search -->
            </div>
        </div>
    </div>
</section>
<!-- end site slider -->

<!-- property list section -->
<section class="property-main property-list">
	<div class="container">

		<div class="row property-img my-5">
			<div class="col-sm-8 property-thum position-relative">
				<img src="<?= Url::base(); ?>/assets/img/properties/2.png" alt="properties" class="mw-100 w-100 h-100 position-absolute">
			</div>
			<div class="col-sm-4 property-img-multi">
				<img src="<?= Url::base(); ?>/assets/img/properties/3.png" alt="properties" class="mw-100">
				<img src="<?= Url::base(); ?>/assets/img/properties/4.png" alt="properties" class="mw-100">
			</div>
		</div>

		<div class="row agents-register text-center">
			<div class="col-lg-10 offset-lg-1">
				<h4>PROPERTY LISTING</h4>
				<p class="pb-0">
					Ut et, ex sed veniam tempor, irure eu, magna nisi. Minim id esse laboris magna do. Qui id ad. Labore exercitation do commodo dolore. Labore anim mollit in enim, sit est, amet nulla eiusmod magna sint in deserunt, in aliquip.
				</p>
			</div>
		</div>

		<!-- property-list-item -->
		<div class="row property-list-item">
			<div class="col-sm-4 d-flex align-items-center">
				<img src="<?= Url::base(); ?>/assets/img/properties/9.png" alt="properties" class="mw-100">
			</div>
			<div class="col-sm-8 mt-sm-0 mt-4 d-flex align-items-center">
				<div class="property-list-info">
					<h4>Lorem ipsum dolor sit amet</h4>
					<h5>Location info here. Posted by: John Smith</h5>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
					</p>
					<a href="<?= Url::to(['/site/detail']); ?>" class="btn btn-info d-inline-flex align-items-center justify-content-center">Learn More</a>
				</div>
			</div>
		</div>
		<!-- property-list-item -->

		<!-- property-list-item -->
		<div class="row property-list-item">
			<div class="col-sm-4 d-flex align-items-center">
				<img src="<?= Url::base(); ?>/assets/img/properties/9.png" alt="properties" class="mw-100">
			</div>
			<div class="col-sm-8 mt-sm-0 mt-4 d-flex align-items-center">
				<div class="property-list-info">
					<h4>Lorem ipsum dolor sit amet</h4>
					<h5>Location info here. Posted by: John Smith</h5>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
					</p>
					<a href="<?= Url::to(['/site/detail']); ?>" class="btn btn-info d-inline-flex align-items-center justify-content-center">Learn More</a>
				</div>
			</div>
		</div>
		<!-- property-list-item -->

		<!-- property-list-item -->
		<div class="row property-list-item">
			<div class="col-sm-4 d-flex align-items-center">
				<img src="<?= Url::base(); ?>/assets/img/properties/9.png" alt="properties" class="mw-100">
			</div>
			<div class="col-sm-8 mt-sm-0 mt-4 d-flex align-items-center">
				<div class="property-list-info">
					<h4>Lorem ipsum dolor sit amet</h4>
					<h5>Location info here. Posted by: John Smith</h5>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
					</p>
					<a href="<?= Url::to(['/site/detail']); ?>" class="btn btn-info d-inline-flex align-items-center justify-content-center">Learn More</a>
				</div>
			</div>
		</div>
		<!-- property-list-item -->

		<!-- property-list-item -->
		<div class="row property-list-item">
			<div class="col-sm-4 d-flex align-items-center">
				<img src="<?= Url::base(); ?>/assets/img/properties/9.png" alt="properties" class="mw-100">
			</div>
			<div class="col-sm-8 mt-sm-0 mt-4 d-flex align-items-center">
				<div class="property-list-info">
					<h4>Lorem ipsum dolor sit amet</h4>
					<h5>Location info here. Posted by: John Smith</h5>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
					</p>
					<a href="<?= Url::to(['/site/detail']); ?>" class="btn btn-info d-inline-flex align-items-center justify-content-center">Learn More</a>
				</div>
			</div>
		</div>
		<!-- property-list-item -->

		<!-- property-list-item -->
		<div class="row property-list-item">
			<div class="col-sm-4 d-flex align-items-center">
				<img src="<?= Url::base(); ?>/assets/img/properties/9.png" alt="properties" class="mw-100">
			</div>
			<div class="col-sm-8 mt-sm-0 mt-4 d-flex align-items-center">
				<div class="property-list-info">
					<h4>Lorem ipsum dolor sit amet</h4>
					<h5>Location info here. Posted by: John Smith</h5>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
					</p>
					<a href="<?= Url::to(['/site/detail']); ?>" class="btn btn-info d-inline-flex align-items-center justify-content-center">Learn More</a>
				</div>
			</div>
		</div>
		<!-- property-list-item -->

		<!-- property-list-item -->
		<div class="row property-list-item">
			<div class="col-sm-4 d-flex align-items-center">
				<img src="<?= Url::base(); ?>/assets/img/properties/9.png" alt="properties" class="mw-100">
			</div>
			<div class="col-sm-8 mt-sm-0 mt-4 d-flex align-items-center">
				<div class="property-list-info">
					<h4>Lorem ipsum dolor sit amet</h4>
					<h5>Location info here. Posted by: John Smith</h5>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
					</p>
					<a href="<?= Url::to(['/site/detail']); ?>" class="btn btn-info d-inline-flex align-items-center justify-content-center">Learn More</a>
				</div>
			</div>
		</div>
		<!-- property-list-item -->

		<!-- property-list-item -->
		<div class="row property-list-item">
			<div class="col-sm-4 d-flex align-items-center">
				<img src="<?= Url::base(); ?>/assets/img/properties/9.png" alt="properties" class="mw-100">
			</div>
			<div class="col-sm-8 mt-sm-0 mt-4 d-flex align-items-center">
				<div class="property-list-info">
					<h4>Lorem ipsum dolor sit amet</h4>
					<h5>Location info here. Posted by: John Smith</h5>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
					</p>
					<a href="<?= Url::to(['/site/detail']); ?>" class="btn btn-info d-inline-flex align-items-center justify-content-center">Learn More</a>
				</div>
			</div>
		</div>
		<!-- property-list-item -->

		<!-- property-list-item -->
		<div class="row property-list-pagination">
			<div class="col-lg-12 d-flex justify-content-end">
				<ul class="pagination d-flex align-items-center justify-content-center">
					<li class="d-none"><a href="#">Prev</a></li>
					<li><a href="#" class="active">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">Next</a></li>
				</ul>
			</div>
		</div>
		<!-- property-list-item -->

	</div>
</section>
<!-- property list section -->