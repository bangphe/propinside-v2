<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'assets/castle/css/style.css',
        'assets/castle/css/search.css',
        'assets/castle/css/reality-icon.css',
        // 'assets/propinside/css/bootstrap.min.css',
        // 'assets/propinside/css/slicknav.min.css',
        // 'assets/propinside/css/font-awesome.min.css',
        // 'assets/propinside/style.css',
        // 'assets/propinside/css/responsive.css',
        'assets/lightslider/css/lightslider.css',
        'assets/lightgallery/css/lightgallery.css',
        'assets/propertyguru/css/desktop.css',
        'assets/custom.css',
    ];
    public $js = [
        // 'assets/js/jquery-3.3.1.min.js',
        'assets/lightslider/js/lightslider.js',
        'assets/lightgallery/js/lightgallery-all.min.js',
        'assets/propinside/js/popper.min.js',
        'assets/propinside/js/bootstrap.min.js',
        'assets/propinside/js/jquery.slicknav.min.js',
        'assets/propinside/js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
