/*------------------------------------------------------------------
[Main Script]

Project     : P3-propinside
Version     : 1.0
Author      : Md Ekramul Hassan Avi
Author URI  : https://www.tigertemplate.com
-------------------------------------------------------------------*/
jQuery(document).ready(function(){

	// mobile menu call
	jQuery('#phn-menu').slicknav();

	// owl-carousel testimonial & courses call
	$("#owl-property,#owl-propertytwo").owlCarousel({
		loop:true,
		autoplay:true,
		autoplayTimeout:3000,
		autoplayHoverPause:true,
	    nav:true,
	    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
	    dots:false,
		margin: 0,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }

	}); 

});